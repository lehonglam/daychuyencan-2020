﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using S7.Net;
namespace DayChuyenHPEX
{
    
    static class PlcFuntion
    {
        public static ErrorCode ketnoiPLC()
        {

            hdexGlobalData.ipPLC= registryFunction.docReg("ipPLC");
            hdexGlobalData.plc = new Plc(CpuType.S71200, hdexGlobalData.ipPLC, hdexGlobalData.rack, hdexGlobalData.slot);
            hdexGlobalData.erPLC = hdexGlobalData.plc.Open();

            if (hdexGlobalData.erPLC == ErrorCode.NoError)
            {
                PlcFuntion.docToanBoDuLieuTuPLCbufM();
                PlcFuntion.docToanBoDuLieuTuPLCbufQ();
            }
            return hdexGlobalData.erPLC;
        }

        public static void docToanBoDuLieuTuPLCbufM()
        {
            if (hdexGlobalData.erPLC == ErrorCode.NoError)
            {
                try
                {
                    hdexGlobalData.bufM = hdexGlobalData.plc.ReadBytes(DataType.Memory, 1, 0,  hdexGlobalData.nSizeM);
                }
                catch(Exception ex)
                {
                }
            }
        }
        public static void docToanBoDuLieuTuPLCbufQ()
        {
            try
            {
                hdexGlobalData.bufQ = hdexGlobalData.plc.ReadBytes(DataType.Output, 1, 0,  hdexGlobalData.nSizeQ);
            }
            catch (Exception ex)
            {
            }
        }
        // Lay du lieu tai BUF Q
        public static byte docDulieuTuBufQ(int vitri, int dichbit)
        {
            //(byte)((bufQ2[0] >> 4) & 0x01);
            try
            {
                byte status = (byte)((hdexGlobalData.bufQ[vitri] >> dichbit) & 0x01);
                return status;
            }
            catch(Exception ex)
            {

            }
            return 0;
        }
        public static int docDulieuTuBufM(int vitri)
        {
            
            int value = ((hdexGlobalData.bufM[vitri] << 8) + hdexGlobalData.bufM[vitri + 1]);
            return value;
        }
        public static void ghiDulieuVaoPLC(int value,int vitritaiPLC)
        {
            if (hdexGlobalData.erPLC == ErrorCode.NoError)
            {
                hdexGlobalData.plc.WriteBytes(DataType.Memory, 1, vitritaiPLC, new byte[] { (byte)(value >> 8) });
                hdexGlobalData.plc.WriteBytes(DataType.Memory, 1, vitritaiPLC + 1, new byte[] { (byte)(value >> 0) });
            }
        }
        public static int docDulieuTuPLC(int vitritaiPLC)
        {
            int val = -1;
            if (hdexGlobalData.erPLC == ErrorCode.NoError)
            {
                Byte[] byt = hdexGlobalData.plc.ReadBytes(DataType.Memory, 1, vitritaiPLC, 2);
                val = (byt[vitritaiPLC] << 8) + byt[vitritaiPLC + 1];
            }
            return val;
        }
    }
}
