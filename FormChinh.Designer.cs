﻿namespace DayChuyenHPEX
{
    partial class FormChinh
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DayChuyenHPEX.DongThoi.PLCS71200 plcS712001 = new DayChuyenHPEX.DongThoi.PLCS71200();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormChinh));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            this.timerChayChinh = new System.Windows.Forms.Timer(this.components);
            this.toolTipThongTin = new System.Windows.Forms.ToolTip(this.components);
            this.BienTanDatBT1 = new DayChuyenHPEX.DieuKhien.UserControlThuocTinhGhiXuongPLC();
            this.configBindingHPEXBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.BienTanDat02 = new DayChuyenHPEX.DieuKhien.UserControlThuocTinhGhiXuongPLC();
            this.NhietDoBinhCoDac = new DayChuyenHPEX.DieuKhien.UserControlLabelHienThiNhanh();
            this.BitXoaLoi = new DayChuyenHPEX.DieuKhien.UserControlDaoBit();
            this.toolTipForm = new System.Windows.Forms.ToolTip(this.components);
            this.panelBaoLoi = new System.Windows.Forms.Panel();
            this.Bientan09 = new DayChuyenHPEX.DieuKhien.UserControlThuocTinhGhiXuongPLC();
            this.userControlThuocTinhGhiXuongPLC1 = new DayChuyenHPEX.DieuKhien.UserControlThuocTinhGhiXuongPLC();
            this.Bientan04 = new DayChuyenHPEX.DieuKhien.UserControlThuocTinhGhiXuongPLC();
            this.BienTan08 = new DayChuyenHPEX.DieuKhien.UserControlThuocTinhGhiXuongPLC();
            this.BienTanDat03 = new DayChuyenHPEX.DieuKhien.UserControlThuocTinhGhiXuongPLC();
            this.Bientan06 = new DayChuyenHPEX.DieuKhien.UserControlThuocTinhGhiXuongPLC();
            this.Bientan05 = new DayChuyenHPEX.DieuKhien.UserControlThuocTinhGhiXuongPLC();
            this.userControlThuocTinhGhiXuongPLC3 = new DayChuyenHPEX.DieuKhien.UserControlThuocTinhGhiXuongPLC();
            this.userControlThuocTinhGhiXuongPLC2 = new DayChuyenHPEX.DieuKhien.UserControlThuocTinhGhiXuongPLC();
            this.userControlThuocTinhGhiXuongPLC7 = new DayChuyenHPEX.DieuKhien.UserControlThuocTinhGhiXuongPLC();
            this.userControlThuocTinhGhiXuongPLC4 = new DayChuyenHPEX.DieuKhien.UserControlThuocTinhGhiXuongPLC();
            this.userControlThuocTinhGhiXuongPLC9 = new DayChuyenHPEX.DieuKhien.UserControlThuocTinhGhiXuongPLC();
            this.userControlThuocTinhGhiXuongPLC6 = new DayChuyenHPEX.DieuKhien.UserControlThuocTinhGhiXuongPLC();
            this.NhietDoDat4 = new DayChuyenHPEX.DieuKhien.UserControlThuocTinhGhiXuongPLC();
            this.NhietDoDat2 = new DayChuyenHPEX.DieuKhien.UserControlThuocTinhGhiXuongPLC();
            this.userControlThuocTinhGhiXuongPLC8 = new DayChuyenHPEX.DieuKhien.UserControlThuocTinhGhiXuongPLC();
            this.userControlThuocTinhGhiXuongPLC5 = new DayChuyenHPEX.DieuKhien.UserControlThuocTinhGhiXuongPLC();
            this.NhietDoDat3 = new DayChuyenHPEX.DieuKhien.UserControlThuocTinhGhiXuongPLC();
            this.userControlDaoBit1 = new DayChuyenHPEX.DieuKhien.UserControlDaoBit();
            this.userControlLabelHienThiNhanh2 = new DayChuyenHPEX.DieuKhien.UserControlLabelHienThiNhanh();
            this.userControlLabelHienThiNhanh1 = new DayChuyenHPEX.DieuKhien.UserControlLabelHienThiNhanh();
            this.panelPhanChinh = new System.Windows.Forms.Panel();
            this.userControlLabelText4 = new DayChuyenHPEX.DieuKhien.UserControlLabelText();
            this.userControlLabelText5 = new DayChuyenHPEX.DieuKhien.UserControlLabelText();
            this.userControlLabelText6 = new DayChuyenHPEX.DieuKhien.UserControlLabelText();
            this.userControlThietBi10 = new DayChuyenHPEX.DieuKhien.UserControlThietBi();
            this.userControlThietBi11 = new DayChuyenHPEX.DieuKhien.UserControlThietBi();
            this.userControlThietBi12 = new DayChuyenHPEX.DieuKhien.UserControlThietBi();
            this.userControlLabelText3 = new DayChuyenHPEX.DieuKhien.UserControlLabelText();
            this.userControlLabelText2 = new DayChuyenHPEX.DieuKhien.UserControlLabelText();
            this.userControlLabelText1 = new DayChuyenHPEX.DieuKhien.UserControlLabelText();
            this.userControlThietBi8 = new DayChuyenHPEX.DieuKhien.UserControlThietBi();
            this.userControlThietBi7 = new DayChuyenHPEX.DieuKhien.UserControlThietBi();
            this.userControlThietBi9 = new DayChuyenHPEX.DieuKhien.UserControlThietBi();
            this.userControlThietBi6 = new DayChuyenHPEX.DieuKhien.UserControlThietBi();
            this.userControlThietBi5 = new DayChuyenHPEX.DieuKhien.UserControlThietBi();
            this.LoiChung = new DayChuyenHPEX.DieuKhien.UserControlBaoLoi();
            this.userControlThietBi4 = new DayChuyenHPEX.DieuKhien.UserControlThietBi();
            this.userControlThietBi3 = new DayChuyenHPEX.DieuKhien.UserControlThietBi();
            this.ucPLCChinh = new DayChuyenHPEX.DieuKhien.ucPLC();
            this.userControlThietBi2 = new DayChuyenHPEX.DieuKhien.UserControlThietBi();
            this.VanCoDac = new DayChuyenHPEX.DieuKhien.UserControlThietBi();
            this.DongCoVitTaiMayDongBao = new DayChuyenHPEX.DieuKhien.UserControlThietBi();
            this.DongCoVitTaiMayTronPhai = new DayChuyenHPEX.DieuKhien.UserControlThietBi();
            this.DongCoVitTaiMayTronTrai = new DayChuyenHPEX.DieuKhien.UserControlThietBi();
            this.BomDungDich01 = new DayChuyenHPEX.DieuKhien.UserControlThietBi();
            this.BomLenCoDac = new DayChuyenHPEX.DieuKhien.UserControlThietBi();
            this.BomBanhRangLenMayTaoHat = new DayChuyenHPEX.DieuKhien.UserControlThietBi();
            this.userControlThietBiBOM = new DayChuyenHPEX.DieuKhien.UserControlThietBi();
            this.DongCoMayBocVo = new DayChuyenHPEX.DieuKhien.UserControlThietBi();
            this.DongCoMayTron = new DayChuyenHPEX.DieuKhien.UserControlThietBi();
            this.userControlThietBi1 = new DayChuyenHPEX.DieuKhien.UserControlThietBi();
            this.NhietDoBinhChuaDungDichDauTien = new DayChuyenHPEX.DieuKhien.UserControlLabelHienThiNhanh();
            this.NhietDoT3 = new DayChuyenHPEX.DieuKhien.UserControlLabelHienThiNhanh();
            this.NhietDoBinhDungDichBocVo = new DayChuyenHPEX.DieuKhien.UserControlLabelHienThiNhanh();
            this.userControlLabelHienThiNhanh7 = new DayChuyenHPEX.DieuKhien.UserControlLabelHienThiNhanh();
            this.userControlLabelHienThiNhanh6 = new DayChuyenHPEX.DieuKhien.UserControlLabelHienThiNhanh();
            this.userControlLabelHienThiNhanh5 = new DayChuyenHPEX.DieuKhien.UserControlLabelHienThiNhanh();
            this.userControlLabelHienThiNhanh4 = new DayChuyenHPEX.DieuKhien.UserControlLabelHienThiNhanh();
            this.userControlLabelHienThiNhanh3 = new DayChuyenHPEX.DieuKhien.UserControlLabelHienThiNhanh();
            this.NhietDoOngBocVo = new DayChuyenHPEX.DieuKhien.UserControlLabelHienThiNhanh();
            this.LuuLuongKeFL2 = new DayChuyenHPEX.DieuKhien.UserControlLabelHienThiNhanh();
            this.LuuLuongKeFL1 = new DayChuyenHPEX.DieuKhien.UserControlLabelHienThiNhanh();
            this.NhietDoBinhChuaSauCoDacT4 = new DayChuyenHPEX.DieuKhien.UserControlLabelHienThiNhanh();
            this.ribbonPage2 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPage3 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.barManagerBottom = new DevExpress.XtraBars.BarManager(this.components);
            this.barBottomStatus = new DevExpress.XtraBars.Bar();
            this.btnServer = new DevExpress.XtraBars.BarButtonItem();
            this.btnUser = new DevExpress.XtraBars.BarButtonItem();
            this.btnQuyen = new DevExpress.XtraBars.BarButtonItem();
            this.btnError = new DevExpress.XtraBars.BarStaticItem();
            this.btnVersion = new DevExpress.XtraBars.BarButtonItem();
            this.barEditItemProgressBarChinh = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemProgressBarChinh = new DevExpress.XtraEditors.Repository.RepositoryItemProgressBar();
            this.ProgressbarEditItem1 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemProgressBarBottom = new DevExpress.XtraEditors.Repository.RepositoryItemProgressBar();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.btnPlc = new DevExpress.XtraBars.BarButtonItem();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.barManagerTop = new DevExpress.XtraBars.BarManager(this.components);
            this.barTop = new DevExpress.XtraBars.Bar();
            this.btnKhoiTao = new DevExpress.XtraBars.BarButtonItem();
            this.menuChinh = new DevExpress.XtraBars.BarSubItem();
            this.btnNhanSu = new DevExpress.XtraBars.BarButtonItem();
            this.btnTroGiup = new DevExpress.XtraBars.BarButtonItem();
            this.btnLog = new DevExpress.XtraBars.BarButtonItem();
            this.btnBanQuyen = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.splashScreenManagerForm = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::DayChuyenHPEX.WaitFormKetNoi), true, true);
            ((System.ComponentModel.ISupportInitialize)(this.configBindingHPEXBindingSource)).BeginInit();
            this.panelBaoLoi.SuspendLayout();
            this.panelPhanChinh.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.barManagerBottom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBarChinh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBarBottom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManagerTop)).BeginInit();
            this.SuspendLayout();
            // 
            // timerChayChinh
            // 
            this.timerChayChinh.Interval = 1000;
            this.timerChayChinh.Tick += new System.EventHandler(this.timerChuongTrinhChinhStick);
            // 
            // toolTipThongTin
            // 
            this.toolTipThongTin.ToolTipTitle = "Thông tin";
            // 
            // BienTanDatBT1
            // 
            this.BienTanDatBT1.BackColor = System.Drawing.Color.Transparent;
            this.BienTanDatBT1.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.BienTanDatBT1.dichbitThuocTinhXacNhanPLC = 2;
            this.BienTanDatBT1.donviDo = "v/ph";
            this.BienTanDatBT1.enable = true;
            this.BienTanDatBT1.formatText = "  0.00";
            this.BienTanDatBT1.gioiHanDuoi = 10D;
            this.BienTanDatBT1.gioiHanTren = 50D;
            this.BienTanDatBT1.Location = new System.Drawing.Point(3, 3);
            this.BienTanDatBT1.m_Location = new System.Drawing.Point(3, 3);
            this.BienTanDatBT1.m_size = new System.Drawing.Size(292, 26);
            this.BienTanDatBT1.m_Visible = true;
            this.BienTanDatBT1.Name = "BienTanDatBT1";
            this.BienTanDatBT1.plc = null;
            this.BienTanDatBT1.quyenQuantri = false;
            this.BienTanDatBT1.scale = 10D;
            this.BienTanDatBT1.Size = new System.Drawing.Size(292, 26);
            this.BienTanDatBT1.TabIndex = 146;
            this.BienTanDatBT1.tenthuocTinh = "Biến tần 01";
            this.toolTipForm.SetToolTip(this.BienTanDatBT1, "Đặt tốc độ cho máy khuấy OXH1");
            this.toolTipThongTin.SetToolTip(this.BienTanDatBT1, "Đặt tốc độ cho máy khuấy OXH1");
            this.BienTanDatBT1.vitriPLC = 100;
            this.BienTanDatBT1.vitriPLCThuocTinhXacNhanPLC = 17;
            this.BienTanDatBT1.Load += new System.EventHandler(this.userControlThuocTinhGhiXuongPLCTanSoDatBT1_Load);
            // 
            // configBindingHPEXBindingSource
            // 
            this.configBindingHPEXBindingSource.DataSource = typeof(DayChuyenHPEX.configBindingHPEX);
            // 
            // BienTanDat02
            // 
            this.BienTanDat02.BackColor = System.Drawing.Color.Transparent;
            this.BienTanDat02.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.BienTanDat02.dichbitThuocTinhXacNhanPLC = 3;
            this.BienTanDat02.donviDo = "v/ph";
            this.BienTanDat02.enable = true;
            this.BienTanDat02.formatText = "  0.00";
            this.BienTanDat02.gioiHanDuoi = 10D;
            this.BienTanDat02.gioiHanTren = 50D;
            this.BienTanDat02.Location = new System.Drawing.Point(3, 35);
            this.BienTanDat02.m_Location = new System.Drawing.Point(3, 35);
            this.BienTanDat02.m_size = new System.Drawing.Size(292, 26);
            this.BienTanDat02.m_Visible = true;
            this.BienTanDat02.Name = "BienTanDat02";
            this.BienTanDat02.plc = null;
            this.BienTanDat02.quyenQuantri = false;
            this.BienTanDat02.scale = 10D;
            this.BienTanDat02.Size = new System.Drawing.Size(292, 26);
            this.BienTanDat02.TabIndex = 182;
            this.BienTanDat02.tenthuocTinh = "Biến tần 02";
            this.toolTipForm.SetToolTip(this.BienTanDat02, "Đặt tốc độ cho máy khuấy OXH1");
            this.toolTipThongTin.SetToolTip(this.BienTanDat02, "Đặt tốc độ cho máy khuấy OXH1");
            this.BienTanDat02.vitriPLC = 104;
            this.BienTanDat02.vitriPLCThuocTinhXacNhanPLC = 17;
            // 
            // NhietDoBinhCoDac
            // 
            this.NhietDoBinhCoDac.BackColor = System.Drawing.Color.Transparent;
            this.NhietDoBinhCoDac.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.NhietDoBinhCoDac.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.NhietDoBinhCoDac.dinhdangthapphan = "0.0";
            this.NhietDoBinhCoDac.donviDo = "độ C";
            this.NhietDoBinhCoDac.enable = true;
            this.NhietDoBinhCoDac.giatri = 25D;
            this.NhietDoBinhCoDac.hienThitenThuocTinh = false;
            this.NhietDoBinhCoDac.Location = new System.Drawing.Point(420, 163);
            this.NhietDoBinhCoDac.m_Location = new System.Drawing.Point(420, 163);
            this.NhietDoBinhCoDac.m_size = new System.Drawing.Size(81, 19);
            this.NhietDoBinhCoDac.m_Visible = true;
            this.NhietDoBinhCoDac.Name = "NhietDoBinhCoDac";
            this.NhietDoBinhCoDac.plc = null;
            this.NhietDoBinhCoDac.quyenQuantri = false;
            this.NhietDoBinhCoDac.scale = 0.1D;
            this.NhietDoBinhCoDac.Size = new System.Drawing.Size(81, 19);
            this.NhietDoBinhCoDac.TabIndex = 140;
            this.NhietDoBinhCoDac.tenthuocTinh = "Nhiệt độ T2";
            this.NhietDoBinhCoDac.vitriPLC = 174;
            // 
            // BitXoaLoi
            // 
            this.BitXoaLoi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BitXoaLoi.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.BitXoaLoi.dichbitThuocTinhXacNhanPLC = 6;
            this.BitXoaLoi.enable = true;
            this.BitXoaLoi.Location = new System.Drawing.Point(170, 740);
            this.BitXoaLoi.m_img = null;
            this.BitXoaLoi.m_Location = new System.Drawing.Point(170, 740);
            this.BitXoaLoi.m_size = new System.Drawing.Size(125, 56);
            this.BitXoaLoi.m_Visible = true;
            this.BitXoaLoi.mauNen = System.Drawing.Color.Transparent;
            this.BitXoaLoi.Name = "BitXoaLoi";
            this.BitXoaLoi.plc = null;
            this.BitXoaLoi.quyenQuantri = false;
            this.BitXoaLoi.Size = new System.Drawing.Size(125, 56);
            this.BitXoaLoi.TabIndex = 150;
            this.BitXoaLoi.tenthuocTinh = "Xóa lỗi";
            this.BitXoaLoi.vitriPLCThuocTinhXacNhanPLC = 13;
            this.BitXoaLoi.Load += new System.EventHandler(this.userControlDaoBitXoaLoi_Load);
            // 
            // panelBaoLoi
            // 
            this.panelBaoLoi.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelBaoLoi.BackColor = System.Drawing.Color.Snow;
            this.panelBaoLoi.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelBaoLoi.Controls.Add(this.Bientan09);
            this.panelBaoLoi.Controls.Add(this.userControlThuocTinhGhiXuongPLC1);
            this.panelBaoLoi.Controls.Add(this.Bientan04);
            this.panelBaoLoi.Controls.Add(this.BienTan08);
            this.panelBaoLoi.Controls.Add(this.BienTanDat03);
            this.panelBaoLoi.Controls.Add(this.Bientan06);
            this.panelBaoLoi.Controls.Add(this.BienTanDat02);
            this.panelBaoLoi.Controls.Add(this.Bientan05);
            this.panelBaoLoi.Controls.Add(this.userControlThuocTinhGhiXuongPLC3);
            this.panelBaoLoi.Controls.Add(this.userControlThuocTinhGhiXuongPLC2);
            this.panelBaoLoi.Controls.Add(this.userControlThuocTinhGhiXuongPLC7);
            this.panelBaoLoi.Controls.Add(this.userControlThuocTinhGhiXuongPLC4);
            this.panelBaoLoi.Controls.Add(this.userControlThuocTinhGhiXuongPLC9);
            this.panelBaoLoi.Controls.Add(this.userControlThuocTinhGhiXuongPLC6);
            this.panelBaoLoi.Controls.Add(this.NhietDoDat4);
            this.panelBaoLoi.Controls.Add(this.NhietDoDat2);
            this.panelBaoLoi.Controls.Add(this.userControlThuocTinhGhiXuongPLC8);
            this.panelBaoLoi.Controls.Add(this.userControlThuocTinhGhiXuongPLC5);
            this.panelBaoLoi.Controls.Add(this.NhietDoDat3);
            this.panelBaoLoi.Controls.Add(this.BienTanDatBT1);
            this.panelBaoLoi.Controls.Add(this.userControlDaoBit1);
            this.panelBaoLoi.Controls.Add(this.BitXoaLoi);
            this.panelBaoLoi.Controls.Add(this.userControlLabelHienThiNhanh2);
            this.panelBaoLoi.Controls.Add(this.userControlLabelHienThiNhanh1);
            this.panelBaoLoi.Location = new System.Drawing.Point(1518, 24);
            this.panelBaoLoi.Name = "panelBaoLoi";
            this.panelBaoLoi.Size = new System.Drawing.Size(311, 801);
            this.panelBaoLoi.TabIndex = 24;
            // 
            // Bientan09
            // 
            this.Bientan09.BackColor = System.Drawing.Color.Transparent;
            this.Bientan09.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.Bientan09.dichbitThuocTinhXacNhanPLC = 2;
            this.Bientan09.donviDo = "v/ph";
            this.Bientan09.enable = true;
            this.Bientan09.formatText = "  0.00";
            this.Bientan09.gioiHanDuoi = 10D;
            this.Bientan09.gioiHanTren = 50D;
            this.Bientan09.Location = new System.Drawing.Point(3, 259);
            this.Bientan09.m_Location = new System.Drawing.Point(3, 259);
            this.Bientan09.m_size = new System.Drawing.Size(292, 26);
            this.Bientan09.m_Visible = true;
            this.Bientan09.Name = "Bientan09";
            this.Bientan09.plc = null;
            this.Bientan09.quyenQuantri = false;
            this.Bientan09.scale = 10D;
            this.Bientan09.Size = new System.Drawing.Size(292, 26);
            this.Bientan09.TabIndex = 182;
            this.Bientan09.tenthuocTinh = "Biến tần 09";
            this.Bientan09.vitriPLC = 132;
            this.Bientan09.vitriPLCThuocTinhXacNhanPLC = 18;
            // 
            // userControlThuocTinhGhiXuongPLC1
            // 
            this.userControlThuocTinhGhiXuongPLC1.BackColor = System.Drawing.Color.Transparent;
            this.userControlThuocTinhGhiXuongPLC1.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.userControlThuocTinhGhiXuongPLC1.dichbitThuocTinhXacNhanPLC = 5;
            this.userControlThuocTinhGhiXuongPLC1.donviDo = "hz";
            this.userControlThuocTinhGhiXuongPLC1.enable = true;
            this.userControlThuocTinhGhiXuongPLC1.formatText = "  0.00";
            this.userControlThuocTinhGhiXuongPLC1.gioiHanDuoi = 10D;
            this.userControlThuocTinhGhiXuongPLC1.gioiHanTren = 50D;
            this.userControlThuocTinhGhiXuongPLC1.Location = new System.Drawing.Point(3, 131);
            this.userControlThuocTinhGhiXuongPLC1.m_Location = new System.Drawing.Point(3, 131);
            this.userControlThuocTinhGhiXuongPLC1.m_size = new System.Drawing.Size(292, 26);
            this.userControlThuocTinhGhiXuongPLC1.m_Visible = true;
            this.userControlThuocTinhGhiXuongPLC1.Name = "userControlThuocTinhGhiXuongPLC1";
            this.userControlThuocTinhGhiXuongPLC1.plc = null;
            this.userControlThuocTinhGhiXuongPLC1.quyenQuantri = false;
            this.userControlThuocTinhGhiXuongPLC1.scale = 10D;
            this.userControlThuocTinhGhiXuongPLC1.Size = new System.Drawing.Size(292, 26);
            this.userControlThuocTinhGhiXuongPLC1.TabIndex = 182;
            this.userControlThuocTinhGhiXuongPLC1.tenthuocTinh = "Biến tần 05";
            this.userControlThuocTinhGhiXuongPLC1.vitriPLC = 112;
            this.userControlThuocTinhGhiXuongPLC1.vitriPLCThuocTinhXacNhanPLC = 17;
            // 
            // Bientan04
            // 
            this.Bientan04.BackColor = System.Drawing.Color.Transparent;
            this.Bientan04.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.Bientan04.dichbitThuocTinhXacNhanPLC = 5;
            this.Bientan04.donviDo = "hz";
            this.Bientan04.enable = true;
            this.Bientan04.formatText = "  0.00";
            this.Bientan04.gioiHanDuoi = 10D;
            this.Bientan04.gioiHanTren = 50D;
            this.Bientan04.Location = new System.Drawing.Point(3, 99);
            this.Bientan04.m_Location = new System.Drawing.Point(3, 99);
            this.Bientan04.m_size = new System.Drawing.Size(292, 26);
            this.Bientan04.m_Visible = true;
            this.Bientan04.Name = "Bientan04";
            this.Bientan04.plc = null;
            this.Bientan04.quyenQuantri = false;
            this.Bientan04.scale = 10D;
            this.Bientan04.Size = new System.Drawing.Size(292, 26);
            this.Bientan04.TabIndex = 182;
            this.Bientan04.tenthuocTinh = "Biến tần 04";
            this.Bientan04.vitriPLC = 112;
            this.Bientan04.vitriPLCThuocTinhXacNhanPLC = 17;
            // 
            // BienTan08
            // 
            this.BienTan08.BackColor = System.Drawing.Color.Transparent;
            this.BienTan08.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.BienTan08.dichbitThuocTinhXacNhanPLC = 1;
            this.BienTan08.donviDo = "v/ph";
            this.BienTan08.enable = true;
            this.BienTan08.formatText = "  0.00";
            this.BienTan08.gioiHanDuoi = 10D;
            this.BienTan08.gioiHanTren = 50D;
            this.BienTan08.Location = new System.Drawing.Point(3, 227);
            this.BienTan08.m_Location = new System.Drawing.Point(3, 227);
            this.BienTan08.m_size = new System.Drawing.Size(292, 26);
            this.BienTan08.m_Visible = true;
            this.BienTan08.Name = "BienTan08";
            this.BienTan08.plc = null;
            this.BienTan08.quyenQuantri = false;
            this.BienTan08.scale = 10D;
            this.BienTan08.Size = new System.Drawing.Size(292, 26);
            this.BienTan08.TabIndex = 182;
            this.BienTan08.tenthuocTinh = "Biến tần 08";
            this.BienTan08.vitriPLC = 128;
            this.BienTan08.vitriPLCThuocTinhXacNhanPLC = 18;
            // 
            // BienTanDat03
            // 
            this.BienTanDat03.BackColor = System.Drawing.Color.Transparent;
            this.BienTanDat03.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.BienTanDat03.dichbitThuocTinhXacNhanPLC = 4;
            this.BienTanDat03.donviDo = "v/ph";
            this.BienTanDat03.enable = true;
            this.BienTanDat03.formatText = "  0.00";
            this.BienTanDat03.gioiHanDuoi = 10D;
            this.BienTanDat03.gioiHanTren = 50D;
            this.BienTanDat03.Location = new System.Drawing.Point(3, 67);
            this.BienTanDat03.m_Location = new System.Drawing.Point(3, 67);
            this.BienTanDat03.m_size = new System.Drawing.Size(292, 26);
            this.BienTanDat03.m_Visible = true;
            this.BienTanDat03.Name = "BienTanDat03";
            this.BienTanDat03.plc = null;
            this.BienTanDat03.quyenQuantri = false;
            this.BienTanDat03.scale = 10D;
            this.BienTanDat03.Size = new System.Drawing.Size(292, 26);
            this.BienTanDat03.TabIndex = 182;
            this.BienTanDat03.tenthuocTinh = "Biến tần 03";
            this.BienTanDat03.vitriPLC = 108;
            this.BienTanDat03.vitriPLCThuocTinhXacNhanPLC = 17;
            // 
            // Bientan06
            // 
            this.Bientan06.BackColor = System.Drawing.Color.Transparent;
            this.Bientan06.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.Bientan06.dichbitThuocTinhXacNhanPLC = 0;
            this.Bientan06.donviDo = "v/ph";
            this.Bientan06.enable = true;
            this.Bientan06.formatText = "  0.00";
            this.Bientan06.gioiHanDuoi = 10D;
            this.Bientan06.gioiHanTren = 50D;
            this.Bientan06.Location = new System.Drawing.Point(3, 195);
            this.Bientan06.m_Location = new System.Drawing.Point(3, 195);
            this.Bientan06.m_size = new System.Drawing.Size(292, 26);
            this.Bientan06.m_Visible = true;
            this.Bientan06.Name = "Bientan06";
            this.Bientan06.plc = null;
            this.Bientan06.quyenQuantri = false;
            this.Bientan06.scale = 10D;
            this.Bientan06.Size = new System.Drawing.Size(292, 26);
            this.Bientan06.TabIndex = 182;
            this.Bientan06.tenthuocTinh = "Biến tần 07";
            this.Bientan06.vitriPLC = 124;
            this.Bientan06.vitriPLCThuocTinhXacNhanPLC = 18;
            // 
            // Bientan05
            // 
            this.Bientan05.BackColor = System.Drawing.Color.Transparent;
            this.Bientan05.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.Bientan05.dichbitThuocTinhXacNhanPLC = 7;
            this.Bientan05.donviDo = "hz";
            this.Bientan05.enable = true;
            this.Bientan05.formatText = "  0.00";
            this.Bientan05.gioiHanDuoi = 10D;
            this.Bientan05.gioiHanTren = 50D;
            this.Bientan05.Location = new System.Drawing.Point(3, 163);
            this.Bientan05.m_Location = new System.Drawing.Point(3, 163);
            this.Bientan05.m_size = new System.Drawing.Size(292, 26);
            this.Bientan05.m_Visible = true;
            this.Bientan05.Name = "Bientan05";
            this.Bientan05.plc = null;
            this.Bientan05.quyenQuantri = false;
            this.Bientan05.scale = 10D;
            this.Bientan05.Size = new System.Drawing.Size(292, 26);
            this.Bientan05.TabIndex = 146;
            this.Bientan05.tenthuocTinh = "Biến tần 06";
            this.Bientan05.vitriPLC = 120;
            this.Bientan05.vitriPLCThuocTinhXacNhanPLC = 17;
            this.Bientan05.Load += new System.EventHandler(this.userControlThuocTinhGhiXuongPLCTanSoDatBT1_Load);
            // 
            // userControlThuocTinhGhiXuongPLC3
            // 
            this.userControlThuocTinhGhiXuongPLC3.BackColor = System.Drawing.Color.Transparent;
            this.userControlThuocTinhGhiXuongPLC3.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.userControlThuocTinhGhiXuongPLC3.dichbitThuocTinhXacNhanPLC = 3;
            this.userControlThuocTinhGhiXuongPLC3.donviDo = "kg";
            this.userControlThuocTinhGhiXuongPLC3.enable = true;
            this.userControlThuocTinhGhiXuongPLC3.ForeColor = System.Drawing.Color.Black;
            this.userControlThuocTinhGhiXuongPLC3.formatText = "0.00";
            this.userControlThuocTinhGhiXuongPLC3.gioiHanDuoi = 10D;
            this.userControlThuocTinhGhiXuongPLC3.gioiHanTren = 50D;
            this.userControlThuocTinhGhiXuongPLC3.Location = new System.Drawing.Point(3, 421);
            this.userControlThuocTinhGhiXuongPLC3.m_Location = new System.Drawing.Point(3, 421);
            this.userControlThuocTinhGhiXuongPLC3.m_size = new System.Drawing.Size(292, 26);
            this.userControlThuocTinhGhiXuongPLC3.m_Visible = true;
            this.userControlThuocTinhGhiXuongPLC3.Name = "userControlThuocTinhGhiXuongPLC3";
            this.userControlThuocTinhGhiXuongPLC3.plc = null;
            this.userControlThuocTinhGhiXuongPLC3.quyenQuantri = false;
            this.userControlThuocTinhGhiXuongPLC3.scale = 10D;
            this.userControlThuocTinhGhiXuongPLC3.Size = new System.Drawing.Size(292, 26);
            this.userControlThuocTinhGhiXuongPLC3.TabIndex = 146;
            this.userControlThuocTinhGhiXuongPLC3.tenthuocTinh = "Dat muc can";
            this.userControlThuocTinhGhiXuongPLC3.vitriPLC = 184;
            this.userControlThuocTinhGhiXuongPLC3.vitriPLCThuocTinhXacNhanPLC = 3;
            this.userControlThuocTinhGhiXuongPLC3.Load += new System.EventHandler(this.userControlThuocTinhGhiXuongPLCTanSoDatBT1_Load);
            // 
            // userControlThuocTinhGhiXuongPLC2
            // 
            this.userControlThuocTinhGhiXuongPLC2.BackColor = System.Drawing.Color.Transparent;
            this.userControlThuocTinhGhiXuongPLC2.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.userControlThuocTinhGhiXuongPLC2.dichbitThuocTinhXacNhanPLC = 3;
            this.userControlThuocTinhGhiXuongPLC2.donviDo = "kg";
            this.userControlThuocTinhGhiXuongPLC2.enable = true;
            this.userControlThuocTinhGhiXuongPLC2.ForeColor = System.Drawing.Color.Black;
            this.userControlThuocTinhGhiXuongPLC2.formatText = "0.00";
            this.userControlThuocTinhGhiXuongPLC2.gioiHanDuoi = 10D;
            this.userControlThuocTinhGhiXuongPLC2.gioiHanTren = 50D;
            this.userControlThuocTinhGhiXuongPLC2.Location = new System.Drawing.Point(3, 453);
            this.userControlThuocTinhGhiXuongPLC2.m_Location = new System.Drawing.Point(3, 453);
            this.userControlThuocTinhGhiXuongPLC2.m_size = new System.Drawing.Size(292, 26);
            this.userControlThuocTinhGhiXuongPLC2.m_Visible = true;
            this.userControlThuocTinhGhiXuongPLC2.Name = "userControlThuocTinhGhiXuongPLC2";
            this.userControlThuocTinhGhiXuongPLC2.plc = null;
            this.userControlThuocTinhGhiXuongPLC2.quyenQuantri = false;
            this.userControlThuocTinhGhiXuongPLC2.scale = 10D;
            this.userControlThuocTinhGhiXuongPLC2.Size = new System.Drawing.Size(292, 26);
            this.userControlThuocTinhGhiXuongPLC2.TabIndex = 146;
            this.userControlThuocTinhGhiXuongPLC2.tenthuocTinh = "Hieu chinh can";
            this.userControlThuocTinhGhiXuongPLC2.vitriPLC = 184;
            this.userControlThuocTinhGhiXuongPLC2.vitriPLCThuocTinhXacNhanPLC = 3;
            this.userControlThuocTinhGhiXuongPLC2.Load += new System.EventHandler(this.userControlThuocTinhGhiXuongPLCTanSoDatBT1_Load);
            // 
            // userControlThuocTinhGhiXuongPLC7
            // 
            this.userControlThuocTinhGhiXuongPLC7.BackColor = System.Drawing.Color.Transparent;
            this.userControlThuocTinhGhiXuongPLC7.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.userControlThuocTinhGhiXuongPLC7.dichbitThuocTinhXacNhanPLC = 3;
            this.userControlThuocTinhGhiXuongPLC7.donviDo = "độ";
            this.userControlThuocTinhGhiXuongPLC7.enable = true;
            this.userControlThuocTinhGhiXuongPLC7.ForeColor = System.Drawing.Color.Black;
            this.userControlThuocTinhGhiXuongPLC7.formatText = "0.00";
            this.userControlThuocTinhGhiXuongPLC7.gioiHanDuoi = 10D;
            this.userControlThuocTinhGhiXuongPLC7.gioiHanTren = 50D;
            this.userControlThuocTinhGhiXuongPLC7.Location = new System.Drawing.Point(3, 701);
            this.userControlThuocTinhGhiXuongPLC7.m_Location = new System.Drawing.Point(3, 701);
            this.userControlThuocTinhGhiXuongPLC7.m_size = new System.Drawing.Size(292, 26);
            this.userControlThuocTinhGhiXuongPLC7.m_Visible = true;
            this.userControlThuocTinhGhiXuongPLC7.Name = "userControlThuocTinhGhiXuongPLC7";
            this.userControlThuocTinhGhiXuongPLC7.plc = null;
            this.userControlThuocTinhGhiXuongPLC7.quyenQuantri = false;
            this.userControlThuocTinhGhiXuongPLC7.scale = 10D;
            this.userControlThuocTinhGhiXuongPLC7.Size = new System.Drawing.Size(292, 26);
            this.userControlThuocTinhGhiXuongPLC7.TabIndex = 146;
            this.userControlThuocTinhGhiXuongPLC7.tenthuocTinh = "Nhiệt độ T05 PG";
            this.userControlThuocTinhGhiXuongPLC7.vitriPLC = 184;
            this.userControlThuocTinhGhiXuongPLC7.vitriPLCThuocTinhXacNhanPLC = 3;
            this.userControlThuocTinhGhiXuongPLC7.Load += new System.EventHandler(this.userControlThuocTinhGhiXuongPLCTanSoDatBT1_Load);
            // 
            // userControlThuocTinhGhiXuongPLC4
            // 
            this.userControlThuocTinhGhiXuongPLC4.BackColor = System.Drawing.Color.Transparent;
            this.userControlThuocTinhGhiXuongPLC4.dichbitThuocTinhXacNhanPLC = 3;
            this.userControlThuocTinhGhiXuongPLC4.donviDo = "độ";
            this.userControlThuocTinhGhiXuongPLC4.enable = true;
            this.userControlThuocTinhGhiXuongPLC4.ForeColor = System.Drawing.Color.Black;
            this.userControlThuocTinhGhiXuongPLC4.formatText = "0.00";
            this.userControlThuocTinhGhiXuongPLC4.gioiHanDuoi = 10D;
            this.userControlThuocTinhGhiXuongPLC4.gioiHanTren = 50D;
            this.userControlThuocTinhGhiXuongPLC4.Location = new System.Drawing.Point(3, 387);
            this.userControlThuocTinhGhiXuongPLC4.m_Location = new System.Drawing.Point(3, 387);
            this.userControlThuocTinhGhiXuongPLC4.m_size = new System.Drawing.Size(292, 26);
            this.userControlThuocTinhGhiXuongPLC4.m_Visible = true;
            this.userControlThuocTinhGhiXuongPLC4.Name = "userControlThuocTinhGhiXuongPLC4";
            this.userControlThuocTinhGhiXuongPLC4.plc = null;
            this.userControlThuocTinhGhiXuongPLC4.quyenQuantri = false;
            this.userControlThuocTinhGhiXuongPLC4.scale = 10D;
            this.userControlThuocTinhGhiXuongPLC4.Size = new System.Drawing.Size(292, 26);
            this.userControlThuocTinhGhiXuongPLC4.TabIndex = 146;
            this.userControlThuocTinhGhiXuongPLC4.tenthuocTinh = "Nhiệt độ T05 PG";
            this.userControlThuocTinhGhiXuongPLC4.vitriPLC = 184;
            this.userControlThuocTinhGhiXuongPLC4.vitriPLCThuocTinhXacNhanPLC = 3;
            this.userControlThuocTinhGhiXuongPLC4.Load += new System.EventHandler(this.userControlThuocTinhGhiXuongPLCTanSoDatBT1_Load);
            // 
            // userControlThuocTinhGhiXuongPLC9
            // 
            this.userControlThuocTinhGhiXuongPLC9.BackColor = System.Drawing.Color.Transparent;
            this.userControlThuocTinhGhiXuongPLC9.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.userControlThuocTinhGhiXuongPLC9.dichbitThuocTinhXacNhanPLC = 3;
            this.userControlThuocTinhGhiXuongPLC9.donviDo = "độ";
            this.userControlThuocTinhGhiXuongPLC9.enable = true;
            this.userControlThuocTinhGhiXuongPLC9.ForeColor = System.Drawing.Color.Black;
            this.userControlThuocTinhGhiXuongPLC9.formatText = "0.00";
            this.userControlThuocTinhGhiXuongPLC9.gioiHanDuoi = 10D;
            this.userControlThuocTinhGhiXuongPLC9.gioiHanTren = 50D;
            this.userControlThuocTinhGhiXuongPLC9.Location = new System.Drawing.Point(3, 611);
            this.userControlThuocTinhGhiXuongPLC9.m_Location = new System.Drawing.Point(3, 611);
            this.userControlThuocTinhGhiXuongPLC9.m_size = new System.Drawing.Size(292, 26);
            this.userControlThuocTinhGhiXuongPLC9.m_Visible = true;
            this.userControlThuocTinhGhiXuongPLC9.Name = "userControlThuocTinhGhiXuongPLC9";
            this.userControlThuocTinhGhiXuongPLC9.plc = null;
            this.userControlThuocTinhGhiXuongPLC9.quyenQuantri = false;
            this.userControlThuocTinhGhiXuongPLC9.scale = 10D;
            this.userControlThuocTinhGhiXuongPLC9.Size = new System.Drawing.Size(292, 26);
            this.userControlThuocTinhGhiXuongPLC9.TabIndex = 146;
            this.userControlThuocTinhGhiXuongPLC9.tenthuocTinh = "Nhiệt độ T05 PG";
            this.userControlThuocTinhGhiXuongPLC9.vitriPLC = 184;
            this.userControlThuocTinhGhiXuongPLC9.vitriPLCThuocTinhXacNhanPLC = 3;
            this.userControlThuocTinhGhiXuongPLC9.Load += new System.EventHandler(this.userControlThuocTinhGhiXuongPLCTanSoDatBT1_Load);
            // 
            // userControlThuocTinhGhiXuongPLC6
            // 
            this.userControlThuocTinhGhiXuongPLC6.BackColor = System.Drawing.Color.Transparent;
            this.userControlThuocTinhGhiXuongPLC6.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.userControlThuocTinhGhiXuongPLC6.dichbitThuocTinhXacNhanPLC = 3;
            this.userControlThuocTinhGhiXuongPLC6.donviDo = "độ";
            this.userControlThuocTinhGhiXuongPLC6.enable = true;
            this.userControlThuocTinhGhiXuongPLC6.ForeColor = System.Drawing.Color.Black;
            this.userControlThuocTinhGhiXuongPLC6.formatText = "0.00";
            this.userControlThuocTinhGhiXuongPLC6.gioiHanDuoi = 10D;
            this.userControlThuocTinhGhiXuongPLC6.gioiHanTren = 50D;
            this.userControlThuocTinhGhiXuongPLC6.Location = new System.Drawing.Point(3, 669);
            this.userControlThuocTinhGhiXuongPLC6.m_Location = new System.Drawing.Point(3, 669);
            this.userControlThuocTinhGhiXuongPLC6.m_size = new System.Drawing.Size(292, 26);
            this.userControlThuocTinhGhiXuongPLC6.m_Visible = true;
            this.userControlThuocTinhGhiXuongPLC6.Name = "userControlThuocTinhGhiXuongPLC6";
            this.userControlThuocTinhGhiXuongPLC6.plc = null;
            this.userControlThuocTinhGhiXuongPLC6.quyenQuantri = false;
            this.userControlThuocTinhGhiXuongPLC6.scale = 10D;
            this.userControlThuocTinhGhiXuongPLC6.Size = new System.Drawing.Size(292, 26);
            this.userControlThuocTinhGhiXuongPLC6.TabIndex = 146;
            this.userControlThuocTinhGhiXuongPLC6.tenthuocTinh = "Nhiệt độ T05 PG";
            this.userControlThuocTinhGhiXuongPLC6.vitriPLC = 184;
            this.userControlThuocTinhGhiXuongPLC6.vitriPLCThuocTinhXacNhanPLC = 3;
            this.userControlThuocTinhGhiXuongPLC6.Load += new System.EventHandler(this.userControlThuocTinhGhiXuongPLCTanSoDatBT1_Load);
            // 
            // NhietDoDat4
            // 
            this.NhietDoDat4.BackColor = System.Drawing.Color.Transparent;
            this.NhietDoDat4.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.NhietDoDat4.dichbitThuocTinhXacNhanPLC = 3;
            this.NhietDoDat4.donviDo = "độ";
            this.NhietDoDat4.enable = true;
            this.NhietDoDat4.ForeColor = System.Drawing.Color.Black;
            this.NhietDoDat4.formatText = "0.00";
            this.NhietDoDat4.gioiHanDuoi = 10D;
            this.NhietDoDat4.gioiHanTren = 50D;
            this.NhietDoDat4.Location = new System.Drawing.Point(3, 355);
            this.NhietDoDat4.m_Location = new System.Drawing.Point(3, 355);
            this.NhietDoDat4.m_size = new System.Drawing.Size(292, 26);
            this.NhietDoDat4.m_Visible = true;
            this.NhietDoDat4.Name = "NhietDoDat4";
            this.NhietDoDat4.plc = null;
            this.NhietDoDat4.quyenQuantri = false;
            this.NhietDoDat4.scale = 10D;
            this.NhietDoDat4.Size = new System.Drawing.Size(292, 26);
            this.NhietDoDat4.TabIndex = 146;
            this.NhietDoDat4.tenthuocTinh = "Nhiệt độ T05 PG";
            this.NhietDoDat4.vitriPLC = 184;
            this.NhietDoDat4.vitriPLCThuocTinhXacNhanPLC = 3;
            this.NhietDoDat4.Load += new System.EventHandler(this.userControlThuocTinhGhiXuongPLCTanSoDatBT1_Load);
            // 
            // NhietDoDat2
            // 
            this.NhietDoDat2.BackColor = System.Drawing.Color.Transparent;
            this.NhietDoDat2.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.NhietDoDat2.dichbitThuocTinhXacNhanPLC = 1;
            this.NhietDoDat2.donviDo = "độ";
            this.NhietDoDat2.enable = true;
            this.NhietDoDat2.ForeColor = System.Drawing.Color.Black;
            this.NhietDoDat2.formatText = "0.00";
            this.NhietDoDat2.gioiHanDuoi = 10D;
            this.NhietDoDat2.gioiHanTren = 50D;
            this.NhietDoDat2.Location = new System.Drawing.Point(3, 291);
            this.NhietDoDat2.m_Location = new System.Drawing.Point(3, 291);
            this.NhietDoDat2.m_size = new System.Drawing.Size(292, 26);
            this.NhietDoDat2.m_Visible = true;
            this.NhietDoDat2.Name = "NhietDoDat2";
            this.NhietDoDat2.plc = null;
            this.NhietDoDat2.quyenQuantri = false;
            this.NhietDoDat2.scale = 10D;
            this.NhietDoDat2.Size = new System.Drawing.Size(292, 26);
            this.NhietDoDat2.TabIndex = 146;
            this.NhietDoDat2.tenthuocTinh = "Nhiệt độ T3-co dac";
            this.NhietDoDat2.vitriPLC = 176;
            this.NhietDoDat2.vitriPLCThuocTinhXacNhanPLC = 3;
            this.NhietDoDat2.Load += new System.EventHandler(this.userControlThuocTinhGhiXuongPLCTanSoDatBT1_Load);
            // 
            // userControlThuocTinhGhiXuongPLC8
            // 
            this.userControlThuocTinhGhiXuongPLC8.BackColor = System.Drawing.Color.Transparent;
            this.userControlThuocTinhGhiXuongPLC8.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.userControlThuocTinhGhiXuongPLC8.dichbitThuocTinhXacNhanPLC = 2;
            this.userControlThuocTinhGhiXuongPLC8.donviDo = "độ";
            this.userControlThuocTinhGhiXuongPLC8.enable = true;
            this.userControlThuocTinhGhiXuongPLC8.ForeColor = System.Drawing.Color.Black;
            this.userControlThuocTinhGhiXuongPLC8.formatText = "0.00";
            this.userControlThuocTinhGhiXuongPLC8.gioiHanDuoi = 10D;
            this.userControlThuocTinhGhiXuongPLC8.gioiHanTren = 50D;
            this.userControlThuocTinhGhiXuongPLC8.Location = new System.Drawing.Point(3, 579);
            this.userControlThuocTinhGhiXuongPLC8.m_Location = new System.Drawing.Point(3, 579);
            this.userControlThuocTinhGhiXuongPLC8.m_size = new System.Drawing.Size(292, 26);
            this.userControlThuocTinhGhiXuongPLC8.m_Visible = true;
            this.userControlThuocTinhGhiXuongPLC8.Name = "userControlThuocTinhGhiXuongPLC8";
            this.userControlThuocTinhGhiXuongPLC8.plc = null;
            this.userControlThuocTinhGhiXuongPLC8.quyenQuantri = false;
            this.userControlThuocTinhGhiXuongPLC8.scale = 10D;
            this.userControlThuocTinhGhiXuongPLC8.Size = new System.Drawing.Size(292, 26);
            this.userControlThuocTinhGhiXuongPLC8.TabIndex = 146;
            this.userControlThuocTinhGhiXuongPLC8.tenthuocTinh = "Nhiệt độ T04-dich CAN";
            this.userControlThuocTinhGhiXuongPLC8.vitriPLC = 180;
            this.userControlThuocTinhGhiXuongPLC8.vitriPLCThuocTinhXacNhanPLC = 4;
            this.userControlThuocTinhGhiXuongPLC8.Load += new System.EventHandler(this.userControlThuocTinhGhiXuongPLCTanSoDatBT1_Load);
            // 
            // userControlThuocTinhGhiXuongPLC5
            // 
            this.userControlThuocTinhGhiXuongPLC5.BackColor = System.Drawing.Color.Transparent;
            this.userControlThuocTinhGhiXuongPLC5.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.userControlThuocTinhGhiXuongPLC5.dichbitThuocTinhXacNhanPLC = 2;
            this.userControlThuocTinhGhiXuongPLC5.donviDo = "độ";
            this.userControlThuocTinhGhiXuongPLC5.enable = true;
            this.userControlThuocTinhGhiXuongPLC5.ForeColor = System.Drawing.Color.Black;
            this.userControlThuocTinhGhiXuongPLC5.formatText = "0.00";
            this.userControlThuocTinhGhiXuongPLC5.gioiHanDuoi = 10D;
            this.userControlThuocTinhGhiXuongPLC5.gioiHanTren = 50D;
            this.userControlThuocTinhGhiXuongPLC5.Location = new System.Drawing.Point(3, 637);
            this.userControlThuocTinhGhiXuongPLC5.m_Location = new System.Drawing.Point(3, 637);
            this.userControlThuocTinhGhiXuongPLC5.m_size = new System.Drawing.Size(292, 26);
            this.userControlThuocTinhGhiXuongPLC5.m_Visible = true;
            this.userControlThuocTinhGhiXuongPLC5.Name = "userControlThuocTinhGhiXuongPLC5";
            this.userControlThuocTinhGhiXuongPLC5.plc = null;
            this.userControlThuocTinhGhiXuongPLC5.quyenQuantri = false;
            this.userControlThuocTinhGhiXuongPLC5.scale = 10D;
            this.userControlThuocTinhGhiXuongPLC5.Size = new System.Drawing.Size(292, 26);
            this.userControlThuocTinhGhiXuongPLC5.TabIndex = 146;
            this.userControlThuocTinhGhiXuongPLC5.tenthuocTinh = "Nhiệt độ T04-dich CAN";
            this.userControlThuocTinhGhiXuongPLC5.vitriPLC = 180;
            this.userControlThuocTinhGhiXuongPLC5.vitriPLCThuocTinhXacNhanPLC = 4;
            this.userControlThuocTinhGhiXuongPLC5.Load += new System.EventHandler(this.userControlThuocTinhGhiXuongPLCTanSoDatBT1_Load);
            // 
            // NhietDoDat3
            // 
            this.NhietDoDat3.BackColor = System.Drawing.Color.Transparent;
            this.NhietDoDat3.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.NhietDoDat3.dichbitThuocTinhXacNhanPLC = 2;
            this.NhietDoDat3.donviDo = "độ";
            this.NhietDoDat3.enable = true;
            this.NhietDoDat3.ForeColor = System.Drawing.Color.Black;
            this.NhietDoDat3.formatText = "0.00";
            this.NhietDoDat3.gioiHanDuoi = 10D;
            this.NhietDoDat3.gioiHanTren = 50D;
            this.NhietDoDat3.Location = new System.Drawing.Point(3, 323);
            this.NhietDoDat3.m_Location = new System.Drawing.Point(3, 323);
            this.NhietDoDat3.m_size = new System.Drawing.Size(292, 26);
            this.NhietDoDat3.m_Visible = true;
            this.NhietDoDat3.Name = "NhietDoDat3";
            this.NhietDoDat3.plc = null;
            this.NhietDoDat3.quyenQuantri = false;
            this.NhietDoDat3.scale = 10D;
            this.NhietDoDat3.Size = new System.Drawing.Size(292, 26);
            this.NhietDoDat3.TabIndex = 146;
            this.NhietDoDat3.tenthuocTinh = "Nhiệt độ T04-dich CAN";
            this.NhietDoDat3.vitriPLC = 180;
            this.NhietDoDat3.vitriPLCThuocTinhXacNhanPLC = 4;
            this.NhietDoDat3.Load += new System.EventHandler(this.userControlThuocTinhGhiXuongPLCTanSoDatBT1_Load);
            // 
            // userControlDaoBit1
            // 
            this.userControlDaoBit1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.userControlDaoBit1.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.userControlDaoBit1.dichbitThuocTinhXacNhanPLC = 6;
            this.userControlDaoBit1.enable = true;
            this.userControlDaoBit1.Location = new System.Drawing.Point(15, 740);
            this.userControlDaoBit1.m_img = null;
            this.userControlDaoBit1.m_Location = new System.Drawing.Point(15, 740);
            this.userControlDaoBit1.m_size = new System.Drawing.Size(149, 56);
            this.userControlDaoBit1.m_Visible = true;
            this.userControlDaoBit1.mauNen = System.Drawing.Color.MistyRose;
            this.userControlDaoBit1.Name = "userControlDaoBit1";
            this.userControlDaoBit1.plc = null;
            this.userControlDaoBit1.quyenQuantri = false;
            this.userControlDaoBit1.Size = new System.Drawing.Size(149, 56);
            this.userControlDaoBit1.TabIndex = 150;
            this.userControlDaoBit1.tenthuocTinh = "xoa can";
            this.userControlDaoBit1.vitriPLCThuocTinhXacNhanPLC = 13;
            this.userControlDaoBit1.Load += new System.EventHandler(this.userControlDaoBitXoaLoi_Load);
            // 
            // userControlLabelHienThiNhanh2
            // 
            this.userControlLabelHienThiNhanh2.BackColor = System.Drawing.Color.Transparent;
            this.userControlLabelHienThiNhanh2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.userControlLabelHienThiNhanh2.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.userControlLabelHienThiNhanh2.dinhdangthapphan = "0.0";
            this.userControlLabelHienThiNhanh2.donviDo = "kg";
            this.userControlLabelHienThiNhanh2.enable = true;
            this.userControlLabelHienThiNhanh2.giatri = 25D;
            this.userControlLabelHienThiNhanh2.hienThitenThuocTinh = false;
            this.userControlLabelHienThiNhanh2.Location = new System.Drawing.Point(15, 528);
            this.userControlLabelHienThiNhanh2.m_Location = new System.Drawing.Point(15, 528);
            this.userControlLabelHienThiNhanh2.m_size = new System.Drawing.Size(291, 19);
            this.userControlLabelHienThiNhanh2.m_Visible = true;
            this.userControlLabelHienThiNhanh2.Name = "userControlLabelHienThiNhanh2";
            this.userControlLabelHienThiNhanh2.plc = null;
            this.userControlLabelHienThiNhanh2.quyenQuantri = false;
            this.userControlLabelHienThiNhanh2.scale = 0.1D;
            this.userControlLabelHienThiNhanh2.Size = new System.Drawing.Size(291, 19);
            this.userControlLabelHienThiNhanh2.TabIndex = 140;
            this.userControlLabelHienThiNhanh2.tenthuocTinh = "Tổng ca cân";
            this.userControlLabelHienThiNhanh2.vitriPLC = 192;
            this.userControlLabelHienThiNhanh2.Load += new System.EventHandler(this.userControlLabelHienThiNhanh1_Load);
            // 
            // userControlLabelHienThiNhanh1
            // 
            this.userControlLabelHienThiNhanh1.BackColor = System.Drawing.Color.Transparent;
            this.userControlLabelHienThiNhanh1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.userControlLabelHienThiNhanh1.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.userControlLabelHienThiNhanh1.dinhdangthapphan = "0.0";
            this.userControlLabelHienThiNhanh1.donviDo = "kg";
            this.userControlLabelHienThiNhanh1.enable = true;
            this.userControlLabelHienThiNhanh1.giatri = 25D;
            this.userControlLabelHienThiNhanh1.hienThitenThuocTinh = true;
            this.userControlLabelHienThiNhanh1.Location = new System.Drawing.Point(15, 503);
            this.userControlLabelHienThiNhanh1.m_Location = new System.Drawing.Point(15, 503);
            this.userControlLabelHienThiNhanh1.m_size = new System.Drawing.Size(280, 19);
            this.userControlLabelHienThiNhanh1.m_Visible = true;
            this.userControlLabelHienThiNhanh1.Name = "userControlLabelHienThiNhanh1";
            this.userControlLabelHienThiNhanh1.plc = null;
            this.userControlLabelHienThiNhanh1.quyenQuantri = false;
            this.userControlLabelHienThiNhanh1.scale = 0.1D;
            this.userControlLabelHienThiNhanh1.Size = new System.Drawing.Size(280, 19);
            this.userControlLabelHienThiNhanh1.TabIndex = 140;
            this.userControlLabelHienThiNhanh1.tenthuocTinh = "Khối lượng cân";
            this.userControlLabelHienThiNhanh1.vitriPLC = 192;
            this.userControlLabelHienThiNhanh1.Load += new System.EventHandler(this.userControlLabelHienThiNhanh1_Load);
            // 
            // panelPhanChinh
            // 
            this.panelPhanChinh.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelPhanChinh.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelPhanChinh.BackColor = System.Drawing.Color.Transparent;
            this.panelPhanChinh.BackgroundImage = global::DayChuyenHPEX.Properties.Resources._2_anhnen_CAN_1500_800;
            this.panelPhanChinh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panelPhanChinh.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelPhanChinh.Controls.Add(this.userControlLabelText4);
            this.panelPhanChinh.Controls.Add(this.userControlLabelText5);
            this.panelPhanChinh.Controls.Add(this.userControlLabelText6);
            this.panelPhanChinh.Controls.Add(this.userControlThietBi10);
            this.panelPhanChinh.Controls.Add(this.userControlThietBi11);
            this.panelPhanChinh.Controls.Add(this.userControlThietBi12);
            this.panelPhanChinh.Controls.Add(this.userControlLabelText3);
            this.panelPhanChinh.Controls.Add(this.userControlLabelText2);
            this.panelPhanChinh.Controls.Add(this.userControlLabelText1);
            this.panelPhanChinh.Controls.Add(this.userControlThietBi8);
            this.panelPhanChinh.Controls.Add(this.userControlThietBi7);
            this.panelPhanChinh.Controls.Add(this.userControlThietBi9);
            this.panelPhanChinh.Controls.Add(this.userControlThietBi6);
            this.panelPhanChinh.Controls.Add(this.userControlThietBi5);
            this.panelPhanChinh.Controls.Add(this.LoiChung);
            this.panelPhanChinh.Controls.Add(this.userControlThietBi4);
            this.panelPhanChinh.Controls.Add(this.userControlThietBi3);
            this.panelPhanChinh.Controls.Add(this.ucPLCChinh);
            this.panelPhanChinh.Controls.Add(this.userControlThietBi2);
            this.panelPhanChinh.Controls.Add(this.VanCoDac);
            this.panelPhanChinh.Controls.Add(this.DongCoVitTaiMayDongBao);
            this.panelPhanChinh.Controls.Add(this.DongCoVitTaiMayTronPhai);
            this.panelPhanChinh.Controls.Add(this.DongCoVitTaiMayTronTrai);
            this.panelPhanChinh.Controls.Add(this.BomDungDich01);
            this.panelPhanChinh.Controls.Add(this.BomLenCoDac);
            this.panelPhanChinh.Controls.Add(this.BomBanhRangLenMayTaoHat);
            this.panelPhanChinh.Controls.Add(this.userControlThietBiBOM);
            this.panelPhanChinh.Controls.Add(this.DongCoMayBocVo);
            this.panelPhanChinh.Controls.Add(this.DongCoMayTron);
            this.panelPhanChinh.Controls.Add(this.userControlThietBi1);
            this.panelPhanChinh.Controls.Add(this.NhietDoBinhChuaDungDichDauTien);
            this.panelPhanChinh.Controls.Add(this.NhietDoT3);
            this.panelPhanChinh.Controls.Add(this.NhietDoBinhDungDichBocVo);
            this.panelPhanChinh.Controls.Add(this.userControlLabelHienThiNhanh7);
            this.panelPhanChinh.Controls.Add(this.userControlLabelHienThiNhanh6);
            this.panelPhanChinh.Controls.Add(this.userControlLabelHienThiNhanh5);
            this.panelPhanChinh.Controls.Add(this.userControlLabelHienThiNhanh4);
            this.panelPhanChinh.Controls.Add(this.userControlLabelHienThiNhanh3);
            this.panelPhanChinh.Controls.Add(this.NhietDoOngBocVo);
            this.panelPhanChinh.Controls.Add(this.LuuLuongKeFL2);
            this.panelPhanChinh.Controls.Add(this.LuuLuongKeFL1);
            this.panelPhanChinh.Controls.Add(this.NhietDoBinhChuaSauCoDacT4);
            this.panelPhanChinh.Controls.Add(this.NhietDoBinhCoDac);
            this.panelPhanChinh.Location = new System.Drawing.Point(12, 24);
            this.panelPhanChinh.Name = "panelPhanChinh";
            this.panelPhanChinh.Size = new System.Drawing.Size(1500, 801);
            this.panelPhanChinh.TabIndex = 23;
            this.panelPhanChinh.Paint += new System.Windows.Forms.PaintEventHandler(this.panelPhanChinh_Paint);
            // 
            // userControlLabelText4
            // 
            this.userControlLabelText4.BackColor = System.Drawing.Color.Transparent;
            this.userControlLabelText4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.userControlLabelText4.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.userControlLabelText4.enable = true;
            this.userControlLabelText4.Location = new System.Drawing.Point(1319, 137);
            this.userControlLabelText4.m_Location = new System.Drawing.Point(1319, 137);
            this.userControlLabelText4.m_size = new System.Drawing.Size(74, 20);
            this.userControlLabelText4.m_Visible = true;
            this.userControlLabelText4.mauText = System.Drawing.Color.Black;
            this.userControlLabelText4.Name = "userControlLabelText4";
            this.userControlLabelText4.plc = null;
            this.userControlLabelText4.quyenQuantri = false;
            this.userControlLabelText4.Size = new System.Drawing.Size(74, 20);
            this.userControlLabelText4.TabIndex = 197;
            this.userControlLabelText4.tenthuocTinh = "Nhiệt độ";
            // 
            // userControlLabelText5
            // 
            this.userControlLabelText5.BackColor = System.Drawing.Color.Transparent;
            this.userControlLabelText5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.userControlLabelText5.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.userControlLabelText5.enable = true;
            this.userControlLabelText5.Location = new System.Drawing.Point(1319, 86);
            this.userControlLabelText5.m_Location = new System.Drawing.Point(1319, 86);
            this.userControlLabelText5.m_size = new System.Drawing.Size(74, 20);
            this.userControlLabelText5.m_Visible = true;
            this.userControlLabelText5.mauText = System.Drawing.Color.Black;
            this.userControlLabelText5.Name = "userControlLabelText5";
            this.userControlLabelText5.plc = null;
            this.userControlLabelText5.quyenQuantri = false;
            this.userControlLabelText5.Size = new System.Drawing.Size(74, 20);
            this.userControlLabelText5.TabIndex = 198;
            this.userControlLabelText5.tenthuocTinh = "Nhiệt độ";
            // 
            // userControlLabelText6
            // 
            this.userControlLabelText6.BackColor = System.Drawing.Color.Transparent;
            this.userControlLabelText6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.userControlLabelText6.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.userControlLabelText6.enable = true;
            this.userControlLabelText6.Location = new System.Drawing.Point(1319, 41);
            this.userControlLabelText6.m_Location = new System.Drawing.Point(1319, 41);
            this.userControlLabelText6.m_size = new System.Drawing.Size(74, 20);
            this.userControlLabelText6.m_Visible = true;
            this.userControlLabelText6.mauText = System.Drawing.Color.Black;
            this.userControlLabelText6.Name = "userControlLabelText6";
            this.userControlLabelText6.plc = null;
            this.userControlLabelText6.quyenQuantri = false;
            this.userControlLabelText6.Size = new System.Drawing.Size(74, 20);
            this.userControlLabelText6.TabIndex = 199;
            this.userControlLabelText6.tenthuocTinh = "Nhiệt độ";
            // 
            // userControlThietBi10
            // 
            this.userControlThietBi10.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.userControlThietBi10.BackColor = System.Drawing.Color.Transparent;
            this.userControlThietBi10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.userControlThietBi10.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.userControlThietBi10.enable = true;
            this.userControlThietBi10.hienThitenThuocTinh = false;
            this.userControlThietBi10.loaiThietBi = DayChuyenHPEX.DieuKhien.LoaiThietBi.MAY;
            this.userControlThietBi10.Location = new System.Drawing.Point(1395, 22);
            this.userControlThietBi10.m_batHopThoaiRun = true;
            this.userControlThietBi10.m_choPhepMoPhong = true;
            this.userControlThietBi10.m_Description = "";
            this.userControlThietBi10.m_dichbitGiamSat = 1;
            this.userControlThietBi10.m_dichbitStart = 0;
            this.userControlThietBi10.m_dichbitStop = 1;
            this.userControlThietBi10.m_gocquay = System.Drawing.RotateFlipType.Rotate180FlipY;
            this.userControlThietBi10.m_imgNomal = global::DayChuyenHPEX.Properties.Resources.Van_Stop;
            this.userControlThietBi10.m_imgRun1 = global::DayChuyenHPEX.Properties.Resources._11;
            this.userControlThietBi10.m_imgRun2 = null;
            this.userControlThietBi10.m_imgStop = global::DayChuyenHPEX.Properties.Resources.Van_Stop;
            this.userControlThietBi10.m_Location = new System.Drawing.Point(1395, 22);
            this.userControlThietBi10.m_maThietBi = "";
            this.userControlThietBi10.m_sImgNomal = null;
            this.userControlThietBi10.m_sImgRun1 = null;
            this.userControlThietBi10.m_sImgRun2 = null;
            this.userControlThietBi10.m_sImgStop = null;
            this.userControlThietBi10.m_size = new System.Drawing.Size(70, 39);
            this.userControlThietBi10.m_Visible = true;
            this.userControlThietBi10.m_vitriPLCGiamSat = 1;
            this.userControlThietBi10.m_vitriPLCStart = 1;
            this.userControlThietBi10.m_vitriPLCStop = 1;
            this.userControlThietBi10.Name = "userControlThietBi10";
            this.userControlThietBi10.plc = null;
            this.userControlThietBi10.quyenQuantri = false;
            this.userControlThietBi10.Size = new System.Drawing.Size(70, 39);
            this.userControlThietBi10.TabIndex = 196;
            this.userControlThietBi10.tenthuocTinh = "M03";
            // 
            // userControlThietBi11
            // 
            this.userControlThietBi11.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.userControlThietBi11.BackColor = System.Drawing.Color.Transparent;
            this.userControlThietBi11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.userControlThietBi11.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.userControlThietBi11.enable = true;
            this.userControlThietBi11.hienThitenThuocTinh = false;
            this.userControlThietBi11.loaiThietBi = DayChuyenHPEX.DieuKhien.LoaiThietBi.MAY;
            this.userControlThietBi11.Location = new System.Drawing.Point(1395, 67);
            this.userControlThietBi11.m_batHopThoaiRun = true;
            this.userControlThietBi11.m_choPhepMoPhong = true;
            this.userControlThietBi11.m_Description = "";
            this.userControlThietBi11.m_dichbitGiamSat = 1;
            this.userControlThietBi11.m_dichbitStart = 0;
            this.userControlThietBi11.m_dichbitStop = 1;
            this.userControlThietBi11.m_gocquay = System.Drawing.RotateFlipType.Rotate180FlipY;
            this.userControlThietBi11.m_imgNomal = global::DayChuyenHPEX.Properties.Resources.Van_Stop;
            this.userControlThietBi11.m_imgRun1 = global::DayChuyenHPEX.Properties.Resources._11;
            this.userControlThietBi11.m_imgRun2 = null;
            this.userControlThietBi11.m_imgStop = global::DayChuyenHPEX.Properties.Resources.Van_Stop;
            this.userControlThietBi11.m_Location = new System.Drawing.Point(1395, 67);
            this.userControlThietBi11.m_maThietBi = "";
            this.userControlThietBi11.m_sImgNomal = null;
            this.userControlThietBi11.m_sImgRun1 = null;
            this.userControlThietBi11.m_sImgRun2 = null;
            this.userControlThietBi11.m_sImgStop = null;
            this.userControlThietBi11.m_size = new System.Drawing.Size(70, 39);
            this.userControlThietBi11.m_Visible = true;
            this.userControlThietBi11.m_vitriPLCGiamSat = 1;
            this.userControlThietBi11.m_vitriPLCStart = 1;
            this.userControlThietBi11.m_vitriPLCStop = 1;
            this.userControlThietBi11.Name = "userControlThietBi11";
            this.userControlThietBi11.plc = null;
            this.userControlThietBi11.quyenQuantri = false;
            this.userControlThietBi11.Size = new System.Drawing.Size(70, 39);
            this.userControlThietBi11.TabIndex = 195;
            this.userControlThietBi11.tenthuocTinh = "M03";
            // 
            // userControlThietBi12
            // 
            this.userControlThietBi12.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.userControlThietBi12.BackColor = System.Drawing.Color.Transparent;
            this.userControlThietBi12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.userControlThietBi12.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.userControlThietBi12.enable = true;
            this.userControlThietBi12.hienThitenThuocTinh = false;
            this.userControlThietBi12.loaiThietBi = DayChuyenHPEX.DieuKhien.LoaiThietBi.MAY;
            this.userControlThietBi12.Location = new System.Drawing.Point(1395, 118);
            this.userControlThietBi12.m_batHopThoaiRun = true;
            this.userControlThietBi12.m_choPhepMoPhong = true;
            this.userControlThietBi12.m_Description = "";
            this.userControlThietBi12.m_dichbitGiamSat = 1;
            this.userControlThietBi12.m_dichbitStart = 0;
            this.userControlThietBi12.m_dichbitStop = 1;
            this.userControlThietBi12.m_gocquay = System.Drawing.RotateFlipType.Rotate180FlipY;
            this.userControlThietBi12.m_imgNomal = global::DayChuyenHPEX.Properties.Resources.Van_Stop;
            this.userControlThietBi12.m_imgRun1 = global::DayChuyenHPEX.Properties.Resources._11;
            this.userControlThietBi12.m_imgRun2 = null;
            this.userControlThietBi12.m_imgStop = global::DayChuyenHPEX.Properties.Resources.Van_Stop;
            this.userControlThietBi12.m_Location = new System.Drawing.Point(1395, 118);
            this.userControlThietBi12.m_maThietBi = "";
            this.userControlThietBi12.m_sImgNomal = null;
            this.userControlThietBi12.m_sImgRun1 = null;
            this.userControlThietBi12.m_sImgRun2 = null;
            this.userControlThietBi12.m_sImgStop = null;
            this.userControlThietBi12.m_size = new System.Drawing.Size(70, 39);
            this.userControlThietBi12.m_Visible = true;
            this.userControlThietBi12.m_vitriPLCGiamSat = 1;
            this.userControlThietBi12.m_vitriPLCStart = 1;
            this.userControlThietBi12.m_vitriPLCStop = 1;
            this.userControlThietBi12.Name = "userControlThietBi12";
            this.userControlThietBi12.plc = null;
            this.userControlThietBi12.quyenQuantri = false;
            this.userControlThietBi12.Size = new System.Drawing.Size(70, 39);
            this.userControlThietBi12.TabIndex = 194;
            this.userControlThietBi12.tenthuocTinh = "M03";
            // 
            // userControlLabelText3
            // 
            this.userControlLabelText3.BackColor = System.Drawing.Color.Transparent;
            this.userControlLabelText3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.userControlLabelText3.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.userControlLabelText3.enable = true;
            this.userControlLabelText3.Location = new System.Drawing.Point(47, 169);
            this.userControlLabelText3.m_Location = new System.Drawing.Point(47, 169);
            this.userControlLabelText3.m_size = new System.Drawing.Size(74, 20);
            this.userControlLabelText3.m_Visible = true;
            this.userControlLabelText3.mauText = System.Drawing.Color.Black;
            this.userControlLabelText3.Name = "userControlLabelText3";
            this.userControlLabelText3.plc = null;
            this.userControlLabelText3.quyenQuantri = false;
            this.userControlLabelText3.Size = new System.Drawing.Size(74, 20);
            this.userControlLabelText3.TabIndex = 193;
            this.userControlLabelText3.tenthuocTinh = "Nhiệt độ";
            // 
            // userControlLabelText2
            // 
            this.userControlLabelText2.BackColor = System.Drawing.Color.Transparent;
            this.userControlLabelText2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.userControlLabelText2.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.userControlLabelText2.enable = true;
            this.userControlLabelText2.Location = new System.Drawing.Point(47, 118);
            this.userControlLabelText2.m_Location = new System.Drawing.Point(47, 118);
            this.userControlLabelText2.m_size = new System.Drawing.Size(74, 20);
            this.userControlLabelText2.m_Visible = true;
            this.userControlLabelText2.mauText = System.Drawing.Color.Black;
            this.userControlLabelText2.Name = "userControlLabelText2";
            this.userControlLabelText2.plc = null;
            this.userControlLabelText2.quyenQuantri = false;
            this.userControlLabelText2.Size = new System.Drawing.Size(74, 20);
            this.userControlLabelText2.TabIndex = 193;
            this.userControlLabelText2.tenthuocTinh = "Nhiệt độ";
            // 
            // userControlLabelText1
            // 
            this.userControlLabelText1.BackColor = System.Drawing.Color.Transparent;
            this.userControlLabelText1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.userControlLabelText1.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.userControlLabelText1.enable = true;
            this.userControlLabelText1.Location = new System.Drawing.Point(47, 73);
            this.userControlLabelText1.m_Location = new System.Drawing.Point(47, 73);
            this.userControlLabelText1.m_size = new System.Drawing.Size(74, 20);
            this.userControlLabelText1.m_Visible = true;
            this.userControlLabelText1.mauText = System.Drawing.Color.Black;
            this.userControlLabelText1.Name = "userControlLabelText1";
            this.userControlLabelText1.plc = null;
            this.userControlLabelText1.quyenQuantri = false;
            this.userControlLabelText1.Size = new System.Drawing.Size(74, 20);
            this.userControlLabelText1.TabIndex = 193;
            this.userControlLabelText1.tenthuocTinh = "Nhiệt độ";
            // 
            // userControlThietBi8
            // 
            this.userControlThietBi8.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.userControlThietBi8.BackColor = System.Drawing.Color.Transparent;
            this.userControlThietBi8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.userControlThietBi8.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.userControlThietBi8.enable = true;
            this.userControlThietBi8.hienThitenThuocTinh = false;
            this.userControlThietBi8.loaiThietBi = DayChuyenHPEX.DieuKhien.LoaiThietBi.MAY;
            this.userControlThietBi8.Location = new System.Drawing.Point(123, 54);
            this.userControlThietBi8.m_batHopThoaiRun = true;
            this.userControlThietBi8.m_choPhepMoPhong = true;
            this.userControlThietBi8.m_Description = "";
            this.userControlThietBi8.m_dichbitGiamSat = 1;
            this.userControlThietBi8.m_dichbitStart = 0;
            this.userControlThietBi8.m_dichbitStop = 1;
            this.userControlThietBi8.m_gocquay = System.Drawing.RotateFlipType.Rotate180FlipY;
            this.userControlThietBi8.m_imgNomal = global::DayChuyenHPEX.Properties.Resources.Van_Stop;
            this.userControlThietBi8.m_imgRun1 = global::DayChuyenHPEX.Properties.Resources._11;
            this.userControlThietBi8.m_imgRun2 = null;
            this.userControlThietBi8.m_imgStop = global::DayChuyenHPEX.Properties.Resources.Van_Stop;
            this.userControlThietBi8.m_Location = new System.Drawing.Point(123, 54);
            this.userControlThietBi8.m_maThietBi = "";
            this.userControlThietBi8.m_sImgNomal = null;
            this.userControlThietBi8.m_sImgRun1 = null;
            this.userControlThietBi8.m_sImgRun2 = null;
            this.userControlThietBi8.m_sImgStop = null;
            this.userControlThietBi8.m_size = new System.Drawing.Size(70, 39);
            this.userControlThietBi8.m_Visible = true;
            this.userControlThietBi8.m_vitriPLCGiamSat = 1;
            this.userControlThietBi8.m_vitriPLCStart = 1;
            this.userControlThietBi8.m_vitriPLCStop = 1;
            this.userControlThietBi8.Name = "userControlThietBi8";
            this.userControlThietBi8.plc = null;
            this.userControlThietBi8.quyenQuantri = false;
            this.userControlThietBi8.Size = new System.Drawing.Size(70, 39);
            this.userControlThietBi8.TabIndex = 192;
            this.userControlThietBi8.tenthuocTinh = "M03";
            // 
            // userControlThietBi7
            // 
            this.userControlThietBi7.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.userControlThietBi7.BackColor = System.Drawing.Color.Transparent;
            this.userControlThietBi7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.userControlThietBi7.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.userControlThietBi7.enable = true;
            this.userControlThietBi7.hienThitenThuocTinh = false;
            this.userControlThietBi7.loaiThietBi = DayChuyenHPEX.DieuKhien.LoaiThietBi.MAY;
            this.userControlThietBi7.Location = new System.Drawing.Point(123, 99);
            this.userControlThietBi7.m_batHopThoaiRun = true;
            this.userControlThietBi7.m_choPhepMoPhong = true;
            this.userControlThietBi7.m_Description = "";
            this.userControlThietBi7.m_dichbitGiamSat = 1;
            this.userControlThietBi7.m_dichbitStart = 0;
            this.userControlThietBi7.m_dichbitStop = 1;
            this.userControlThietBi7.m_gocquay = System.Drawing.RotateFlipType.Rotate180FlipY;
            this.userControlThietBi7.m_imgNomal = global::DayChuyenHPEX.Properties.Resources.Van_Stop;
            this.userControlThietBi7.m_imgRun1 = global::DayChuyenHPEX.Properties.Resources._11;
            this.userControlThietBi7.m_imgRun2 = null;
            this.userControlThietBi7.m_imgStop = global::DayChuyenHPEX.Properties.Resources.Van_Stop;
            this.userControlThietBi7.m_Location = new System.Drawing.Point(123, 99);
            this.userControlThietBi7.m_maThietBi = "";
            this.userControlThietBi7.m_sImgNomal = null;
            this.userControlThietBi7.m_sImgRun1 = null;
            this.userControlThietBi7.m_sImgRun2 = null;
            this.userControlThietBi7.m_sImgStop = null;
            this.userControlThietBi7.m_size = new System.Drawing.Size(70, 39);
            this.userControlThietBi7.m_Visible = true;
            this.userControlThietBi7.m_vitriPLCGiamSat = 1;
            this.userControlThietBi7.m_vitriPLCStart = 1;
            this.userControlThietBi7.m_vitriPLCStop = 1;
            this.userControlThietBi7.Name = "userControlThietBi7";
            this.userControlThietBi7.plc = null;
            this.userControlThietBi7.quyenQuantri = false;
            this.userControlThietBi7.Size = new System.Drawing.Size(70, 39);
            this.userControlThietBi7.TabIndex = 191;
            this.userControlThietBi7.tenthuocTinh = "M03";
            // 
            // userControlThietBi9
            // 
            this.userControlThietBi9.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.userControlThietBi9.BackColor = System.Drawing.Color.Transparent;
            this.userControlThietBi9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.userControlThietBi9.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.userControlThietBi9.enable = true;
            this.userControlThietBi9.hienThitenThuocTinh = false;
            this.userControlThietBi9.loaiThietBi = DayChuyenHPEX.DieuKhien.LoaiThietBi.MAY;
            this.userControlThietBi9.Location = new System.Drawing.Point(123, 150);
            this.userControlThietBi9.m_batHopThoaiRun = true;
            this.userControlThietBi9.m_choPhepMoPhong = true;
            this.userControlThietBi9.m_Description = "";
            this.userControlThietBi9.m_dichbitGiamSat = 1;
            this.userControlThietBi9.m_dichbitStart = 0;
            this.userControlThietBi9.m_dichbitStop = 1;
            this.userControlThietBi9.m_gocquay = System.Drawing.RotateFlipType.Rotate180FlipY;
            this.userControlThietBi9.m_imgNomal = global::DayChuyenHPEX.Properties.Resources.Van_Stop;
            this.userControlThietBi9.m_imgRun1 = global::DayChuyenHPEX.Properties.Resources._11;
            this.userControlThietBi9.m_imgRun2 = null;
            this.userControlThietBi9.m_imgStop = global::DayChuyenHPEX.Properties.Resources.Van_Stop;
            this.userControlThietBi9.m_Location = new System.Drawing.Point(123, 150);
            this.userControlThietBi9.m_maThietBi = "";
            this.userControlThietBi9.m_sImgNomal = null;
            this.userControlThietBi9.m_sImgRun1 = null;
            this.userControlThietBi9.m_sImgRun2 = null;
            this.userControlThietBi9.m_sImgStop = null;
            this.userControlThietBi9.m_size = new System.Drawing.Size(70, 39);
            this.userControlThietBi9.m_Visible = true;
            this.userControlThietBi9.m_vitriPLCGiamSat = 1;
            this.userControlThietBi9.m_vitriPLCStart = 1;
            this.userControlThietBi9.m_vitriPLCStop = 1;
            this.userControlThietBi9.Name = "userControlThietBi9";
            this.userControlThietBi9.plc = null;
            this.userControlThietBi9.quyenQuantri = false;
            this.userControlThietBi9.Size = new System.Drawing.Size(70, 39);
            this.userControlThietBi9.TabIndex = 190;
            this.userControlThietBi9.tenthuocTinh = "M03";
            // 
            // userControlThietBi6
            // 
            this.userControlThietBi6.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.userControlThietBi6.BackColor = System.Drawing.Color.Transparent;
            this.userControlThietBi6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.userControlThietBi6.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.userControlThietBi6.enable = true;
            this.userControlThietBi6.hienThitenThuocTinh = false;
            this.userControlThietBi6.loaiThietBi = DayChuyenHPEX.DieuKhien.LoaiThietBi.MAY;
            this.userControlThietBi6.Location = new System.Drawing.Point(857, 246);
            this.userControlThietBi6.m_batHopThoaiRun = true;
            this.userControlThietBi6.m_choPhepMoPhong = true;
            this.userControlThietBi6.m_Description = "";
            this.userControlThietBi6.m_dichbitGiamSat = 1;
            this.userControlThietBi6.m_dichbitStart = 0;
            this.userControlThietBi6.m_dichbitStop = 1;
            this.userControlThietBi6.m_gocquay = System.Drawing.RotateFlipType.Rotate270FlipXY;
            this.userControlThietBi6.m_imgNomal = global::DayChuyenHPEX.Properties.Resources.Van_Stop;
            this.userControlThietBi6.m_imgRun1 = global::DayChuyenHPEX.Properties.Resources._11;
            this.userControlThietBi6.m_imgRun2 = null;
            this.userControlThietBi6.m_imgStop = global::DayChuyenHPEX.Properties.Resources.Van_Stop;
            this.userControlThietBi6.m_Location = new System.Drawing.Point(857, 246);
            this.userControlThietBi6.m_maThietBi = "";
            this.userControlThietBi6.m_sImgNomal = null;
            this.userControlThietBi6.m_sImgRun1 = null;
            this.userControlThietBi6.m_sImgRun2 = null;
            this.userControlThietBi6.m_sImgStop = null;
            this.userControlThietBi6.m_size = new System.Drawing.Size(40, 84);
            this.userControlThietBi6.m_Visible = true;
            this.userControlThietBi6.m_vitriPLCGiamSat = 1;
            this.userControlThietBi6.m_vitriPLCStart = 1;
            this.userControlThietBi6.m_vitriPLCStop = 1;
            this.userControlThietBi6.Name = "userControlThietBi6";
            this.userControlThietBi6.plc = null;
            this.userControlThietBi6.quyenQuantri = false;
            this.userControlThietBi6.Size = new System.Drawing.Size(40, 84);
            this.userControlThietBi6.TabIndex = 187;
            this.userControlThietBi6.tenthuocTinh = "M03";
            // 
            // userControlThietBi5
            // 
            this.userControlThietBi5.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.userControlThietBi5.BackColor = System.Drawing.Color.Transparent;
            this.userControlThietBi5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.userControlThietBi5.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.userControlThietBi5.enable = true;
            this.userControlThietBi5.hienThitenThuocTinh = false;
            this.userControlThietBi5.loaiThietBi = DayChuyenHPEX.DieuKhien.LoaiThietBi.MAY;
            this.userControlThietBi5.Location = new System.Drawing.Point(949, 163);
            this.userControlThietBi5.m_batHopThoaiRun = true;
            this.userControlThietBi5.m_choPhepMoPhong = true;
            this.userControlThietBi5.m_Description = "";
            this.userControlThietBi5.m_dichbitGiamSat = 1;
            this.userControlThietBi5.m_dichbitStart = 0;
            this.userControlThietBi5.m_dichbitStop = 1;
            this.userControlThietBi5.m_gocquay = System.Drawing.RotateFlipType.Rotate270FlipX;
            this.userControlThietBi5.m_imgNomal = global::DayChuyenHPEX.Properties.Resources.Van_Stop;
            this.userControlThietBi5.m_imgRun1 = global::DayChuyenHPEX.Properties.Resources._11;
            this.userControlThietBi5.m_imgRun2 = null;
            this.userControlThietBi5.m_imgStop = global::DayChuyenHPEX.Properties.Resources.Van_Stop;
            this.userControlThietBi5.m_Location = new System.Drawing.Point(949, 163);
            this.userControlThietBi5.m_maThietBi = "";
            this.userControlThietBi5.m_sImgNomal = null;
            this.userControlThietBi5.m_sImgRun1 = null;
            this.userControlThietBi5.m_sImgRun2 = null;
            this.userControlThietBi5.m_sImgStop = null;
            this.userControlThietBi5.m_size = new System.Drawing.Size(40, 84);
            this.userControlThietBi5.m_Visible = true;
            this.userControlThietBi5.m_vitriPLCGiamSat = 1;
            this.userControlThietBi5.m_vitriPLCStart = 1;
            this.userControlThietBi5.m_vitriPLCStop = 1;
            this.userControlThietBi5.Name = "userControlThietBi5";
            this.userControlThietBi5.plc = null;
            this.userControlThietBi5.quyenQuantri = false;
            this.userControlThietBi5.Size = new System.Drawing.Size(40, 84);
            this.userControlThietBi5.TabIndex = 186;
            this.userControlThietBi5.tenthuocTinh = "M03";
            // 
            // LoiChung
            // 
            this.LoiChung.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.LoiChung.BackColor = System.Drawing.Color.Transparent;
            this.LoiChung.beep = false;
            this.LoiChung.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LoiChung.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.LoiChung.dichbitPLC = 1;
            this.LoiChung.enable = true;
            this.LoiChung.Location = new System.Drawing.Point(3, 756);
            this.LoiChung.loi = false;
            this.LoiChung.m_Location = new System.Drawing.Point(3, 756);
            this.LoiChung.m_size = new System.Drawing.Size(217, 40);
            this.LoiChung.m_sTextLoi = "Loi chung";
            this.LoiChung.m_Visible = true;
            this.LoiChung.Name = "LoiChung";
            this.LoiChung.plc = null;
            this.LoiChung.quyenQuantri = false;
            this.LoiChung.Size = new System.Drawing.Size(217, 40);
            this.LoiChung.strPLCVaMLoi = "";
            this.LoiChung.TabIndex = 185;
            this.LoiChung.tenthuocTinh = "Nhiệt độ";
            this.LoiChung.vitriPLC = 0;
            // 
            // userControlThietBi4
            // 
            this.userControlThietBi4.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.userControlThietBi4.BackColor = System.Drawing.Color.Transparent;
            this.userControlThietBi4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.userControlThietBi4.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.userControlThietBi4.enable = true;
            this.userControlThietBi4.hienThitenThuocTinh = false;
            this.userControlThietBi4.loaiThietBi = DayChuyenHPEX.DieuKhien.LoaiThietBi.MAY;
            this.userControlThietBi4.Location = new System.Drawing.Point(477, 554);
            this.userControlThietBi4.m_batHopThoaiRun = true;
            this.userControlThietBi4.m_choPhepMoPhong = false;
            this.userControlThietBi4.m_Description = "";
            this.userControlThietBi4.m_dichbitGiamSat = 1;
            this.userControlThietBi4.m_dichbitStart = 0;
            this.userControlThietBi4.m_dichbitStop = 1;
            this.userControlThietBi4.m_gocquay = System.Drawing.RotateFlipType.Rotate270FlipXY;
            this.userControlThietBi4.m_imgNomal = global::DayChuyenHPEX.Properties.Resources.Van_Stop;
            this.userControlThietBi4.m_imgRun1 = global::DayChuyenHPEX.Properties.Resources._11;
            this.userControlThietBi4.m_imgRun2 = null;
            this.userControlThietBi4.m_imgStop = global::DayChuyenHPEX.Properties.Resources.Van_Stop;
            this.userControlThietBi4.m_Location = new System.Drawing.Point(477, 554);
            this.userControlThietBi4.m_maThietBi = "";
            this.userControlThietBi4.m_sImgNomal = null;
            this.userControlThietBi4.m_sImgRun1 = null;
            this.userControlThietBi4.m_sImgRun2 = null;
            this.userControlThietBi4.m_sImgStop = null;
            this.userControlThietBi4.m_size = new System.Drawing.Size(40, 84);
            this.userControlThietBi4.m_Visible = true;
            this.userControlThietBi4.m_vitriPLCGiamSat = 1;
            this.userControlThietBi4.m_vitriPLCStart = 1;
            this.userControlThietBi4.m_vitriPLCStop = 1;
            this.userControlThietBi4.Name = "userControlThietBi4";
            this.userControlThietBi4.plc = null;
            this.userControlThietBi4.quyenQuantri = false;
            this.userControlThietBi4.Size = new System.Drawing.Size(40, 84);
            this.userControlThietBi4.TabIndex = 184;
            this.userControlThietBi4.tenthuocTinh = "M03";
            // 
            // userControlThietBi3
            // 
            this.userControlThietBi3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.userControlThietBi3.BackColor = System.Drawing.Color.Transparent;
            this.userControlThietBi3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.userControlThietBi3.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.userControlThietBi3.enable = true;
            this.userControlThietBi3.hienThitenThuocTinh = false;
            this.userControlThietBi3.loaiThietBi = DayChuyenHPEX.DieuKhien.LoaiThietBi.MAY;
            this.userControlThietBi3.Location = new System.Drawing.Point(376, 385);
            this.userControlThietBi3.m_batHopThoaiRun = true;
            this.userControlThietBi3.m_choPhepMoPhong = false;
            this.userControlThietBi3.m_Description = "";
            this.userControlThietBi3.m_dichbitGiamSat = 1;
            this.userControlThietBi3.m_dichbitStart = 0;
            this.userControlThietBi3.m_dichbitStop = 1;
            this.userControlThietBi3.m_gocquay = System.Drawing.RotateFlipType.Rotate270FlipXY;
            this.userControlThietBi3.m_imgNomal = global::DayChuyenHPEX.Properties.Resources.Van_Stop;
            this.userControlThietBi3.m_imgRun1 = global::DayChuyenHPEX.Properties.Resources._11;
            this.userControlThietBi3.m_imgRun2 = null;
            this.userControlThietBi3.m_imgStop = global::DayChuyenHPEX.Properties.Resources.Van_Stop;
            this.userControlThietBi3.m_Location = new System.Drawing.Point(376, 385);
            this.userControlThietBi3.m_maThietBi = "";
            this.userControlThietBi3.m_sImgNomal = null;
            this.userControlThietBi3.m_sImgRun1 = null;
            this.userControlThietBi3.m_sImgRun2 = null;
            this.userControlThietBi3.m_sImgStop = null;
            this.userControlThietBi3.m_size = new System.Drawing.Size(40, 84);
            this.userControlThietBi3.m_Visible = true;
            this.userControlThietBi3.m_vitriPLCGiamSat = 1;
            this.userControlThietBi3.m_vitriPLCStart = 1;
            this.userControlThietBi3.m_vitriPLCStop = 1;
            this.userControlThietBi3.Name = "userControlThietBi3";
            this.userControlThietBi3.plc = null;
            this.userControlThietBi3.quyenQuantri = false;
            this.userControlThietBi3.Size = new System.Drawing.Size(40, 84);
            this.userControlThietBi3.TabIndex = 183;
            this.userControlThietBi3.tenthuocTinh = "M03";
            // 
            // ucPLCChinh
            // 
            this.ucPLCChinh.BackColor = System.Drawing.Color.Transparent;
            this.ucPLCChinh.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.ucPLCChinh.enable = true;
            this.ucPLCChinh.Location = new System.Drawing.Point(-1, 0);
            this.ucPLCChinh.m_batHopThoaiRun = true;
            this.ucPLCChinh.m_Location = new System.Drawing.Point(-1, 0);
            this.ucPLCChinh.m_size = new System.Drawing.Size(122, 46);
            this.ucPLCChinh.m_Visible = true;
            this.ucPLCChinh.Name = "ucPLCChinh";
            this.ucPLCChinh.nSizeM = 1000;
            this.ucPLCChinh.nSizeQ = 100;
            plcS712001.tenPLC = "PLC";
            this.ucPLCChinh.plc = plcS712001;
            this.ucPLCChinh.plcIp = "192.168.1.7";
            this.ucPLCChinh.quyenQuantri = true;
            this.ucPLCChinh.Size = new System.Drawing.Size(122, 46);
            this.ucPLCChinh.TabIndex = 150;
            this.ucPLCChinh.tenthuocTinh = "192.168.1.6";
            this.ucPLCChinh.trangThaiKetNoi = false;
            // 
            // userControlThietBi2
            // 
            this.userControlThietBi2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.userControlThietBi2.BackColor = System.Drawing.Color.Transparent;
            this.userControlThietBi2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.userControlThietBi2.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.userControlThietBi2.enable = true;
            this.userControlThietBi2.hienThitenThuocTinh = false;
            this.userControlThietBi2.loaiThietBi = DayChuyenHPEX.DieuKhien.LoaiThietBi.MAY;
            this.userControlThietBi2.Location = new System.Drawing.Point(780, 612);
            this.userControlThietBi2.m_batHopThoaiRun = true;
            this.userControlThietBi2.m_choPhepMoPhong = false;
            this.userControlThietBi2.m_Description = "";
            this.userControlThietBi2.m_dichbitGiamSat = 1;
            this.userControlThietBi2.m_dichbitStart = 0;
            this.userControlThietBi2.m_dichbitStop = 1;
            this.userControlThietBi2.m_gocquay = System.Drawing.RotateFlipType.Rotate270FlipXY;
            this.userControlThietBi2.m_imgNomal = global::DayChuyenHPEX.Properties.Resources.Van_Stop;
            this.userControlThietBi2.m_imgRun1 = global::DayChuyenHPEX.Properties.Resources._11;
            this.userControlThietBi2.m_imgRun2 = null;
            this.userControlThietBi2.m_imgStop = global::DayChuyenHPEX.Properties.Resources.Van_Stop;
            this.userControlThietBi2.m_Location = new System.Drawing.Point(780, 612);
            this.userControlThietBi2.m_maThietBi = "";
            this.userControlThietBi2.m_sImgNomal = null;
            this.userControlThietBi2.m_sImgRun1 = null;
            this.userControlThietBi2.m_sImgRun2 = null;
            this.userControlThietBi2.m_sImgStop = null;
            this.userControlThietBi2.m_size = new System.Drawing.Size(40, 84);
            this.userControlThietBi2.m_Visible = true;
            this.userControlThietBi2.m_vitriPLCGiamSat = 1;
            this.userControlThietBi2.m_vitriPLCStart = 1;
            this.userControlThietBi2.m_vitriPLCStop = 1;
            this.userControlThietBi2.Name = "userControlThietBi2";
            this.userControlThietBi2.plc = null;
            this.userControlThietBi2.quyenQuantri = false;
            this.userControlThietBi2.Size = new System.Drawing.Size(40, 84);
            this.userControlThietBi2.TabIndex = 142;
            this.userControlThietBi2.tenthuocTinh = "M03";
            // 
            // VanCoDac
            // 
            this.VanCoDac.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.VanCoDac.BackColor = System.Drawing.Color.Transparent;
            this.VanCoDac.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.VanCoDac.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.VanCoDac.enable = true;
            this.VanCoDac.hienThitenThuocTinh = false;
            this.VanCoDac.loaiThietBi = DayChuyenHPEX.DieuKhien.LoaiThietBi.MAY;
            this.VanCoDac.Location = new System.Drawing.Point(211, 537);
            this.VanCoDac.m_batHopThoaiRun = true;
            this.VanCoDac.m_choPhepMoPhong = false;
            this.VanCoDac.m_Description = "";
            this.VanCoDac.m_dichbitGiamSat = 1;
            this.VanCoDac.m_dichbitStart = 0;
            this.VanCoDac.m_dichbitStop = 1;
            this.VanCoDac.m_gocquay = System.Drawing.RotateFlipType.Rotate270FlipXY;
            this.VanCoDac.m_imgNomal = global::DayChuyenHPEX.Properties.Resources.Van_Stop;
            this.VanCoDac.m_imgRun1 = global::DayChuyenHPEX.Properties.Resources._11;
            this.VanCoDac.m_imgRun2 = null;
            this.VanCoDac.m_imgStop = global::DayChuyenHPEX.Properties.Resources.Van_Stop;
            this.VanCoDac.m_Location = new System.Drawing.Point(211, 537);
            this.VanCoDac.m_maThietBi = "";
            this.VanCoDac.m_sImgNomal = null;
            this.VanCoDac.m_sImgRun1 = null;
            this.VanCoDac.m_sImgRun2 = null;
            this.VanCoDac.m_sImgStop = null;
            this.VanCoDac.m_size = new System.Drawing.Size(40, 84);
            this.VanCoDac.m_Visible = true;
            this.VanCoDac.m_vitriPLCGiamSat = 1;
            this.VanCoDac.m_vitriPLCStart = 1;
            this.VanCoDac.m_vitriPLCStop = 1;
            this.VanCoDac.Name = "VanCoDac";
            this.VanCoDac.plc = null;
            this.VanCoDac.quyenQuantri = false;
            this.VanCoDac.Size = new System.Drawing.Size(40, 84);
            this.VanCoDac.TabIndex = 142;
            this.VanCoDac.tenthuocTinh = "M03";
            // 
            // DongCoVitTaiMayDongBao
            // 
            this.DongCoVitTaiMayDongBao.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.DongCoVitTaiMayDongBao.BackColor = System.Drawing.Color.Transparent;
            this.DongCoVitTaiMayDongBao.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.DongCoVitTaiMayDongBao.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.DongCoVitTaiMayDongBao.enable = true;
            this.DongCoVitTaiMayDongBao.hienThitenThuocTinh = false;
            this.DongCoVitTaiMayDongBao.loaiThietBi = DayChuyenHPEX.DieuKhien.LoaiThietBi.MAY;
            this.DongCoVitTaiMayDongBao.Location = new System.Drawing.Point(1170, 625);
            this.DongCoVitTaiMayDongBao.m_batHopThoaiRun = true;
            this.DongCoVitTaiMayDongBao.m_choPhepMoPhong = false;
            this.DongCoVitTaiMayDongBao.m_Description = "";
            this.DongCoVitTaiMayDongBao.m_dichbitGiamSat = 1;
            this.DongCoVitTaiMayDongBao.m_dichbitStart = 0;
            this.DongCoVitTaiMayDongBao.m_dichbitStop = 1;
            this.DongCoVitTaiMayDongBao.m_gocquay = System.Drawing.RotateFlipType.RotateNoneFlipNone;
            this.DongCoVitTaiMayDongBao.m_imgNomal = global::DayChuyenHPEX.Properties.Resources.vit_tai_stop;
            this.DongCoVitTaiMayDongBao.m_imgRun1 = global::DayChuyenHPEX.Properties.Resources.vit_tai_start;
            this.DongCoVitTaiMayDongBao.m_imgRun2 = null;
            this.DongCoVitTaiMayDongBao.m_imgStop = global::DayChuyenHPEX.Properties.Resources.vit_tai_stop;
            this.DongCoVitTaiMayDongBao.m_Location = new System.Drawing.Point(1170, 625);
            this.DongCoVitTaiMayDongBao.m_maThietBi = "";
            this.DongCoVitTaiMayDongBao.m_sImgNomal = null;
            this.DongCoVitTaiMayDongBao.m_sImgRun1 = null;
            this.DongCoVitTaiMayDongBao.m_sImgRun2 = null;
            this.DongCoVitTaiMayDongBao.m_sImgStop = null;
            this.DongCoVitTaiMayDongBao.m_size = new System.Drawing.Size(40, 29);
            this.DongCoVitTaiMayDongBao.m_Visible = true;
            this.DongCoVitTaiMayDongBao.m_vitriPLCGiamSat = 1;
            this.DongCoVitTaiMayDongBao.m_vitriPLCStart = 1;
            this.DongCoVitTaiMayDongBao.m_vitriPLCStop = 1;
            this.DongCoVitTaiMayDongBao.Name = "DongCoVitTaiMayDongBao";
            this.DongCoVitTaiMayDongBao.plc = null;
            this.DongCoVitTaiMayDongBao.quyenQuantri = false;
            this.DongCoVitTaiMayDongBao.Size = new System.Drawing.Size(40, 29);
            this.DongCoVitTaiMayDongBao.TabIndex = 149;
            this.DongCoVitTaiMayDongBao.tenthuocTinh = "M03";
            // 
            // DongCoVitTaiMayTronPhai
            // 
            this.DongCoVitTaiMayTronPhai.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.DongCoVitTaiMayTronPhai.BackColor = System.Drawing.Color.Transparent;
            this.DongCoVitTaiMayTronPhai.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.DongCoVitTaiMayTronPhai.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.DongCoVitTaiMayTronPhai.enable = true;
            this.DongCoVitTaiMayTronPhai.hienThitenThuocTinh = false;
            this.DongCoVitTaiMayTronPhai.loaiThietBi = DayChuyenHPEX.DieuKhien.LoaiThietBi.MAY;
            this.DongCoVitTaiMayTronPhai.Location = new System.Drawing.Point(1283, 289);
            this.DongCoVitTaiMayTronPhai.m_batHopThoaiRun = true;
            this.DongCoVitTaiMayTronPhai.m_choPhepMoPhong = false;
            this.DongCoVitTaiMayTronPhai.m_Description = "";
            this.DongCoVitTaiMayTronPhai.m_dichbitGiamSat = 1;
            this.DongCoVitTaiMayTronPhai.m_dichbitStart = 0;
            this.DongCoVitTaiMayTronPhai.m_dichbitStop = 1;
            this.DongCoVitTaiMayTronPhai.m_gocquay = System.Drawing.RotateFlipType.RotateNoneFlipNone;
            this.DongCoVitTaiMayTronPhai.m_imgNomal = global::DayChuyenHPEX.Properties.Resources.vit_tai_stop;
            this.DongCoVitTaiMayTronPhai.m_imgRun1 = global::DayChuyenHPEX.Properties.Resources.vit_tai_start;
            this.DongCoVitTaiMayTronPhai.m_imgRun2 = null;
            this.DongCoVitTaiMayTronPhai.m_imgStop = global::DayChuyenHPEX.Properties.Resources.vit_tai_stop;
            this.DongCoVitTaiMayTronPhai.m_Location = new System.Drawing.Point(1283, 289);
            this.DongCoVitTaiMayTronPhai.m_maThietBi = "";
            this.DongCoVitTaiMayTronPhai.m_sImgNomal = null;
            this.DongCoVitTaiMayTronPhai.m_sImgRun1 = null;
            this.DongCoVitTaiMayTronPhai.m_sImgRun2 = null;
            this.DongCoVitTaiMayTronPhai.m_sImgStop = null;
            this.DongCoVitTaiMayTronPhai.m_size = new System.Drawing.Size(40, 29);
            this.DongCoVitTaiMayTronPhai.m_Visible = true;
            this.DongCoVitTaiMayTronPhai.m_vitriPLCGiamSat = 1;
            this.DongCoVitTaiMayTronPhai.m_vitriPLCStart = 1;
            this.DongCoVitTaiMayTronPhai.m_vitriPLCStop = 1;
            this.DongCoVitTaiMayTronPhai.Name = "DongCoVitTaiMayTronPhai";
            this.DongCoVitTaiMayTronPhai.plc = null;
            this.DongCoVitTaiMayTronPhai.quyenQuantri = false;
            this.DongCoVitTaiMayTronPhai.Size = new System.Drawing.Size(40, 29);
            this.DongCoVitTaiMayTronPhai.TabIndex = 148;
            this.DongCoVitTaiMayTronPhai.tenthuocTinh = "M03";
            // 
            // DongCoVitTaiMayTronTrai
            // 
            this.DongCoVitTaiMayTronTrai.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.DongCoVitTaiMayTronTrai.BackColor = System.Drawing.Color.Transparent;
            this.DongCoVitTaiMayTronTrai.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.DongCoVitTaiMayTronTrai.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.DongCoVitTaiMayTronTrai.enable = true;
            this.DongCoVitTaiMayTronTrai.hienThitenThuocTinh = false;
            this.DongCoVitTaiMayTronTrai.loaiThietBi = DayChuyenHPEX.DieuKhien.LoaiThietBi.MAY;
            this.DongCoVitTaiMayTronTrai.Location = new System.Drawing.Point(655, 290);
            this.DongCoVitTaiMayTronTrai.m_batHopThoaiRun = true;
            this.DongCoVitTaiMayTronTrai.m_choPhepMoPhong = false;
            this.DongCoVitTaiMayTronTrai.m_Description = "";
            this.DongCoVitTaiMayTronTrai.m_dichbitGiamSat = 1;
            this.DongCoVitTaiMayTronTrai.m_dichbitStart = 0;
            this.DongCoVitTaiMayTronTrai.m_dichbitStop = 1;
            this.DongCoVitTaiMayTronTrai.m_gocquay = System.Drawing.RotateFlipType.RotateNoneFlipNone;
            this.DongCoVitTaiMayTronTrai.m_imgNomal = global::DayChuyenHPEX.Properties.Resources.vit_tai_stop;
            this.DongCoVitTaiMayTronTrai.m_imgRun1 = global::DayChuyenHPEX.Properties.Resources.vit_tai_start;
            this.DongCoVitTaiMayTronTrai.m_imgRun2 = null;
            this.DongCoVitTaiMayTronTrai.m_imgStop = global::DayChuyenHPEX.Properties.Resources.vit_tai_stop;
            this.DongCoVitTaiMayTronTrai.m_Location = new System.Drawing.Point(655, 290);
            this.DongCoVitTaiMayTronTrai.m_maThietBi = "";
            this.DongCoVitTaiMayTronTrai.m_sImgNomal = null;
            this.DongCoVitTaiMayTronTrai.m_sImgRun1 = null;
            this.DongCoVitTaiMayTronTrai.m_sImgRun2 = null;
            this.DongCoVitTaiMayTronTrai.m_sImgStop = null;
            this.DongCoVitTaiMayTronTrai.m_size = new System.Drawing.Size(40, 29);
            this.DongCoVitTaiMayTronTrai.m_Visible = true;
            this.DongCoVitTaiMayTronTrai.m_vitriPLCGiamSat = 1;
            this.DongCoVitTaiMayTronTrai.m_vitriPLCStart = 1;
            this.DongCoVitTaiMayTronTrai.m_vitriPLCStop = 1;
            this.DongCoVitTaiMayTronTrai.Name = "DongCoVitTaiMayTronTrai";
            this.DongCoVitTaiMayTronTrai.plc = null;
            this.DongCoVitTaiMayTronTrai.quyenQuantri = false;
            this.DongCoVitTaiMayTronTrai.Size = new System.Drawing.Size(40, 29);
            this.DongCoVitTaiMayTronTrai.TabIndex = 147;
            this.DongCoVitTaiMayTronTrai.tenthuocTinh = "M03";
            // 
            // BomDungDich01
            // 
            this.BomDungDich01.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BomDungDich01.BackColor = System.Drawing.Color.Transparent;
            this.BomDungDich01.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.BomDungDich01.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.BomDungDich01.enable = true;
            this.BomDungDich01.hienThitenThuocTinh = false;
            this.BomDungDich01.loaiThietBi = DayChuyenHPEX.DieuKhien.LoaiThietBi.MAY;
            this.BomDungDich01.Location = new System.Drawing.Point(68, 307);
            this.BomDungDich01.m_batHopThoaiRun = true;
            this.BomDungDich01.m_choPhepMoPhong = false;
            this.BomDungDich01.m_Description = "";
            this.BomDungDich01.m_dichbitGiamSat = 1;
            this.BomDungDich01.m_dichbitStart = 0;
            this.BomDungDich01.m_dichbitStop = 1;
            this.BomDungDich01.m_gocquay = System.Drawing.RotateFlipType.RotateNoneFlipNone;
            this.BomDungDich01.m_imgNomal = global::DayChuyenHPEX.Properties.Resources.Bom_STOP1;
            this.BomDungDich01.m_imgRun1 = global::DayChuyenHPEX.Properties.Resources.Bom_RUN;
            this.BomDungDich01.m_imgRun2 = null;
            this.BomDungDich01.m_imgStop = global::DayChuyenHPEX.Properties.Resources.Bom_STOP1;
            this.BomDungDich01.m_Location = new System.Drawing.Point(68, 307);
            this.BomDungDich01.m_maThietBi = "";
            this.BomDungDich01.m_sImgNomal = null;
            this.BomDungDich01.m_sImgRun1 = null;
            this.BomDungDich01.m_sImgRun2 = null;
            this.BomDungDich01.m_sImgStop = null;
            this.BomDungDich01.m_size = new System.Drawing.Size(92, 97);
            this.BomDungDich01.m_Visible = true;
            this.BomDungDich01.m_vitriPLCGiamSat = 1;
            this.BomDungDich01.m_vitriPLCStart = 1;
            this.BomDungDich01.m_vitriPLCStop = 1;
            this.BomDungDich01.Name = "BomDungDich01";
            this.BomDungDich01.plc = null;
            this.BomDungDich01.quyenQuantri = false;
            this.BomDungDich01.Size = new System.Drawing.Size(92, 97);
            this.BomDungDich01.TabIndex = 146;
            this.BomDungDich01.tenthuocTinh = "M03";
            // 
            // BomLenCoDac
            // 
            this.BomLenCoDac.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BomLenCoDac.BackColor = System.Drawing.Color.Transparent;
            this.BomLenCoDac.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.BomLenCoDac.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.BomLenCoDac.enable = true;
            this.BomLenCoDac.hienThitenThuocTinh = false;
            this.BomLenCoDac.loaiThietBi = DayChuyenHPEX.DieuKhien.LoaiThietBi.MAY;
            this.BomLenCoDac.Location = new System.Drawing.Point(288, 554);
            this.BomLenCoDac.m_batHopThoaiRun = true;
            this.BomLenCoDac.m_choPhepMoPhong = false;
            this.BomLenCoDac.m_Description = "";
            this.BomLenCoDac.m_dichbitGiamSat = 1;
            this.BomLenCoDac.m_dichbitStart = 0;
            this.BomLenCoDac.m_dichbitStop = 1;
            this.BomLenCoDac.m_gocquay = System.Drawing.RotateFlipType.RotateNoneFlipNone;
            this.BomLenCoDac.m_imgNomal = global::DayChuyenHPEX.Properties.Resources.Bom_STOP1;
            this.BomLenCoDac.m_imgRun1 = global::DayChuyenHPEX.Properties.Resources.Bom_RUN;
            this.BomLenCoDac.m_imgRun2 = null;
            this.BomLenCoDac.m_imgStop = global::DayChuyenHPEX.Properties.Resources.Bom_STOP1;
            this.BomLenCoDac.m_Location = new System.Drawing.Point(288, 554);
            this.BomLenCoDac.m_maThietBi = "";
            this.BomLenCoDac.m_sImgNomal = null;
            this.BomLenCoDac.m_sImgRun1 = null;
            this.BomLenCoDac.m_sImgRun2 = null;
            this.BomLenCoDac.m_sImgStop = null;
            this.BomLenCoDac.m_size = new System.Drawing.Size(92, 97);
            this.BomLenCoDac.m_Visible = true;
            this.BomLenCoDac.m_vitriPLCGiamSat = 1;
            this.BomLenCoDac.m_vitriPLCStart = 1;
            this.BomLenCoDac.m_vitriPLCStop = 1;
            this.BomLenCoDac.Name = "BomLenCoDac";
            this.BomLenCoDac.plc = null;
            this.BomLenCoDac.quyenQuantri = false;
            this.BomLenCoDac.Size = new System.Drawing.Size(92, 97);
            this.BomLenCoDac.TabIndex = 145;
            this.BomLenCoDac.tenthuocTinh = "M03";
            // 
            // BomBanhRangLenMayTaoHat
            // 
            this.BomBanhRangLenMayTaoHat.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BomBanhRangLenMayTaoHat.BackColor = System.Drawing.Color.Transparent;
            this.BomBanhRangLenMayTaoHat.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.BomBanhRangLenMayTaoHat.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.BomBanhRangLenMayTaoHat.enable = true;
            this.BomBanhRangLenMayTaoHat.hienThitenThuocTinh = false;
            this.BomBanhRangLenMayTaoHat.loaiThietBi = DayChuyenHPEX.DieuKhien.LoaiThietBi.MAY;
            this.BomBanhRangLenMayTaoHat.Location = new System.Drawing.Point(593, 579);
            this.BomBanhRangLenMayTaoHat.m_batHopThoaiRun = true;
            this.BomBanhRangLenMayTaoHat.m_choPhepMoPhong = false;
            this.BomBanhRangLenMayTaoHat.m_Description = "";
            this.BomBanhRangLenMayTaoHat.m_dichbitGiamSat = 1;
            this.BomBanhRangLenMayTaoHat.m_dichbitStart = 0;
            this.BomBanhRangLenMayTaoHat.m_dichbitStop = 1;
            this.BomBanhRangLenMayTaoHat.m_gocquay = System.Drawing.RotateFlipType.RotateNoneFlipNone;
            this.BomBanhRangLenMayTaoHat.m_imgNomal = global::DayChuyenHPEX.Properties.Resources.Bom_STOP1;
            this.BomBanhRangLenMayTaoHat.m_imgRun1 = global::DayChuyenHPEX.Properties.Resources.Bom_RUN;
            this.BomBanhRangLenMayTaoHat.m_imgRun2 = null;
            this.BomBanhRangLenMayTaoHat.m_imgStop = global::DayChuyenHPEX.Properties.Resources.Bom_STOP1;
            this.BomBanhRangLenMayTaoHat.m_Location = new System.Drawing.Point(593, 579);
            this.BomBanhRangLenMayTaoHat.m_maThietBi = "";
            this.BomBanhRangLenMayTaoHat.m_sImgNomal = null;
            this.BomBanhRangLenMayTaoHat.m_sImgRun1 = null;
            this.BomBanhRangLenMayTaoHat.m_sImgRun2 = null;
            this.BomBanhRangLenMayTaoHat.m_sImgStop = null;
            this.BomBanhRangLenMayTaoHat.m_size = new System.Drawing.Size(92, 97);
            this.BomBanhRangLenMayTaoHat.m_Visible = true;
            this.BomBanhRangLenMayTaoHat.m_vitriPLCGiamSat = 1;
            this.BomBanhRangLenMayTaoHat.m_vitriPLCStart = 1;
            this.BomBanhRangLenMayTaoHat.m_vitriPLCStop = 1;
            this.BomBanhRangLenMayTaoHat.Name = "BomBanhRangLenMayTaoHat";
            this.BomBanhRangLenMayTaoHat.plc = null;
            this.BomBanhRangLenMayTaoHat.quyenQuantri = false;
            this.BomBanhRangLenMayTaoHat.Size = new System.Drawing.Size(92, 97);
            this.BomBanhRangLenMayTaoHat.TabIndex = 144;
            this.BomBanhRangLenMayTaoHat.tenthuocTinh = "M03";
            // 
            // userControlThietBiBOM
            // 
            this.userControlThietBiBOM.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.userControlThietBiBOM.BackColor = System.Drawing.Color.Transparent;
            this.userControlThietBiBOM.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.userControlThietBiBOM.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.userControlThietBiBOM.enable = true;
            this.userControlThietBiBOM.hienThitenThuocTinh = false;
            this.userControlThietBiBOM.loaiThietBi = DayChuyenHPEX.DieuKhien.LoaiThietBi.MAY;
            this.userControlThietBiBOM.Location = new System.Drawing.Point(877, 630);
            this.userControlThietBiBOM.m_batHopThoaiRun = true;
            this.userControlThietBiBOM.m_choPhepMoPhong = false;
            this.userControlThietBiBOM.m_Description = "";
            this.userControlThietBiBOM.m_dichbitGiamSat = 1;
            this.userControlThietBiBOM.m_dichbitStart = 0;
            this.userControlThietBiBOM.m_dichbitStop = 1;
            this.userControlThietBiBOM.m_gocquay = System.Drawing.RotateFlipType.RotateNoneFlipNone;
            this.userControlThietBiBOM.m_imgNomal = global::DayChuyenHPEX.Properties.Resources.Bom_STOP1;
            this.userControlThietBiBOM.m_imgRun1 = global::DayChuyenHPEX.Properties.Resources.Bom_RUN;
            this.userControlThietBiBOM.m_imgRun2 = null;
            this.userControlThietBiBOM.m_imgStop = global::DayChuyenHPEX.Properties.Resources.Bom_STOP1;
            this.userControlThietBiBOM.m_Location = new System.Drawing.Point(877, 630);
            this.userControlThietBiBOM.m_maThietBi = "";
            this.userControlThietBiBOM.m_sImgNomal = null;
            this.userControlThietBiBOM.m_sImgRun1 = null;
            this.userControlThietBiBOM.m_sImgRun2 = null;
            this.userControlThietBiBOM.m_sImgStop = null;
            this.userControlThietBiBOM.m_size = new System.Drawing.Size(92, 97);
            this.userControlThietBiBOM.m_Visible = true;
            this.userControlThietBiBOM.m_vitriPLCGiamSat = 1;
            this.userControlThietBiBOM.m_vitriPLCStart = 1;
            this.userControlThietBiBOM.m_vitriPLCStop = 1;
            this.userControlThietBiBOM.Name = "userControlThietBiBOM";
            this.userControlThietBiBOM.plc = null;
            this.userControlThietBiBOM.quyenQuantri = false;
            this.userControlThietBiBOM.Size = new System.Drawing.Size(92, 97);
            this.userControlThietBiBOM.TabIndex = 143;
            this.userControlThietBiBOM.tenthuocTinh = "M03";
            // 
            // DongCoMayBocVo
            // 
            this.DongCoMayBocVo.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.DongCoMayBocVo.BackColor = System.Drawing.Color.Transparent;
            this.DongCoMayBocVo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.DongCoMayBocVo.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.DongCoMayBocVo.enable = true;
            this.DongCoMayBocVo.hienThitenThuocTinh = false;
            this.DongCoMayBocVo.loaiThietBi = DayChuyenHPEX.DieuKhien.LoaiThietBi.MAY;
            this.DongCoMayBocVo.Location = new System.Drawing.Point(987, 459);
            this.DongCoMayBocVo.m_batHopThoaiRun = true;
            this.DongCoMayBocVo.m_choPhepMoPhong = false;
            this.DongCoMayBocVo.m_Description = "";
            this.DongCoMayBocVo.m_dichbitGiamSat = 1;
            this.DongCoMayBocVo.m_dichbitStart = 0;
            this.DongCoMayBocVo.m_dichbitStop = 1;
            this.DongCoMayBocVo.m_gocquay = System.Drawing.RotateFlipType.RotateNoneFlipNone;
            this.DongCoMayBocVo.m_imgNomal = ((System.Drawing.Image)(resources.GetObject("DongCoMayBocVo.m_imgNomal")));
            this.DongCoMayBocVo.m_imgRun1 = global::DayChuyenHPEX.Properties.Resources.DongCoMayTron;
            this.DongCoMayBocVo.m_imgRun2 = null;
            this.DongCoMayBocVo.m_imgStop = global::DayChuyenHPEX.Properties.Resources.DongCoMayTron_STOP;
            this.DongCoMayBocVo.m_Location = new System.Drawing.Point(987, 459);
            this.DongCoMayBocVo.m_maThietBi = "";
            this.DongCoMayBocVo.m_sImgNomal = null;
            this.DongCoMayBocVo.m_sImgRun1 = null;
            this.DongCoMayBocVo.m_sImgRun2 = null;
            this.DongCoMayBocVo.m_sImgStop = null;
            this.DongCoMayBocVo.m_size = new System.Drawing.Size(40, 82);
            this.DongCoMayBocVo.m_Visible = true;
            this.DongCoMayBocVo.m_vitriPLCGiamSat = 1;
            this.DongCoMayBocVo.m_vitriPLCStart = 1;
            this.DongCoMayBocVo.m_vitriPLCStop = 1;
            this.DongCoMayBocVo.Name = "DongCoMayBocVo";
            this.DongCoMayBocVo.plc = null;
            this.DongCoMayBocVo.quyenQuantri = false;
            this.DongCoMayBocVo.Size = new System.Drawing.Size(40, 82);
            this.DongCoMayBocVo.TabIndex = 141;
            this.DongCoMayBocVo.tenthuocTinh = "M03";
            // 
            // DongCoMayTron
            // 
            this.DongCoMayTron.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.DongCoMayTron.BackColor = System.Drawing.Color.Transparent;
            this.DongCoMayTron.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.DongCoMayTron.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.DongCoMayTron.enable = true;
            this.DongCoMayTron.hienThitenThuocTinh = false;
            this.DongCoMayTron.loaiThietBi = DayChuyenHPEX.DieuKhien.LoaiThietBi.MAY;
            this.DongCoMayTron.Location = new System.Drawing.Point(873, 311);
            this.DongCoMayTron.m_batHopThoaiRun = true;
            this.DongCoMayTron.m_choPhepMoPhong = false;
            this.DongCoMayTron.m_Description = "";
            this.DongCoMayTron.m_dichbitGiamSat = 1;
            this.DongCoMayTron.m_dichbitStart = 0;
            this.DongCoMayTron.m_dichbitStop = 1;
            this.DongCoMayTron.m_gocquay = System.Drawing.RotateFlipType.RotateNoneFlipNone;
            this.DongCoMayTron.m_imgNomal = ((System.Drawing.Image)(resources.GetObject("DongCoMayTron.m_imgNomal")));
            this.DongCoMayTron.m_imgRun1 = global::DayChuyenHPEX.Properties.Resources.DongCoMayTron;
            this.DongCoMayTron.m_imgRun2 = null;
            this.DongCoMayTron.m_imgStop = global::DayChuyenHPEX.Properties.Resources.DongCoMayTron_STOP;
            this.DongCoMayTron.m_Location = new System.Drawing.Point(873, 311);
            this.DongCoMayTron.m_maThietBi = "";
            this.DongCoMayTron.m_sImgNomal = null;
            this.DongCoMayTron.m_sImgRun1 = null;
            this.DongCoMayTron.m_sImgRun2 = null;
            this.DongCoMayTron.m_sImgStop = null;
            this.DongCoMayTron.m_size = new System.Drawing.Size(40, 82);
            this.DongCoMayTron.m_Visible = true;
            this.DongCoMayTron.m_vitriPLCGiamSat = 1;
            this.DongCoMayTron.m_vitriPLCStart = 1;
            this.DongCoMayTron.m_vitriPLCStop = 1;
            this.DongCoMayTron.Name = "DongCoMayTron";
            this.DongCoMayTron.plc = null;
            this.DongCoMayTron.quyenQuantri = false;
            this.DongCoMayTron.Size = new System.Drawing.Size(40, 82);
            this.DongCoMayTron.TabIndex = 0;
            this.DongCoMayTron.tenthuocTinh = "M03";
            // 
            // userControlThietBi1
            // 
            this.userControlThietBi1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.userControlThietBi1.AutoScroll = true;
            this.userControlThietBi1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.userControlThietBi1.BackColor = System.Drawing.Color.Transparent;
            this.userControlThietBi1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.userControlThietBi1.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.userControlThietBi1.enable = true;
            this.userControlThietBi1.hienThitenThuocTinh = false;
            this.userControlThietBi1.loaiThietBi = DayChuyenHPEX.DieuKhien.LoaiThietBi.MAY;
            this.userControlThietBi1.Location = new System.Drawing.Point(39, 637);
            this.userControlThietBi1.m_batHopThoaiRun = true;
            this.userControlThietBi1.m_choPhepMoPhong = false;
            this.userControlThietBi1.m_Description = "";
            this.userControlThietBi1.m_dichbitGiamSat = 2;
            this.userControlThietBi1.m_dichbitStart = 0;
            this.userControlThietBi1.m_dichbitStop = 1;
            this.userControlThietBi1.m_gocquay = System.Drawing.RotateFlipType.RotateNoneFlipNone;
            this.userControlThietBi1.m_imgNomal = null;
            this.userControlThietBi1.m_imgRun1 = null;
            this.userControlThietBi1.m_imgRun2 = null;
            this.userControlThietBi1.m_imgStop = null;
            this.userControlThietBi1.m_Location = new System.Drawing.Point(39, 637);
            this.userControlThietBi1.m_maThietBi = "";
            this.userControlThietBi1.m_sImgNomal = null;
            this.userControlThietBi1.m_sImgRun1 = null;
            this.userControlThietBi1.m_sImgRun2 = null;
            this.userControlThietBi1.m_sImgStop = null;
            this.userControlThietBi1.m_size = new System.Drawing.Size(145, 134);
            this.userControlThietBi1.m_Visible = true;
            this.userControlThietBi1.m_vitriPLCGiamSat = 5;
            this.userControlThietBi1.m_vitriPLCStart = 1;
            this.userControlThietBi1.m_vitriPLCStop = 1;
            this.userControlThietBi1.Name = "userControlThietBi1";
            this.userControlThietBi1.plc = null;
            this.userControlThietBi1.quyenQuantri = false;
            this.userControlThietBi1.Size = new System.Drawing.Size(145, 134);
            this.userControlThietBi1.TabIndex = 0;
            this.userControlThietBi1.tenthuocTinh = "BT01";
            // 
            // NhietDoBinhChuaDungDichDauTien
            // 
            this.NhietDoBinhChuaDungDichDauTien.BackColor = System.Drawing.Color.Transparent;
            this.NhietDoBinhChuaDungDichDauTien.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.NhietDoBinhChuaDungDichDauTien.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.NhietDoBinhChuaDungDichDauTien.dinhdangthapphan = "0.0";
            this.NhietDoBinhChuaDungDichDauTien.donviDo = "độ C";
            this.NhietDoBinhChuaDungDichDauTien.enable = true;
            this.NhietDoBinhChuaDungDichDauTien.giatri = 25D;
            this.NhietDoBinhChuaDungDichDauTien.hienThitenThuocTinh = false;
            this.NhietDoBinhChuaDungDichDauTien.Location = new System.Drawing.Point(117, 488);
            this.NhietDoBinhChuaDungDichDauTien.m_Location = new System.Drawing.Point(117, 488);
            this.NhietDoBinhChuaDungDichDauTien.m_size = new System.Drawing.Size(76, 19);
            this.NhietDoBinhChuaDungDichDauTien.m_Visible = true;
            this.NhietDoBinhChuaDungDichDauTien.Name = "NhietDoBinhChuaDungDichDauTien";
            this.NhietDoBinhChuaDungDichDauTien.plc = null;
            this.NhietDoBinhChuaDungDichDauTien.quyenQuantri = false;
            this.NhietDoBinhChuaDungDichDauTien.scale = 0.1D;
            this.NhietDoBinhChuaDungDichDauTien.Size = new System.Drawing.Size(76, 19);
            this.NhietDoBinhChuaDungDichDauTien.TabIndex = 140;
            this.NhietDoBinhChuaDungDichDauTien.tenthuocTinh = "T04";
            this.NhietDoBinhChuaDungDichDauTien.vitriPLC = 186;
            this.NhietDoBinhChuaDungDichDauTien.Load += new System.EventHandler(this.userControlLabelHienThiNhanh2_Load);
            // 
            // NhietDoT3
            // 
            this.NhietDoT3.BackColor = System.Drawing.Color.Transparent;
            this.NhietDoT3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.NhietDoT3.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.NhietDoT3.dinhdangthapphan = "0.0";
            this.NhietDoT3.donviDo = "độ C";
            this.NhietDoT3.enable = true;
            this.NhietDoT3.giatri = 25D;
            this.NhietDoT3.hienThitenThuocTinh = false;
            this.NhietDoT3.Location = new System.Drawing.Point(418, 289);
            this.NhietDoT3.m_Location = new System.Drawing.Point(418, 289);
            this.NhietDoT3.m_size = new System.Drawing.Size(83, 19);
            this.NhietDoT3.m_Visible = true;
            this.NhietDoT3.Name = "NhietDoT3";
            this.NhietDoT3.plc = null;
            this.NhietDoT3.quyenQuantri = false;
            this.NhietDoT3.scale = 0.1D;
            this.NhietDoT3.Size = new System.Drawing.Size(83, 19);
            this.NhietDoT3.TabIndex = 140;
            this.NhietDoT3.tenthuocTinh = "T03";
            this.NhietDoT3.vitriPLC = 182;
            this.NhietDoT3.Load += new System.EventHandler(this.userControlLabelHienThiNhanh2_Load);
            // 
            // NhietDoBinhDungDichBocVo
            // 
            this.NhietDoBinhDungDichBocVo.BackColor = System.Drawing.Color.Transparent;
            this.NhietDoBinhDungDichBocVo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.NhietDoBinhDungDichBocVo.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.NhietDoBinhDungDichBocVo.dinhdangthapphan = "0.0";
            this.NhietDoBinhDungDichBocVo.donviDo = "độ C";
            this.NhietDoBinhDungDichBocVo.enable = true;
            this.NhietDoBinhDungDichBocVo.giatri = 25D;
            this.NhietDoBinhDungDichBocVo.hienThitenThuocTinh = false;
            this.NhietDoBinhDungDichBocVo.Location = new System.Drawing.Point(682, 544);
            this.NhietDoBinhDungDichBocVo.m_Location = new System.Drawing.Point(682, 544);
            this.NhietDoBinhDungDichBocVo.m_size = new System.Drawing.Size(83, 19);
            this.NhietDoBinhDungDichBocVo.m_Visible = true;
            this.NhietDoBinhDungDichBocVo.Name = "NhietDoBinhDungDichBocVo";
            this.NhietDoBinhDungDichBocVo.plc = null;
            this.NhietDoBinhDungDichBocVo.quyenQuantri = false;
            this.NhietDoBinhDungDichBocVo.scale = 0.1D;
            this.NhietDoBinhDungDichBocVo.Size = new System.Drawing.Size(83, 19);
            this.NhietDoBinhDungDichBocVo.TabIndex = 140;
            this.NhietDoBinhDungDichBocVo.tenthuocTinh = "Nhiệt độ T5";
            this.NhietDoBinhDungDichBocVo.vitriPLC = 182;
            this.NhietDoBinhDungDichBocVo.Load += new System.EventHandler(this.userControlLabelHienThiNhanh2_Load);
            // 
            // userControlLabelHienThiNhanh7
            // 
            this.userControlLabelHienThiNhanh7.BackColor = System.Drawing.Color.Transparent;
            this.userControlLabelHienThiNhanh7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.userControlLabelHienThiNhanh7.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.userControlLabelHienThiNhanh7.dinhdangthapphan = "0.0";
            this.userControlLabelHienThiNhanh7.donviDo = "l/ph";
            this.userControlLabelHienThiNhanh7.enable = true;
            this.userControlLabelHienThiNhanh7.giatri = 25D;
            this.userControlLabelHienThiNhanh7.hienThitenThuocTinh = false;
            this.userControlLabelHienThiNhanh7.Location = new System.Drawing.Point(1270, 756);
            this.userControlLabelHienThiNhanh7.m_Location = new System.Drawing.Point(1270, 756);
            this.userControlLabelHienThiNhanh7.m_size = new System.Drawing.Size(70, 19);
            this.userControlLabelHienThiNhanh7.m_Visible = true;
            this.userControlLabelHienThiNhanh7.Name = "userControlLabelHienThiNhanh7";
            this.userControlLabelHienThiNhanh7.plc = null;
            this.userControlLabelHienThiNhanh7.quyenQuantri = false;
            this.userControlLabelHienThiNhanh7.scale = 0.1D;
            this.userControlLabelHienThiNhanh7.Size = new System.Drawing.Size(70, 19);
            this.userControlLabelHienThiNhanh7.TabIndex = 140;
            this.userControlLabelHienThiNhanh7.tenthuocTinh = "Luu luong do FL3";
            this.userControlLabelHienThiNhanh7.vitriPLC = 192;
            // 
            // userControlLabelHienThiNhanh6
            // 
            this.userControlLabelHienThiNhanh6.BackColor = System.Drawing.Color.Transparent;
            this.userControlLabelHienThiNhanh6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.userControlLabelHienThiNhanh6.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.userControlLabelHienThiNhanh6.dinhdangthapphan = "0.0";
            this.userControlLabelHienThiNhanh6.donviDo = "l/ph";
            this.userControlLabelHienThiNhanh6.enable = true;
            this.userControlLabelHienThiNhanh6.giatri = 25D;
            this.userControlLabelHienThiNhanh6.hienThitenThuocTinh = false;
            this.userControlLabelHienThiNhanh6.Location = new System.Drawing.Point(1204, 756);
            this.userControlLabelHienThiNhanh6.m_Location = new System.Drawing.Point(1204, 756);
            this.userControlLabelHienThiNhanh6.m_size = new System.Drawing.Size(70, 19);
            this.userControlLabelHienThiNhanh6.m_Visible = true;
            this.userControlLabelHienThiNhanh6.Name = "userControlLabelHienThiNhanh6";
            this.userControlLabelHienThiNhanh6.plc = null;
            this.userControlLabelHienThiNhanh6.quyenQuantri = false;
            this.userControlLabelHienThiNhanh6.scale = 0.1D;
            this.userControlLabelHienThiNhanh6.Size = new System.Drawing.Size(70, 19);
            this.userControlLabelHienThiNhanh6.TabIndex = 140;
            this.userControlLabelHienThiNhanh6.tenthuocTinh = "Luu luong do FL3";
            this.userControlLabelHienThiNhanh6.vitriPLC = 192;
            // 
            // userControlLabelHienThiNhanh5
            // 
            this.userControlLabelHienThiNhanh5.BackColor = System.Drawing.Color.Transparent;
            this.userControlLabelHienThiNhanh5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.userControlLabelHienThiNhanh5.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.userControlLabelHienThiNhanh5.dinhdangthapphan = "0.0";
            this.userControlLabelHienThiNhanh5.donviDo = "l/ph";
            this.userControlLabelHienThiNhanh5.enable = true;
            this.userControlLabelHienThiNhanh5.giatri = 25D;
            this.userControlLabelHienThiNhanh5.hienThitenThuocTinh = false;
            this.userControlLabelHienThiNhanh5.Location = new System.Drawing.Point(1128, 756);
            this.userControlLabelHienThiNhanh5.m_Location = new System.Drawing.Point(1128, 756);
            this.userControlLabelHienThiNhanh5.m_size = new System.Drawing.Size(70, 19);
            this.userControlLabelHienThiNhanh5.m_Visible = true;
            this.userControlLabelHienThiNhanh5.Name = "userControlLabelHienThiNhanh5";
            this.userControlLabelHienThiNhanh5.plc = null;
            this.userControlLabelHienThiNhanh5.quyenQuantri = false;
            this.userControlLabelHienThiNhanh5.scale = 0.1D;
            this.userControlLabelHienThiNhanh5.Size = new System.Drawing.Size(70, 19);
            this.userControlLabelHienThiNhanh5.TabIndex = 140;
            this.userControlLabelHienThiNhanh5.tenthuocTinh = "Luu luong do FL3";
            this.userControlLabelHienThiNhanh5.vitriPLC = 192;
            // 
            // userControlLabelHienThiNhanh4
            // 
            this.userControlLabelHienThiNhanh4.BackColor = System.Drawing.Color.Transparent;
            this.userControlLabelHienThiNhanh4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.userControlLabelHienThiNhanh4.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.userControlLabelHienThiNhanh4.dinhdangthapphan = "0.0";
            this.userControlLabelHienThiNhanh4.donviDo = "l/ph";
            this.userControlLabelHienThiNhanh4.enable = true;
            this.userControlLabelHienThiNhanh4.giatri = 25D;
            this.userControlLabelHienThiNhanh4.hienThitenThuocTinh = false;
            this.userControlLabelHienThiNhanh4.Location = new System.Drawing.Point(1063, 756);
            this.userControlLabelHienThiNhanh4.m_Location = new System.Drawing.Point(1063, 756);
            this.userControlLabelHienThiNhanh4.m_size = new System.Drawing.Size(70, 19);
            this.userControlLabelHienThiNhanh4.m_Visible = true;
            this.userControlLabelHienThiNhanh4.Name = "userControlLabelHienThiNhanh4";
            this.userControlLabelHienThiNhanh4.plc = null;
            this.userControlLabelHienThiNhanh4.quyenQuantri = false;
            this.userControlLabelHienThiNhanh4.scale = 0.1D;
            this.userControlLabelHienThiNhanh4.Size = new System.Drawing.Size(70, 19);
            this.userControlLabelHienThiNhanh4.TabIndex = 140;
            this.userControlLabelHienThiNhanh4.tenthuocTinh = "Luu luong do FL3";
            this.userControlLabelHienThiNhanh4.vitriPLC = 192;
            // 
            // userControlLabelHienThiNhanh3
            // 
            this.userControlLabelHienThiNhanh3.BackColor = System.Drawing.Color.Transparent;
            this.userControlLabelHienThiNhanh3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.userControlLabelHienThiNhanh3.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.userControlLabelHienThiNhanh3.dinhdangthapphan = "0.0";
            this.userControlLabelHienThiNhanh3.donviDo = "l/ph";
            this.userControlLabelHienThiNhanh3.enable = true;
            this.userControlLabelHienThiNhanh3.giatri = 25D;
            this.userControlLabelHienThiNhanh3.hienThitenThuocTinh = false;
            this.userControlLabelHienThiNhanh3.Location = new System.Drawing.Point(987, 756);
            this.userControlLabelHienThiNhanh3.m_Location = new System.Drawing.Point(987, 756);
            this.userControlLabelHienThiNhanh3.m_size = new System.Drawing.Size(70, 19);
            this.userControlLabelHienThiNhanh3.m_Visible = true;
            this.userControlLabelHienThiNhanh3.Name = "userControlLabelHienThiNhanh3";
            this.userControlLabelHienThiNhanh3.plc = null;
            this.userControlLabelHienThiNhanh3.quyenQuantri = false;
            this.userControlLabelHienThiNhanh3.scale = 0.1D;
            this.userControlLabelHienThiNhanh3.Size = new System.Drawing.Size(70, 19);
            this.userControlLabelHienThiNhanh3.TabIndex = 140;
            this.userControlLabelHienThiNhanh3.tenthuocTinh = "Luu luong do FL3";
            this.userControlLabelHienThiNhanh3.vitriPLC = 192;
            // 
            // NhietDoOngBocVo
            // 
            this.NhietDoOngBocVo.BackColor = System.Drawing.Color.Transparent;
            this.NhietDoOngBocVo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.NhietDoOngBocVo.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.NhietDoOngBocVo.dinhdangthapphan = "0.0";
            this.NhietDoOngBocVo.donviDo = "l/ph";
            this.NhietDoOngBocVo.enable = true;
            this.NhietDoOngBocVo.giatri = 25D;
            this.NhietDoOngBocVo.hienThitenThuocTinh = false;
            this.NhietDoOngBocVo.Location = new System.Drawing.Point(910, 571);
            this.NhietDoOngBocVo.m_Location = new System.Drawing.Point(910, 571);
            this.NhietDoOngBocVo.m_size = new System.Drawing.Size(70, 19);
            this.NhietDoOngBocVo.m_Visible = true;
            this.NhietDoOngBocVo.Name = "NhietDoOngBocVo";
            this.NhietDoOngBocVo.plc = null;
            this.NhietDoOngBocVo.quyenQuantri = false;
            this.NhietDoOngBocVo.scale = 0.1D;
            this.NhietDoOngBocVo.Size = new System.Drawing.Size(70, 19);
            this.NhietDoOngBocVo.TabIndex = 140;
            this.NhietDoOngBocVo.tenthuocTinh = "Luu luong do FL3";
            this.NhietDoOngBocVo.vitriPLC = 192;
            // 
            // LuuLuongKeFL2
            // 
            this.LuuLuongKeFL2.BackColor = System.Drawing.Color.Transparent;
            this.LuuLuongKeFL2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.LuuLuongKeFL2.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.LuuLuongKeFL2.dinhdangthapphan = "0.0";
            this.LuuLuongKeFL2.donviDo = "l/ph";
            this.LuuLuongKeFL2.enable = true;
            this.LuuLuongKeFL2.giatri = 25D;
            this.LuuLuongKeFL2.hienThitenThuocTinh = false;
            this.LuuLuongKeFL2.Location = new System.Drawing.Point(621, 517);
            this.LuuLuongKeFL2.m_Location = new System.Drawing.Point(621, 517);
            this.LuuLuongKeFL2.m_size = new System.Drawing.Size(61, 19);
            this.LuuLuongKeFL2.m_Visible = true;
            this.LuuLuongKeFL2.Name = "LuuLuongKeFL2";
            this.LuuLuongKeFL2.plc = null;
            this.LuuLuongKeFL2.quyenQuantri = false;
            this.LuuLuongKeFL2.scale = 0.1D;
            this.LuuLuongKeFL2.Size = new System.Drawing.Size(61, 19);
            this.LuuLuongKeFL2.TabIndex = 140;
            this.LuuLuongKeFL2.tenthuocTinh = "Lưu lượng FL2";
            this.LuuLuongKeFL2.vitriPLC = 190;
            // 
            // LuuLuongKeFL1
            // 
            this.LuuLuongKeFL1.BackColor = System.Drawing.Color.Transparent;
            this.LuuLuongKeFL1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.LuuLuongKeFL1.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.LuuLuongKeFL1.dinhdangthapphan = "0.0";
            this.LuuLuongKeFL1.donviDo = "l/ph";
            this.LuuLuongKeFL1.enable = true;
            this.LuuLuongKeFL1.giatri = 25D;
            this.LuuLuongKeFL1.hienThitenThuocTinh = false;
            this.LuuLuongKeFL1.Location = new System.Drawing.Point(241, 311);
            this.LuuLuongKeFL1.m_Location = new System.Drawing.Point(241, 311);
            this.LuuLuongKeFL1.m_size = new System.Drawing.Size(72, 19);
            this.LuuLuongKeFL1.m_Visible = true;
            this.LuuLuongKeFL1.Name = "LuuLuongKeFL1";
            this.LuuLuongKeFL1.plc = null;
            this.LuuLuongKeFL1.quyenQuantri = false;
            this.LuuLuongKeFL1.scale = 0.1D;
            this.LuuLuongKeFL1.Size = new System.Drawing.Size(72, 19);
            this.LuuLuongKeFL1.TabIndex = 140;
            this.LuuLuongKeFL1.tenthuocTinh = "Lưu lượng FL1";
            this.LuuLuongKeFL1.vitriPLC = 188;
            // 
            // NhietDoBinhChuaSauCoDacT4
            // 
            this.NhietDoBinhChuaSauCoDacT4.BackColor = System.Drawing.Color.Transparent;
            this.NhietDoBinhChuaSauCoDacT4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.NhietDoBinhChuaSauCoDacT4.DataBindings.Add(new System.Windows.Forms.Binding("quyenQuantri", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.NhietDoBinhChuaSauCoDacT4.dinhdangthapphan = "0.0";
            this.NhietDoBinhChuaSauCoDacT4.donviDo = "độ C";
            this.NhietDoBinhChuaSauCoDacT4.enable = true;
            this.NhietDoBinhChuaSauCoDacT4.giatri = 25D;
            this.NhietDoBinhChuaSauCoDacT4.hienThitenThuocTinh = false;
            this.NhietDoBinhChuaSauCoDacT4.Location = new System.Drawing.Point(388, 483);
            this.NhietDoBinhChuaSauCoDacT4.m_Location = new System.Drawing.Point(388, 483);
            this.NhietDoBinhChuaSauCoDacT4.m_size = new System.Drawing.Size(66, 19);
            this.NhietDoBinhChuaSauCoDacT4.m_Visible = true;
            this.NhietDoBinhChuaSauCoDacT4.Name = "NhietDoBinhChuaSauCoDacT4";
            this.NhietDoBinhChuaSauCoDacT4.plc = null;
            this.NhietDoBinhChuaSauCoDacT4.quyenQuantri = false;
            this.NhietDoBinhChuaSauCoDacT4.scale = 0.1D;
            this.NhietDoBinhChuaSauCoDacT4.Size = new System.Drawing.Size(66, 19);
            this.NhietDoBinhChuaSauCoDacT4.TabIndex = 140;
            this.NhietDoBinhChuaSauCoDacT4.tenthuocTinh = "Nhiệt độ T4";
            this.NhietDoBinhChuaSauCoDacT4.vitriPLC = 178;
            // 
            // ribbonPage2
            // 
            this.ribbonPage2.Name = "ribbonPage2";
            this.ribbonPage2.Text = "ribbonPage2";
            // 
            // ribbonPage3
            // 
            this.ribbonPage3.Name = "ribbonPage3";
            this.ribbonPage3.Text = "ribbonPage3";
            // 
            // barManagerBottom
            // 
            this.barManagerBottom.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.barBottomStatus});
            this.barManagerBottom.DockControls.Add(this.barDockControlTop);
            this.barManagerBottom.DockControls.Add(this.barDockControlBottom);
            this.barManagerBottom.DockControls.Add(this.barDockControlLeft);
            this.barManagerBottom.DockControls.Add(this.barDockControlRight);
            this.barManagerBottom.Form = this;
            this.barManagerBottom.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnServer,
            this.btnUser,
            this.btnQuyen,
            this.btnVersion,
            this.btnError,
            this.btnPlc,
            this.ProgressbarEditItem1,
            this.barEditItemProgressBarChinh});
            this.barManagerBottom.MaxItemId = 15;
            this.barManagerBottom.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1,
            this.repositoryItemProgressBarBottom,
            this.repositoryItemProgressBarChinh});
            this.barManagerBottom.StatusBar = this.barBottomStatus;
            // 
            // barBottomStatus
            // 
            this.barBottomStatus.BarName = "barBottomStatusCustom";
            this.barBottomStatus.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.barBottomStatus.DockCol = 0;
            this.barBottomStatus.DockRow = 0;
            this.barBottomStatus.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.barBottomStatus.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnServer),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnUser),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnQuyen),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnError),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnVersion),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this.barEditItemProgressBarChinh, "", false, true, true, 197),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this.ProgressbarEditItem1, "", false, true, true, 321)});
            this.barBottomStatus.OptionsBar.AllowQuickCustomization = false;
            this.barBottomStatus.OptionsBar.DrawDragBorder = false;
            this.barBottomStatus.OptionsBar.UseWholeRow = true;
            this.barBottomStatus.Text = "barBottom";
            // 
            // btnServer
            // 
            this.btnServer.Border = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.btnServer.Caption = "Server";
            this.btnServer.DataBindings.Add(new System.Windows.Forms.Binding("Caption", this.configBindingHPEXBindingSource, "server", true));
            this.btnServer.Id = 1;
            this.btnServer.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnServer.ImageOptions.Image")));
            this.btnServer.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnServer.ImageOptions.LargeImage")));
            this.btnServer.Name = "btnServer";
            this.btnServer.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnServer.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.clickServerLogin);
            // 
            // btnUser
            // 
            this.btnUser.Border = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.btnUser.Caption = "User";
            this.btnUser.DataBindings.Add(new System.Windows.Forms.Binding("Caption", this.configBindingHPEXBindingSource, "userName", true));
            this.btnUser.Id = 2;
            this.btnUser.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnUser.ImageOptions.Image")));
            this.btnUser.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnUser.ImageOptions.LargeImage")));
            this.btnUser.Name = "btnUser";
            this.btnUser.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // btnQuyen
            // 
            this.btnQuyen.Border = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.btnQuyen.Caption = "Quyen";
            this.btnQuyen.DataBindings.Add(new System.Windows.Forms.Binding("Caption", this.configBindingHPEXBindingSource, "nhomquyen", true));
            this.btnQuyen.Id = 3;
            this.btnQuyen.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnQuyen.ImageOptions.Image")));
            this.btnQuyen.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnQuyen.ImageOptions.LargeImage")));
            this.btnQuyen.Name = "btnQuyen";
            this.btnQuyen.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // btnError
            // 
            this.btnError.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnError.Border = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btnError.Caption = "phát hiện lỗi chương trình thì hiển thị";
            this.btnError.DataBindings.Add(new System.Windows.Forms.Binding("Caption", this.configBindingHPEXBindingSource, "errorText", true));
            this.btnError.DataBindings.Add(new System.Windows.Forms.Binding("Visibility", this.configBindingHPEXBindingSource, "Error", true));
            this.btnError.Id = 6;
            this.btnError.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnError.ImageOptions.LargeImage")));
            this.btnError.Name = "btnError";
            this.btnError.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // btnVersion
            // 
            this.btnVersion.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btnVersion.Border = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btnVersion.Caption = "version";
            this.btnVersion.DataBindings.Add(new System.Windows.Forms.Binding("Caption", this.configBindingHPEXBindingSource, "version", true));
            this.btnVersion.Id = 5;
            this.btnVersion.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnVersion.ImageOptions.Image")));
            this.btnVersion.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnVersion.ImageOptions.LargeImage")));
            this.btnVersion.Name = "btnVersion";
            this.btnVersion.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barEditItemProgressBarChinh
            // 
            this.barEditItemProgressBarChinh.Caption = "Progress bar chinh";
            this.barEditItemProgressBarChinh.Edit = this.repositoryItemProgressBarChinh;
            this.barEditItemProgressBarChinh.Id = 14;
            this.barEditItemProgressBarChinh.Name = "barEditItemProgressBarChinh";
            // 
            // repositoryItemProgressBarChinh
            // 
            this.repositoryItemProgressBarChinh.Name = "repositoryItemProgressBarChinh";
            this.repositoryItemProgressBarChinh.Step = 20;
            // 
            // ProgressbarEditItem1
            // 
            this.ProgressbarEditItem1.Caption = "progress bar";
            this.ProgressbarEditItem1.Edit = this.repositoryItemProgressBarBottom;
            this.ProgressbarEditItem1.Id = 13;
            this.ProgressbarEditItem1.Name = "ProgressbarEditItem1";
            this.ProgressbarEditItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.ProgressbarEditItem1_ItemClick);
            // 
            // repositoryItemProgressBarBottom
            // 
            this.repositoryItemProgressBarBottom.Name = "repositoryItemProgressBarBottom";
            this.repositoryItemProgressBarBottom.ShowTitle = true;
            this.repositoryItemProgressBarBottom.Step = 1;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 24);
            this.barDockControlTop.Manager = this.barManagerBottom;
            this.barDockControlTop.Size = new System.Drawing.Size(1841, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 831);
            this.barDockControlBottom.Manager = this.barManagerBottom;
            this.barDockControlBottom.Size = new System.Drawing.Size(1841, 28);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 24);
            this.barDockControlLeft.Manager = this.barManagerBottom;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 807);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1841, 24);
            this.barDockControlRight.Manager = this.barManagerBottom;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 807);
            // 
            // btnPlc
            // 
            this.btnPlc.Caption = "PLC";
            this.btnPlc.Id = 8;
            this.btnPlc.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnPlc.ImageOptions.Image")));
            this.btnPlc.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnPlc.ImageOptions.LargeImage")));
            this.btnPlc.Name = "btnPlc";
            this.btnPlc.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // barManagerTop
            // 
            this.barManagerTop.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.barTop});
            this.barManagerTop.DockControls.Add(this.barDockControl1);
            this.barManagerTop.DockControls.Add(this.barDockControl2);
            this.barManagerTop.DockControls.Add(this.barDockControl3);
            this.barManagerTop.DockControls.Add(this.barDockControl4);
            this.barManagerTop.Form = this;
            this.barManagerTop.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnNhanSu,
            this.btnLog,
            this.menuChinh,
            this.btnTroGiup,
            this.btnBanQuyen,
            this.btnKhoiTao});
            this.barManagerTop.MaxItemId = 14;
            // 
            // barTop
            // 
            this.barTop.BarName = "barTopCustom 2";
            this.barTop.DockCol = 0;
            this.barTop.DockRow = 0;
            this.barTop.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.barTop.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnKhoiTao, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.menuChinh)});
            this.barTop.Text = "bar top";
            // 
            // btnKhoiTao
            // 
            this.btnKhoiTao.Caption = "khởi tạo";
            this.btnKhoiTao.Id = 13;
            this.btnKhoiTao.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnKhoiTao.ImageOptions.Image")));
            this.btnKhoiTao.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnKhoiTao.ImageOptions.LargeImage")));
            this.btnKhoiTao.Name = "btnKhoiTao";
            this.btnKhoiTao.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipItem1.Appearance.ForeColor = System.Drawing.Color.DeepSkyBlue;
            toolTipItem1.Appearance.Options.UseForeColor = true;
            toolTipItem1.Text = "Kết nối PLC\r\nKhởi tạo dữ cho các thiết bị chương trình ";
            superToolTip1.Items.Add(toolTipItem1);
            this.btnKhoiTao.SuperTip = superToolTip1;
            this.btnKhoiTao.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.khoiTaoPLC);
            // 
            // menuChinh
            // 
            this.menuChinh.Caption = "Thực đơn";
            this.menuChinh.Id = 3;
            this.menuChinh.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("menuChinh.ImageOptions.Image")));
            this.menuChinh.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("menuChinh.ImageOptions.LargeImage")));
            this.menuChinh.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnNhanSu),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnTroGiup),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnLog),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnBanQuyen)});
            this.menuChinh.Name = "menuChinh";
            this.menuChinh.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // btnNhanSu
            // 
            this.btnNhanSu.Border = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.btnNhanSu.Caption = "Người dùng";
            this.btnNhanSu.DataBindings.Add(new System.Windows.Forms.Binding("Enabled", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.btnNhanSu.Id = 1;
            this.btnNhanSu.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnNhanSu.ImageOptions.Image")));
            this.btnNhanSu.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnNhanSu.ImageOptions.LargeImage")));
            this.btnNhanSu.Name = "btnNhanSu";
            this.btnNhanSu.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnNhanSu.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnNhanSu_ItemClick);
            // 
            // btnTroGiup
            // 
            this.btnTroGiup.Caption = "Trợ giúp";
            this.btnTroGiup.Id = 5;
            this.btnTroGiup.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnTroGiup.ImageOptions.Image")));
            this.btnTroGiup.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnTroGiup.ImageOptions.LargeImage")));
            this.btnTroGiup.Name = "btnTroGiup";
            this.btnTroGiup.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // btnLog
            // 
            this.btnLog.Border = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.btnLog.Caption = "Log";
            this.btnLog.DataBindings.Add(new System.Windows.Forms.Binding("Enabled", this.configBindingHPEXBindingSource, "canAccessHethong", true));
            this.btnLog.Id = 2;
            this.btnLog.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnLog.ImageOptions.Image")));
            this.btnLog.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnLog.ImageOptions.LargeImage")));
            this.btnLog.Name = "btnLog";
            this.btnLog.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnLog.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.logViewClick);
            // 
            // btnBanQuyen
            // 
            this.btnBanQuyen.Caption = "Bản quyền";
            this.btnBanQuyen.Id = 6;
            this.btnBanQuyen.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnBanQuyen.ImageOptions.Image")));
            this.btnBanQuyen.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnBanQuyen.ImageOptions.LargeImage")));
            this.btnBanQuyen.Name = "btnBanQuyen";
            this.btnBanQuyen.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Manager = this.barManagerTop;
            this.barDockControl1.Size = new System.Drawing.Size(1841, 24);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 859);
            this.barDockControl2.Manager = this.barManagerTop;
            this.barDockControl2.Size = new System.Drawing.Size(1841, 0);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 24);
            this.barDockControl3.Manager = this.barManagerTop;
            this.barDockControl3.Size = new System.Drawing.Size(0, 835);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(1841, 24);
            this.barDockControl4.Manager = this.barManagerTop;
            this.barDockControl4.Size = new System.Drawing.Size(0, 835);
            // 
            // splashScreenManagerForm
            // 
            this.splashScreenManagerForm.ClosingDelay = 500;
            // 
            // FormChinh
            // 
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1841, 859);
            this.Controls.Add(this.panelBaoLoi);
            this.Controls.Add(this.panelPhanChinh);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "FormChinh";
            this.Text = "HỆ THỐNG CAN";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.formClose);
            this.Load += new System.EventHandler(this.FormChinh_Load);
            ((System.ComponentModel.ISupportInitialize)(this.configBindingHPEXBindingSource)).EndInit();
            this.panelBaoLoi.ResumeLayout(false);
            this.panelPhanChinh.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.barManagerBottom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBarChinh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBarBottom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManagerTop)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Timer timerChayChinh;
        private System.Windows.Forms.ToolTip toolTipThongTin;
        private System.Windows.Forms.ToolTip toolTipForm;
        private DieuKhien.UserControlLabelHienThiNhanh NhietDoBinhCoDac;
        private DieuKhien.UserControlThuocTinhGhiXuongPLC BienTanDatBT1;
        private DieuKhien.UserControlDaoBit BitXoaLoi;
        private System.Windows.Forms.Panel panelBaoLoi;
        public System.Windows.Forms.Panel panelPhanChinh;
        private System.Windows.Forms.BindingSource configBindingHPEXBindingSource;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage2;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage3;
        private DevExpress.XtraBars.BarManager barManagerBottom;
        private DevExpress.XtraBars.BarButtonItem btnServer;
        private DevExpress.XtraBars.BarButtonItem btnQuyen;
        private DevExpress.XtraBars.BarButtonItem btnUser;
        private DevExpress.XtraBars.BarButtonItem btnVersion;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarManager barManagerTop;
        private DevExpress.XtraBars.BarButtonItem btnNhanSu;
        private DevExpress.XtraBars.BarButtonItem btnLog;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraBars.BarStaticItem btnError;
        private DieuKhien.UserControlThietBi userControlThietBi1;
        private DevExpress.XtraBars.BarSubItem menuChinh;
        private DevExpress.XtraBars.BarButtonItem btnTroGiup;
        private DevExpress.XtraBars.BarButtonItem btnBanQuyen;
        private DieuKhien.UserControlThietBi DongCoMayTron;
        private DieuKhien.UserControlThuocTinhGhiXuongPLC Bientan09;
        private DieuKhien.UserControlThuocTinhGhiXuongPLC Bientan04;
        private DieuKhien.UserControlThuocTinhGhiXuongPLC BienTan08;
        private DieuKhien.UserControlThuocTinhGhiXuongPLC BienTanDat03;
        private DieuKhien.UserControlThuocTinhGhiXuongPLC Bientan06;
        private DieuKhien.UserControlThuocTinhGhiXuongPLC BienTanDat02;
        private DieuKhien.UserControlThuocTinhGhiXuongPLC Bientan05;
        private DieuKhien.UserControlThuocTinhGhiXuongPLC NhietDoDat4;
        private DieuKhien.UserControlThuocTinhGhiXuongPLC NhietDoDat2;
        private DieuKhien.UserControlThuocTinhGhiXuongPLC NhietDoDat3;
        private DieuKhien.UserControlLabelHienThiNhanh NhietDoBinhDungDichBocVo;
        private DieuKhien.UserControlLabelHienThiNhanh NhietDoBinhChuaSauCoDacT4;
        private DieuKhien.UserControlLabelHienThiNhanh NhietDoBinhChuaDungDichDauTien;
        private DieuKhien.UserControlLabelHienThiNhanh NhietDoOngBocVo;
        private DieuKhien.UserControlLabelHienThiNhanh LuuLuongKeFL2;
        private DieuKhien.UserControlLabelHienThiNhanh LuuLuongKeFL1;
        private DieuKhien.UserControlThietBi VanCoDac;
        private DieuKhien.UserControlThietBi DongCoMayBocVo;
        private DieuKhien.UserControlThietBi userControlThietBiBOM;
        private DieuKhien.UserControlThietBi DongCoVitTaiMayTronTrai;
        private DieuKhien.UserControlThietBi BomDungDich01;
        private DieuKhien.UserControlThietBi BomLenCoDac;
        private DieuKhien.UserControlThietBi BomBanhRangLenMayTaoHat;
        private DieuKhien.UserControlThietBi DongCoVitTaiMayTronPhai;
        private DieuKhien.UserControlThietBi DongCoVitTaiMayDongBao;
        private DevExpress.XtraBars.Bar barBottomStatus;
        private DevExpress.XtraBars.BarButtonItem btnPlc;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraBars.Bar barTop;
        private DevExpress.XtraBars.BarEditItem ProgressbarEditItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemProgressBar repositoryItemProgressBarBottom;
        private DevExpress.XtraBars.BarEditItem barEditItemProgressBarChinh;
        private DevExpress.XtraEditors.Repository.RepositoryItemProgressBar repositoryItemProgressBarChinh;
        private DevExpress.XtraBars.BarButtonItem btnKhoiTao;
        private DieuKhien.ucPLC ucPLCChinh;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManagerForm;
        private DieuKhien.UserControlThietBi userControlThietBi2;
        private DieuKhien.UserControlLabelHienThiNhanh NhietDoT3;
        private DieuKhien.UserControlThietBi userControlThietBi4;
        private DieuKhien.UserControlThietBi userControlThietBi3;
        private DieuKhien.UserControlThuocTinhGhiXuongPLC userControlThuocTinhGhiXuongPLC1;
        private DieuKhien.UserControlThuocTinhGhiXuongPLC userControlThuocTinhGhiXuongPLC2;
        private DieuKhien.UserControlDaoBit userControlDaoBit1;
        private DieuKhien.UserControlLabelHienThiNhanh userControlLabelHienThiNhanh1;
        private DieuKhien.UserControlBaoLoi LoiChung;
        private DieuKhien.UserControlThuocTinhGhiXuongPLC userControlThuocTinhGhiXuongPLC3;
        private DieuKhien.UserControlLabelHienThiNhanh userControlLabelHienThiNhanh2;
        private DieuKhien.UserControlThuocTinhGhiXuongPLC userControlThuocTinhGhiXuongPLC4;
        private DieuKhien.UserControlThuocTinhGhiXuongPLC userControlThuocTinhGhiXuongPLC7;
        private DieuKhien.UserControlThuocTinhGhiXuongPLC userControlThuocTinhGhiXuongPLC6;
        private DieuKhien.UserControlThuocTinhGhiXuongPLC userControlThuocTinhGhiXuongPLC5;
        private DieuKhien.UserControlLabelHienThiNhanh userControlLabelHienThiNhanh7;
        private DieuKhien.UserControlLabelHienThiNhanh userControlLabelHienThiNhanh6;
        private DieuKhien.UserControlLabelHienThiNhanh userControlLabelHienThiNhanh5;
        private DieuKhien.UserControlLabelHienThiNhanh userControlLabelHienThiNhanh4;
        private DieuKhien.UserControlLabelHienThiNhanh userControlLabelHienThiNhanh3;
        private DieuKhien.UserControlThietBi userControlThietBi6;
        private DieuKhien.UserControlThietBi userControlThietBi5;
        private DieuKhien.UserControlThietBi userControlThietBi8;
        private DieuKhien.UserControlThietBi userControlThietBi7;
        private DieuKhien.UserControlThietBi userControlThietBi9;
        private DieuKhien.UserControlLabelText userControlLabelText4;
        private DieuKhien.UserControlLabelText userControlLabelText5;
        private DieuKhien.UserControlLabelText userControlLabelText6;
        private DieuKhien.UserControlThietBi userControlThietBi10;
        private DieuKhien.UserControlThietBi userControlThietBi11;
        private DieuKhien.UserControlThietBi userControlThietBi12;
        private DieuKhien.UserControlLabelText userControlLabelText3;
        private DieuKhien.UserControlLabelText userControlLabelText2;
        private DieuKhien.UserControlLabelText userControlLabelText1;
        private DieuKhien.UserControlThuocTinhGhiXuongPLC userControlThuocTinhGhiXuongPLC9;
        private DieuKhien.UserControlThuocTinhGhiXuongPLC userControlThuocTinhGhiXuongPLC8;
    }
}

