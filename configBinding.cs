﻿using System;
using DevExpress.Xpo;
using DevExpress.XtraBars;

namespace DayChuyenHPEX
{

    public class configBindingHPEX : LAM_Base.confidBinding
    {
        string _server = "", _PLCIP, _userName,_thongtin,_version,_nhomquyen;
        bool _plcRun=false;
        BarItemVisibility _Error = DevExpress.XtraBars.BarItemVisibility.Never;
        bool _canAccessHethong= false, _canAccessSuDung= false, _canAccessQuanLy= false;
        public bool canAccessHethong
        {
            get { return this._canAccessHethong; }
            set
            {
                if (value != this._canAccessHethong)
                {
                    this._canAccessHethong = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public bool canAccessSuDung
        {
            get { return this._canAccessSuDung; }
            set
            {
                if (value != this._canAccessSuDung)
                {
                    this._canAccessSuDung = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public bool canAccessQuanLy
        {
            get { return this._canAccessQuanLy; }
            set
            {
                if (value != this._canAccessQuanLy)
                {
                    this._canAccessQuanLy = value;
                    NotifyPropertyChanged();
                }
            }
        }
        string _errorText = "";
        public string errorText
        {
            get { return this._errorText; }
            set
            {
                if (value != this._errorText)
                {
                    this._errorText = value;
                    if (_errorText != string.Empty)
                    {
                        _Error = BarItemVisibility.Always;
                    }
                    else
                    {
                        _Error = BarItemVisibility.Never;
                    }
                    NotifyPropertyChanged();
                }
            }
        }
        public BarItemVisibility Error
        {
            get { return this._Error; }
            set
            {
                if (value != this._Error)
                {
                    this._Error = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public bool plcRun
        {
            get { return this._plcRun; }
            set
            {
                if (value != this._plcRun)
                {
                    this._plcRun = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public string server
        {
            get { return this._server; }
            set
            {
                if (value != this._server)
                {
                    this._server = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public string PLCIP
        {
            get { return this._PLCIP; }
            set
            {
                if (value != this._PLCIP)
                {
                    this._PLCIP = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public string userName
        {
            get { return this._userName; }
            set
            {
                if (value != this._userName)
                {
                    this._userName = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public string thongtin
        {
            get { return this._thongtin; }
            set
            {
                if (value != this._thongtin)
                {
                    this._thongtin = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public string version
        {

            get { return this._version; }
            set
            {
                if (value != this._version)
                {
                    this._version = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public string nhomquyen
        {
            get { return this._nhomquyen; }
            set
            {
                if (value != this._nhomquyen)
                {
                    this._nhomquyen = value;
                    NotifyPropertyChanged();
                }
            }
        }
    }

}