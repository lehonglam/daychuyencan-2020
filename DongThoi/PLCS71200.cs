﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using S7.Net;
using System.Windows.Forms;
using System.Collections;

namespace DayChuyenHPEX.DongThoi
{
    public class PLCS71200
    {
        string _ipPLC="192.168.1.6";// PLC dong Thoi
        CpuType cpu = CpuType.S71200;
        short rack = 0;
        short slot = 0;
        public ErrorCode erPLC = ErrorCode.NoError;

        public int nSizeM = 1000;//500;
        public int nSizeQ = 100;//50;

        public byte[] bufM = null;// new byte[nSizeM];
        public byte[] bufQ = null;//new byte[nSizeQ];

        Plc _plcS71200 = null;
        string _tenPLC = "PLC";
        public string tenPLC { get { return _tenPLC; } set { _tenPLC = value; } }

        public PLCS71200()
        {
            bufM = new byte[nSizeM];
            bufQ = new byte[nSizeQ];
        }
        public void setIpPLC(string ip)
        {
            _ipPLC = ip;
        }
        public ErrorCode ketnoiPLC()
        {

            _plcS71200 = new Plc(CpuType.S71200, _ipPLC, rack, slot);
            erPLC = _plcS71200.Open();

            if (hdexGlobalData.erPLC == ErrorCode.NoError)
            {
                docToanBoDuLieuTuPLCbufM();
                docToanBoDuLieuTuPLCbufQ();
            }
            return erPLC;
        }
        public void Close()
        {
            try
            {
                _plcS71200 = new Plc(CpuType.S71200, _ipPLC, rack, slot);
                _plcS71200.Close();
            }
            catch(Exception ex)
            {

            }
        }

        public void docToanBoDuLieuTuPLCbufM()
        {
            if (erPLC == ErrorCode.NoError)
            {
                try
                {
                    bufM = _plcS71200.ReadBytes(DataType.Memory, 1, 0, nSizeM);
                }
                catch (Exception ex)
                {
                }
            }
        }
        public void docToanBoDuLieuTuPLCbufQ()
        {
            try
            {
                bufQ = _plcS71200.ReadBytes(DataType.Output, 1, 0, nSizeQ);
            }
            catch (Exception ex)
            {
            }
        }
        // Lay du lieu tai BUF Q
        public byte docDulieuTuBufQ(int vitri, int dichbit)
        {
            //(byte)((bufQ2[0] >> 4) & 0x01);
            if (vitri >= bufQ.Length) return 0;
            try
            {
                byte status = (byte)((bufQ[vitri] >> dichbit) & 0x01);
                return status;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return 0;
        }
        public int docDulieuTuBufM(int vitri)
        {

            int value = ((bufM[vitri] << 8) + bufM[vitri + 1]);
            return value;
        }
        public void ghiDulieuVaoPLC(int value, int vitritaiPLC)
        {
            if (erPLC == ErrorCode.NoError)
            {
                _plcS71200.WriteBytes(DataType.Memory, 1, vitritaiPLC, new byte[] { (byte)(value >> 8) });
                _plcS71200.WriteBytes(DataType.Memory, 1, vitritaiPLC + 1, new byte[] { (byte)(value >> 0) });
            }
        }
        public int docDulieuTuPLC(int vitritaiPLC)
        {
            int val = -1;
            if (erPLC == ErrorCode.NoError)
            {
                Byte[] byt = _plcS71200.ReadBytes(DataType.Memory, 1, vitritaiPLC, 2);
                val = (byt[vitritaiPLC] << 8) + byt[vitritaiPLC + 1];
            }
            return val;
        }
        public void daoBitTaiVitriThuocPLC(int vitriPLC, int dichbit)
        {
            DateTime now1 = DateTime.Now;
            Byte[] byt = null;
            try
            {
                //Byte[] 
                byt = _plcS71200.ReadBytes(DataType.Memory, 1, vitriPLC, 1);
            }
            catch (Exception ex)
            {
                string s = string.Format("{0},vitriPLC:{1}/dichbit:{2},", "_plcS71200.ReadBytes(DataType.Memory, 1, vitriPLC, 1); " + ex.Message, vitriPLC, dichbit);
                MessageBox.Show(s, "Loi dao bit xuong PLC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

//            Byte[] byt = _plcS71200.ReadBytes(DataType.Memory, 1, vitriPLC, 1);
            if(byt==null)
            {
                MessageBox.Show("Không đọc được PLC", "Loi _plcS71200.ReadBytes(DataType.Memory, 1, vitriPLC, 1);", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var bits = new BitArray(byt);
            //bool[] b = new bool[8];
            //for (int i = 0; i < 8; i++) b[i] = bits[i];

            bits[dichbit] = true;
            byte[] bytes = new byte[1];
            bits.CopyTo(bytes, 0);
            try
            {
                _plcS71200.WriteBytes(DataType.Memory, 1, vitriPLC, bytes);
            }
            catch (Exception ex)
            {
                string s = string.Format("{0},vitriPLC:{1}/dichbit:{2},", "_plcS71200.WriteBytes(DataType.Memory, 1, vitriPLC, bytes);" + ex.Message, vitriPLC, bytes);
                MessageBox.Show(s, "Loi dao bit xuong PLC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            //_plcS71200.WriteBytes(DataType.Memory, 1, vitriPLC, bytes);

            // Cho
            DateTime now2 = DateTime.Now;
            while ((now2 - now1).TotalMilliseconds < 300)
            {
                now2 = DateTime.Now;
            }



            // Bật bít tại vị trí bit thành 0 rồi ghi vao PLC
            try
            {
                byt = _plcS71200.ReadBytes(DataType.Memory, 1, vitriPLC, 1);
            }
            catch (Exception ex)
            {
                string s = string.Format("{0},vitriPLC:{1}/DataType.Memory:{2},", "byt = _plcS71200.ReadBytes(DataType.Memory, 1, vitriPLC, 1);" + ex.Message, vitriPLC, DataType.Memory);
                MessageBox.Show(s, "Loi dao bit xuong PLC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            //byt = _plcS71200.ReadBytes(DataType.Memory, 1, vitriPLC, 1);
            bits = new BitArray(byt);
            //for (int i = 0; i < 8; i++) b[i] = bits[i];
            bits[dichbit] = false;
            bytes = new byte[1];
            bits.CopyTo(bytes, 0);

            //_plcS71200.WriteBytes(DataType.Memory, 1, vitriPLC, bytes);
            try
            {
                _plcS71200.WriteBytes(DataType.Memory, 1, vitriPLC, bytes);
            }
            catch (Exception ex)
            {
                string s = string.Format("{0},vitriPLC:{1}/dichbit:{2},", "_plcS71200.WriteBytes(DataType.Memory, 1, vitriPLC, bytes);" + ex.Message, vitriPLC, bytes);
                MessageBox.Show(s, "_plcS71200.WriteBytes(DataType.Memory, 1, vitriPLC, bytes);", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }
        public void chayThietBi(int vitriPLC, int dichbit)
        {
            // Bật bít tại vị trí bit thành 1 rồi ghi vao PLC
            daoBitTaiVitriThuocPLC(vitriPLC, dichbit);
        }
        public void dungThietBi(int vitriPLC, int dichbit)
        {
            daoBitTaiVitriThuocPLC(vitriPLC, dichbit);
        }
    }
}
