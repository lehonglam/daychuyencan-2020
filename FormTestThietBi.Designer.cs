﻿namespace DayChuyenHPEX
{
    partial class FormTestThietBi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.vitriBitQtextBox = new System.Windows.Forms.TextBox();
            this.giaTriQtextBox2 = new System.Windows.Forms.TextBox();
            this.vitriMtextBox3 = new System.Windows.Forms.TextBox();
            this.giaTriMtextBox4 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dichBitQtextBox5 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.VitriMStartStoptextBox1 = new System.Windows.Forms.TextBox();
            this.vitriBitStartStoptextBox2 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 24);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(123, 20);
            this.button1.TabIndex = 0;
            this.button1.Text = "Ket noi PLC";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.ketNoiPLC);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(242, 258);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(123, 55);
            this.button2.TabIndex = 1;
            this.button2.Text = "Bật";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            this.button2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MouseDown);
            this.button2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.MouseUp);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(242, 319);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(123, 55);
            this.button3.TabIndex = 1;
            this.button3.Text = "Tắt";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(196, 131);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(123, 55);
            this.button4.TabIndex = 1;
            this.button4.Text = "Đọc trang thái";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.docTrangThaiBienM);
            // 
            // vitriBitQtextBox
            // 
            this.vitriBitQtextBox.Location = new System.Drawing.Point(80, 59);
            this.vitriBitQtextBox.Name = "vitriBitQtextBox";
            this.vitriBitQtextBox.Size = new System.Drawing.Size(100, 20);
            this.vitriBitQtextBox.TabIndex = 2;
            this.vitriBitQtextBox.Text = "12";
            // 
            // giaTriQtextBox2
            // 
            this.giaTriQtextBox2.Location = new System.Drawing.Point(355, 59);
            this.giaTriQtextBox2.Name = "giaTriQtextBox2";
            this.giaTriQtextBox2.Size = new System.Drawing.Size(100, 20);
            this.giaTriQtextBox2.TabIndex = 2;
            // 
            // vitriMtextBox3
            // 
            this.vitriMtextBox3.Location = new System.Drawing.Point(80, 149);
            this.vitriMtextBox3.Name = "vitriMtextBox3";
            this.vitriMtextBox3.Size = new System.Drawing.Size(100, 20);
            this.vitriMtextBox3.TabIndex = 2;
            this.vitriMtextBox3.Text = "120";
            // 
            // giaTriMtextBox4
            // 
            this.giaTriMtextBox4.Location = new System.Drawing.Point(355, 149);
            this.giaTriMtextBox4.Name = "giaTriMtextBox4";
            this.giaTriMtextBox4.Size = new System.Drawing.Size(100, 20);
            this.giaTriMtextBox4.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Vị trí Q";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 152);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "vị trí M";
            // 
            // dichBitQtextBox5
            // 
            this.dichBitQtextBox5.Location = new System.Drawing.Point(80, 85);
            this.dichBitQtextBox5.Name = "dichBitQtextBox5";
            this.dichBitQtextBox5.Size = new System.Drawing.Size(100, 20);
            this.dichBitQtextBox5.TabIndex = 2;
            this.dichBitQtextBox5.Text = "12";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 89);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Dich Bit";
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(196, 50);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(123, 55);
            this.button5.TabIndex = 1;
            this.button5.Text = "Đọc";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.DocTrangThaiChay);
            // 
            // VitriMStartStoptextBox1
            // 
            this.VitriMStartStoptextBox1.Location = new System.Drawing.Point(126, 258);
            this.VitriMStartStoptextBox1.Name = "VitriMStartStoptextBox1";
            this.VitriMStartStoptextBox1.Size = new System.Drawing.Size(100, 20);
            this.VitriMStartStoptextBox1.TabIndex = 2;
            this.VitriMStartStoptextBox1.Text = "34";
            // 
            // vitriBitStartStoptextBox2
            // 
            this.vitriBitStartStoptextBox2.Location = new System.Drawing.Point(126, 284);
            this.vitriBitStartStoptextBox2.Name = "vitriBitStartStoptextBox2";
            this.vitriBitStartStoptextBox2.Size = new System.Drawing.Size(100, 20);
            this.vitriBitStartStoptextBox2.TabIndex = 2;
            this.vitriBitStartStoptextBox2.Text = "1";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 261);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Vị trí M Start/Stop";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 288);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Vi trí Bit";
            // 
            // FormTestThietBi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(533, 401);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.giaTriMtextBox4);
            this.Controls.Add(this.giaTriQtextBox2);
            this.Controls.Add(this.vitriMtextBox3);
            this.Controls.Add(this.vitriBitStartStoptextBox2);
            this.Controls.Add(this.dichBitQtextBox5);
            this.Controls.Add(this.VitriMStartStoptextBox1);
            this.Controls.Add(this.vitriBitQtextBox);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Name = "FormTestThietBi";
            this.Text = "Kiểm tra PLC";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox vitriBitQtextBox;
        private System.Windows.Forms.TextBox giaTriQtextBox2;
        private System.Windows.Forms.TextBox vitriMtextBox3;
        private System.Windows.Forms.TextBox giaTriMtextBox4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox dichBitQtextBox5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.TextBox VitriMStartStoptextBox1;
        private System.Windows.Forms.TextBox vitriBitStartStoptextBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
    }
}