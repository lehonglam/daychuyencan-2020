﻿namespace DayChuyenHPEX.DoThi
{
    partial class UserControlDoThiHPEX
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.TocDoBomOXH1zedGraphControl1 = new ZedGraph.ZedGraphControl();
            this.SuspendLayout();
            // 
            // TocDoBomOXH1zedGraphControl1
            // 
            this.TocDoBomOXH1zedGraphControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TocDoBomOXH1zedGraphControl1.IsSynchronizeXAxes = true;
            this.TocDoBomOXH1zedGraphControl1.IsSynchronizeYAxes = true;
            this.TocDoBomOXH1zedGraphControl1.Location = new System.Drawing.Point(3, 3);
            this.TocDoBomOXH1zedGraphControl1.Name = "TocDoBomOXH1zedGraphControl1";
            this.TocDoBomOXH1zedGraphControl1.ScrollGrace = 0D;
            this.TocDoBomOXH1zedGraphControl1.ScrollMaxX = 0D;
            this.TocDoBomOXH1zedGraphControl1.ScrollMaxY = 0D;
            this.TocDoBomOXH1zedGraphControl1.ScrollMaxY2 = 0D;
            this.TocDoBomOXH1zedGraphControl1.ScrollMinX = 0D;
            this.TocDoBomOXH1zedGraphControl1.ScrollMinY = 0D;
            this.TocDoBomOXH1zedGraphControl1.ScrollMinY2 = 0D;
            this.TocDoBomOXH1zedGraphControl1.Size = new System.Drawing.Size(558, 245);
            this.TocDoBomOXH1zedGraphControl1.TabIndex = 5;
            this.TocDoBomOXH1zedGraphControl1.ZoomButtons2 = System.Windows.Forms.MouseButtons.Right;
            // 
            // UserControlDoThiHPEX
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.TocDoBomOXH1zedGraphControl1);
            this.Name = "UserControlDoThiHPEX";
            this.Size = new System.Drawing.Size(564, 262);
            this.Load += new System.EventHandler(this.UserControlDoThiHPEX_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private ZedGraph.ZedGraphControl TocDoBomOXH1zedGraphControl1;
    }
}
