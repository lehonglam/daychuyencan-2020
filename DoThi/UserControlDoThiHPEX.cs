﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZedGraph;
namespace DayChuyenHPEX.DoThi
{
    public partial class UserControlDoThiHPEX : UserControl
    {
        string _xAxisTitle="t", _yAxisTitle="Nhiệt độ",_Label="Label";
        Color _color = Color.Red;
        double _XAxisScaleMin = 0,
                _XAxisScaleMax = 1200,
                _XAxisScaleMinorStep = 1,
                _XAxisScaleMajorStep = 50;
        string _maThuoctinh;
        public int idCa = -1;
        public int idThuocTinh=-1;
        [Category("AppearanceTDIN")]
        public string MaThuocTinh { get { return _maThuoctinh; } set { _maThuoctinh = value; ; } }
        [Category("AppearanceTDIN")]
        public string xAxisTitle { get { return _xAxisTitle; } set { _xAxisTitle = value;  } }
        [Category("AppearanceTDIN")]
        public string yAxisTitle { get { return _yAxisTitle; } set { _yAxisTitle = value; } }
        [Category("AppearanceTDIN")]
        public string Label { get { return _Label; } set { _Label = value;  } }
        [Category("AppearanceTDIN")]
        public Color Color { get { return _color; } set { _color = value;  } }
        [Category("AppearanceTDIN")]
        public double XAxisScaleMin { get { return _XAxisScaleMin; } set { _XAxisScaleMin = value;  } }
        [Category("AppearanceTDIN")]
        public double XAxisScaleMax { get { return _XAxisScaleMax; } set { _XAxisScaleMax = value;  } }
        [Category("AppearanceTDIN")]
        public double XAxisScaleMinorStep { get { return _XAxisScaleMinorStep; } set { _XAxisScaleMinorStep = value;  } }
        [Category("AppearanceTDIN")]
        public double XAxisScaleMajorStep { get { return _XAxisScaleMajorStep; } set { _XAxisScaleMajorStep = value;  } }

        public UserControlDoThiHPEX()
        {
            InitializeComponent();
        }
        public void capNhatDoThi()
        {
            // doc thong so cac diem roi ve
            GraphPane myPane = TocDoBomOXH1zedGraphControl1.GraphPane; // Khai báo sửa dụng Graph loại GraphPane;
            var list=myPane.CurveList[0];
            list.Clear();
            myPane.Title.Text = _maThuoctinh;
            list.Label.Text = _maThuoctinh;
        }
        private void UserControlDoThiHPEX_Load(object sender, EventArgs e)
        {
            GraphPane myPane = TocDoBomOXH1zedGraphControl1.GraphPane; // Khai báo sửa dụng Graph loại GraphPane;
            // Các thông tin cho đồ thị của mình
            myPane.Title.Text = _maThuoctinh;
            myPane.XAxis.Title.Text = xAxisTitle;
            myPane.YAxis.Title.Text = yAxisTitle;
            // Định nghĩa list để vẽ đồ thị. Để các bạn hiểu rõ cơ chế làm việc ở đây khai báo 2 list điểm <=> 2 đường đồ thị
            RollingPointPairList list1 = new RollingPointPairList(1200);
            // dòng dưới là định nghĩa curve để vẽ.
            LineItem curve1 = myPane.AddCurve(Label, list1, Color, SymbolType.None); // Color màu đỏ, đặc trưng cho đường 1
            // Định hiện thị cho trục thời gian (Trục X)
            myPane.XAxis.Scale.Min = XAxisScaleMin;  // Min  = 0;
            myPane.XAxis.Scale.Max = XAxisScaleMax; // Mã  = 30;
            myPane.XAxis.Scale.MinorStep = XAxisScaleMinorStep;  // Đơn vị chia nhỏ nhất 1
            myPane.XAxis.Scale.MajorStep = XAxisScaleMajorStep; // Đơn vị chia lớn 5
            TocDoBomOXH1zedGraphControl1.ZoomOutAll(myPane);
        }
        public void  addPoint(double x, double y)
        {
            LineItem curve1 = TocDoBomOXH1zedGraphControl1.GraphPane.CurveList[0] as LineItem;
            curve1.AddPoint(x, y);
        }
        public void setCaId(int caId)
        {
            idCa = caId;
        }
    }
}
