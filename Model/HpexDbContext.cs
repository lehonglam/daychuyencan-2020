﻿using System;
using DevExpress.Xpo;

namespace DayChuyenHPEX.Model
{
    using System.Data.Entity;
    using System.Data.Entity.Core.EntityClient;
    public partial class tdincanEntities1 : DbContext
    {
        public tdincanEntities1(string sConnectionString) : base(sConnectionString) { }
    }
    public class HpexDbContext : tdincanEntities1
    {
        public HpexDbContext() : base()
        {
            Configuration.LazyLoadingEnabled = true;
        }

        public HpexDbContext(string connectionString) : base(connectionString)
        {
            Configuration.LazyLoadingEnabled = true;
        }

        public static string BuildConnnectionString(string sDataSource, string database, string user, string pass)
        {
            EntityConnectionStringBuilder ecb = new EntityConnectionStringBuilder();
            ecb.Metadata = "res://*/Model.ModelHPEX.csdl|res://*/Model.ModelHPEX.ssdl|res://*/Model.ModelHPEX.msl";
            ecb.Provider = "System.Data.SqlClient";
            ecb.ProviderConnectionString = $"data source={sDataSource};initial catalog={database};user id={user};password={pass};" +
                                            $"MultipleActiveResultSets=True;App=EntityFramework";
            return ecb.ToString();
            //
            //connectionString = "metadata=res://*/Model.ModelKhachHang.csdl|res://*/Model.ModelKhachHang.ssdl|res://*/Model.ModelKhachHang.msl;
            //provider=System.Data.SqlClient;
            //provider connection string=&quot;data source=123.16.189.33;initial catalog=ban247;persist security info=True;user id=ban247_new;multipleactiveresultsets=True;application name=EntityFramework&quot;" providerName = "System.Data.EntityClient" /></ connectionStrings >
        }

    }
}