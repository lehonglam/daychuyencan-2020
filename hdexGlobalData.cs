﻿using System;
using System.Collections.Generic;
using S7.Net;
using System.Windows.Forms;
using System.Security.Cryptography;
using System.IO;
using System.Text;
using System.Collections;
namespace DayChuyenHPEX
{
    static public class hdexGlobalData
    {
        static public bool bThietlapHethong=false;
        public static string codemahoaHDD   ="123bcLacasy";
        public const  string codemahoa      ="!#$a54?3ju223td123bc";
        public static string codemahoa2     ="!#$a54?3ju223td123bc";

        static public logInDataStatic trangthaiLogin = new logInDataStatic();
        static public int idCalamviec = -1;
        static public string caTen="";// ten ca lam viec
        static public string tenNguoiDung= "";// ten ca lam viec
        static public DateTime giobatdau=DateTime.Now;
        static public phanquyen phanQuyen = new phanquyen();
        static public int thoigianGhiDulieu = 30000; // mm giay

        // Thong tin chung
        public static string datasource     = "datasource";
        public static string database       = "database";
        public static string userDatabase   = "user";
        public static string PassDatabase   = "pass";

        // Du liệu PLC
        static public Plc plc;
        static public int nSizeM    = 1000;
        static public int nSizeQ    = 200;
        static public byte[] bufM   = new byte[nSizeM];
        static public byte[] bufQ   = new byte[nSizeQ];


        static public string ipPLC = "192.168.1.5";
        static public CpuType cpu = CpuType.S71200;
        static public short rack = 0;
        static public short slot = 0;
        static public ErrorCode erPLC = ErrorCode.NoError;
        //
        static public List<string> danhsachThietbi = new List<string>();
        // Danh muc Mã Thiết bị, và vị trí thuoc PLC
        static public Dictionary<string, thietbi> danhmucthietbiViTriPLC = new Dictionary<string, thietbi>();

        public static byte ToByte(this BitArray bits)
        {
            if (bits.Count != 8)
            {
                throw new ArgumentException("bits");
            }
            byte[] bytes = new byte[1];
            bits.CopyTo(bytes, 0);
            return bytes[0];
        }
        // HAM
        // Ham doc toan bo thiet bi va dua vao danh sach

        static public void docToanBoThietBiTuDataBase()
        {
        }
        public static void daoBitTaiVitriThuocPLC(int vitriPLC,int dichbit)
        {
            if(plc==null)
            {
                MessageBox.Show("Chưa kết nối PLC");
                return;
            }
            DateTime now1 = DateTime.Now;
            Byte[] byt = hdexGlobalData.plc.ReadBytes(DataType.Memory, 1, vitriPLC, 1);
            var bits = new BitArray(byt);
            //bool[] b = new bool[8];
            //for (int i = 0; i < 8; i++) b[i] = bits[i];

            bits[dichbit] = true;
            byte[] bytes = new byte[1];
            bits.CopyTo(bytes, 0);
            hdexGlobalData.plc.WriteBytes(DataType.Memory, 1, vitriPLC, bytes);

            // Cho
            DateTime now2 = DateTime.Now;
            while ((now2 - now1).TotalMilliseconds < 300)
            {
                now2 = DateTime.Now;
            }

            // Bật bít tại vị trí bit thành 0 rồi ghi vao PLC
            byt = hdexGlobalData.plc.ReadBytes(DataType.Memory, 1, vitriPLC, 1);
            bits = new BitArray(byt);
            //for (int i = 0; i < 8; i++) b[i] = bits[i];
            bits[dichbit] = false;
            bytes = new byte[1];
            bits.CopyTo(bytes, 0);
            hdexGlobalData.plc.WriteBytes(DataType.Memory, 1, vitriPLC, bytes);
        }
        static public void chayThietBi(int vitriPLC, int dichbit)
        {
            // Bật bít tại vị trí bit thành 1 rồi ghi vao PLC
            daoBitTaiVitriThuocPLC(vitriPLC, dichbit);
        }
        static public void dungThietBi(int vitriPLC, int dichbit)
        {
            daoBitTaiVitriThuocPLC(vitriPLC, dichbit);
        }

        static public string connectionstring()
        {
            string str = "Data Source=" + datasource + ";";
            str = str + "Initial Catalog=" + database + ";";
            if (userDatabase != "")
                str = str + "User ID=" + userDatabase + ";";
            try
            {
                mahoastringtheoCrypto mahoa = new mahoastringtheoCrypto();
                string pass = mahoa.DecryptQueryString(PassDatabase, codemahoa);
                if (pass != "")
                    str = str + "Password=" + pass;
            }
            catch
            {
                return "";
            }
            return str;
        }
        static public bool kiemtraTepBanQuyen()
        {
            {
                Microsoft.Win32.RegistryKey key;
                key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(Properties.Settings.Default.registryName);
                if (key == null)
                {
                    errorPro.errorProClass.errorMessage("Lỗi bản quyền");
                    return false;
                }
                string __codeMahoabanquyen = (string)key.GetValue(Properties.Settings.Default.codename);
                if (__codeMahoabanquyen == null || __codeMahoabanquyen.Length == 0)
                {
                    errorPro.errorProClass.errorMessage("Lỗi bản quyền");
                    return false;
                }

                bool b = formBanquyen.kiemtraKeySanphamNhapvao(__codeMahoabanquyen);
                if (b == false)
                {
                    errorPro.errorProClass.errorMessage("Lỗi bản quyền");
                    return false;
                }

            }
            return true;
        }
        static public string GetHDDSerialNumber(string drive)
        {
            return FingerPrint.Value();
        }
    }

    public class mahoastringtheoCrypto
    {
        public mahoastringtheoCrypto()
        {
        }
        private byte[] key = { };
        private byte[] IV = { 18, 52, 86, 120, 144, 171, 205, 239 };

        private string Decrypt(string stringToDecrypt, string sEncryptionKey)
        {

            byte[] inputByteArray = new byte[stringToDecrypt.Length + 1];
            try
            {
                key = System.Text.Encoding.UTF8.GetBytes(Left(sEncryptionKey, 8));
                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                inputByteArray = Convert.FromBase64String(stringToDecrypt);
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(key, IV), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                System.Text.Encoding encoding = System.Text.Encoding.UTF8;
                return encoding.GetString(ms.ToArray());
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        private string Encrypt(string stringToEncrypt, string SEncryptionKey)
        {
            try
            {
                key = System.Text.Encoding.UTF8.GetBytes(Left(SEncryptionKey, 8));
                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                byte[] inputByteArray = Encoding.UTF8.GetBytes(stringToEncrypt);
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(key, IV), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);

                cs.FlushFinalBlock();
                return Convert.ToBase64String(ms.ToArray());
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        private string Left(string MyString, int length)
        {
            string tmpstr = MyString.Substring(0, length);
            return tmpstr;
        }

        public string EncryptQueryString(string strQueryString, string codemahoa2)
        {
            return Encrypt(strQueryString, codemahoa2);
        }

        public string DecryptQueryString(string strQueryString, string codemahoa2)
        {
            return Decrypt(strQueryString, codemahoa2);
        }
    }
}
