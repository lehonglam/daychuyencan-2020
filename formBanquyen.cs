﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DayChuyenHPEX
{
    public partial class formBanquyen : Form
    {
        string hddId = "";
        string keySanpham = "";
        public bool banquyenOK = false;
        public formBanquyen()
        {
            InitializeComponent();
        }

        private void formBanquyen_Load(object sender, EventArgs e)
        {
            // dọc mã code bản quyền
            docCode();
            docKeyBanQuyen();
            kiemtraKeySanphamNhapvao();
        }
        private void docCode()
        {
            macodeMaytinh.Text =hddId = hdexGlobalData.GetHDDSerialNumber("c");
//            mahoastringtheoCrypto mahoa = new mahoastringtheoCrypto();
            //macodeMaytinh.Text = mahoa.EncryptQueryString(hddId, configData.codemahoaHDD);
        }

        // kiem tra key san pham co dung khong
        bool kiemtraKeySanphamNhapvao()
        {
            labelInfo.Text = "Chưa đăng ký - đang kiểm tra...";
            string __codeMahoabanquyen = textBoxkeySanpham.Text;
            bool b=kiemtraKeySanphamNhapvao(__codeMahoabanquyen);
            if (b==false)
            {
                //messageCalasy.show("bản quyền", "Mã bản quyền lỗi");
                return false;
            }
            labelInfo.Text = "Bản quyền OK";
            return true;

        }
        public static bool kiemtraKeySanphamNhapvao(string key)
        {
            string __codeMahoabanquyen = key;

            string s = hdexGlobalData.GetHDDSerialNumber("c");
            string codeHDD = FingerPrint.GetHash(s);
            if (__codeMahoabanquyen != codeHDD)
            {
                //messageCalasy.show("bản quyền", "Mã bản quyền lỗi");
                return false;
            }
            return true;
        }

        public bool docKeyBanQuyen()
        {
            {
                textBoxkeySanpham.Text = "";
                Microsoft.Win32.RegistryKey key;
                key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(Properties.Settings.Default.registryName);
                if (key == null)
                {
                    return false;
                }
                textBoxkeySanpham.Text = (string)key.GetValue(Properties.Settings.Default.codename);
            }
            return true;
        }
        public bool xoaKeyBanQuyen()
        {
            ghiRaregistry("");
            return true;
        }
        // ghi key san pham vao registry
        void ghiRaregistry()
        {
            bool b=kiemtraKeySanphamNhapvao();
            if (b == false)
            {
                MessageBox.Show("bản quyền", "Mã bản quyền lỗi");
                return;
            }
            keySanpham =textBoxkeySanpham.Text;
            ghiRaregistry(keySanpham);
        }
        void ghiRaregistry(string s)
        {
            Microsoft.Win32.RegistryKey mykey;
            mykey = Microsoft.Win32.Registry.CurrentUser.CreateSubKey(Properties.Settings.Default.registryName);
            mykey.SetValue(Properties.Settings.Default.codename, s);
            mykey.Close();
        }

        private void ghiKeyBanQuyen(object sender, EventArgs e)
        {
            ghiRaregistry();
        }

        private void taoKeyBanQuyen(object sender, EventArgs e)
        {
            // lay code nhap tu HDD
            mahoastringtheoCrypto mahoa = new mahoastringtheoCrypto();
            hddId=mahoa.DecryptQueryString(macodeMaytinh.Text, hdexGlobalData.codemahoaHDD);
            //
            textBoxkeySanpham.Text=mahoa.EncryptQueryString(hddId, hdexGlobalData.codemahoa2);
        }

        private void keysanhamKeyDown(object sender, KeyEventArgs e)
        {

        }

        private void xoacodeBanQuyen(object sender, EventArgs e)
        {
            // hoi xem thuc su muon xoa khong
            var res = MessageBox.Show("Nếu bạn khẳng định xóa mã bản quyền, bạn không tự tạo được mã bản quyền", "Xóa mã bản quyền", MessageBoxButtons.YesNoCancel);
            if(res==DialogResult.Yes)
            {
                // thuc hien xoa ma ban quyen
                xoaKeyBanQuyen();
                docKeyBanQuyen();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // OnOK
            banquyenOK= kiemtraKeySanphamNhapvao();
        }
    }
}
