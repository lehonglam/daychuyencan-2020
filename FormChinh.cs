﻿using DayChuyenHPEX.DieuKhien;
using DayChuyenHPEX.DongThoi;
using DayChuyenHPEX.Model;
using quanlyLogIn;
using quanlyLogIn.Model;
using quanlyLogIn.Model.DAO;
using S7.Net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace DayChuyenHPEX
{
    public partial class FormChinh : DevExpress.XtraEditors.XtraForm
    {
        #region DATA
        int thoigianghidulieu = 0;
        public quanlyLogIn.Model.nsNhanVien nhanvienLogin = null;
        loginGlobalData loginData = new loginGlobalData();
        configBindingHPEX confid = new configBindingHPEX();
        SysUserPermit sysPermit = new SysUserPermit();
        HpexDbContext dbContext = null;
        #endregion
        #region CONTRUCTOR-LOAD
        public FormChinh()
        {
            var f = new quanlyLogIn.logInForm();
            if (f.ShowDialog() == DialogResult.OK)
            {
                loginData = logInForm.globalData;
                Login(loginData);
            }
            InitializeComponent();
        }
        private void FormChinh_Load(object sender, EventArgs e)
        {
            confid.version=$"Phiên bản {Assembly.GetExecutingAssembly().GetName().Version.ToString()}";
            configBindingHPEXBindingSource.DataSource = confid;
            docThongSoXML();

        }
        bool Login(loginGlobalData loginDataIn)
        {
            string passChuaGiaiMa = LAM_Base.mahoastringtheoCrypto.DecryptQueryStringStatic(loginDataIn.databasepass);
            string sConnecTionString = HpexDbContext.BuildConnnectionString(loginDataIn.server, loginDataIn.database,
                loginDataIn.databaseuser, passChuaGiaiMa);

            dbContext = new HpexDbContext(sConnecTionString);

            if (dbContext.Database.Exists() == false)
            {
                errorPro.errorProClass.errorMessage(errorPro.erroCodeE.loiKetnoiCSDL);
                return false;
            }
            nhanvienLogin = loginDataIn.nhanvien;
            sysPermit = loginDataIn.listSysPermit.SingleOrDefault(x => x.FunctionID == ProgramFunction.SU_DUNG.ToString());
            var sHethong=loginDataIn.listSysPermit.SingleOrDefault(x => x.FunctionID == ProgramFunction.HE_THONG.ToString());
            var sQuanly= loginDataIn.listSysPermit.SingleOrDefault(x => x.FunctionID == ProgramFunction.QUAN_LY.ToString());
            confid.canAccessHethong= sHethong.AllowAccess.Value;
            confid.canAccessQuanLy= sHethong.AllowAccess.Value;

            setQuyen(confid, sysPermit);
            confid.server= loginDataIn.server;
            confid.userName= nhanvienLogin.Name;
            confid.nhomquyen = loginDataIn.sysUser.SysUserGroup.GroupName;
            quanlyThongBao.thongBao.themThongBao_theoNhom(loginData, quanlyThongBao.thongBao.nhomthongbao.hethong, "login", "login he thong", nhanvienLogin.Id);
            return true;
        }
        void setQuyen(configBindingHPEX cf, SysUserPermit pm)
        {
            if (pm != null)
            {
                cf.canAddNew = pm.AllowAdd.HasValue ? pm.AllowAdd.Value : false;
                cf.canAccess = pm.AllowAccess.HasValue ? pm.AllowAccess.Value : false;
                cf.canDelete = pm.AllowDelete.HasValue ? pm.AllowDelete.Value : false;
                cf.canEdit = pm.AllowEdit.HasValue ? pm.AllowEdit.Value : false;
                cf.canPrint = pm.AllowPrint.HasValue ? pm.AllowPrint.Value : false;
                cf.canReport = pm.AllowExport.HasValue ? pm.AllowExport.Value : false;
            }
        }
        private void clickServerLogin(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var f = new quanlyLogIn.logInForm();
            if (f.ShowDialog() == DialogResult.OK)
            {
                loginData = logInForm.globalData;
                Login(loginData);
                Refresh();
            }
        }
        #endregion
        #region NhOM HAM KHOI TAO BAN DAU
        void ganPlcVaoControl(Panel p, PLCS71200 plc)
        {
            repositoryItemProgressBarBottom.Step = 0;
            repositoryItemProgressBarBottom.Maximum = p.Controls.Count;
            foreach (Control control in p.Controls)
            {
                var t = control.GetType().IsSubclassOf(typeof(basePLCControl));
                if (t == true)
                {
                    DieuKhien.basePLCControl o = (DieuKhien.basePLCControl)control;
                    o.plc = plc;
                }
                repositoryItemProgressBarBottom.Step++;
            }
            repositoryItemProgressBarBottom.Step = 0;
        }
        bool ketNoiPLC(PLCS71200 x)
        {
            try
            {
                x.erPLC = x.ketnoiPLC();
                if (x.erPLC == S7.Net.ErrorCode.NoError)
                {
                    confid.errorText = $"Ket noi PLC tot";
                    return true;
                }
                else
                {
                    confid.errorText = $"Ket noi PLC bi loi {x.erPLC.ToString()}";
                    errorPro.errorProClass.errorMessage(confid.errorText);
                    return false;
                }
                
            }
            catch (Exception ex)
            {
                confid.errorText = $"Ket noi PLC bi loi";
                return false;
            }
        }
        void khoiTaoGanPLCChoThietBi(PLCS71200 plc)
        {
            ganPlcVaoControl(panelPhanChinh, plc);
            ganPlcVaoControl(panelBaoLoi, plc);
        }
        void khoitaoHeThong()
        {
            splashScreenManagerForm.ShowWaitForm();
            repositoryItemProgressBarChinh.Step++;
            ucPLCChinh.trangThaiKetNoi = ketNoiPLC(ucPLCChinh.plc);
            if (ucPLCChinh.trangThaiKetNoi==true)
            {
                repositoryItemProgressBarChinh.Step++;
                khoiTaoGanPLCChoThietBi(ucPLCChinh.plc);
                repositoryItemProgressBarChinh.Step++;
                // tao menu chu trinh
                timerChayChinh.Start();
            }
            splashScreenManagerForm.CloseWaitForm();
            repositoryItemProgressBarChinh.Step = 0;
        }
        private void khoiTaoPLC(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            khoitaoHeThong();
        }
        // Hàm đọc toàn bộ Control theo mã và đưa vào danh sách
        #endregion
        #region NHOM HAM THUC HIỆN KHI TIMER STICK
        void CapNhatGiaTriControlTrongPanel(Panel p)
        {
            foreach (Control control in p.Controls)
            {
                var t = control.GetType().IsSubclassOf(typeof(basePLCControl));
                if (t == true)
                {
                    DieuKhien.basePLCControl o = (DieuKhien.basePLCControl)control;
                    o.capNhatGiatri();
                }
            }
        }
        void capNhatThongSoChuongTrinhLaBelhienThiNhanh()
        {
            CapNhatGiaTriControlTrongPanel(panelBaoLoi);
            CapNhatGiaTriControlTrongPanel(panelPhanChinh);

        }
        private void timerChuongTrinhChinhStick(object sender, EventArgs e)
        {
            // Đây là timer chạy chính trong chương trình: cứ 100mms thì đọc thông số cập nhật từ PLC 1 lần
            // sau đó cập nhật trang thái thiết bị và trang thai thông số
            //B1: Đọc cập nhật thông tin từ PLC
            ucPLCChinh.docToanBoDuLieuTuPLCbufM();
            ucPLCChinh.docToanBoDuLieuTuPLCbufQ();
            // Cập nhật gio he thong
            ghiDulieuTheoThoiGian();

            capNhatThongSoChuongTrinhLaBelhienThiNhanh();
        }
        #endregion
        #region NHOM HAM TEST
        // Ham Test he thong
        private void chayToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in panelPhanChinh.Controls)
            {
                if (ct.GetType() == typeof(DieuKhien.UserControlThietBi))
                {
                    var bom = (DieuKhien.UserControlThietBi)ct;
                    bom.setTrangThai(DieuKhien.trangthaihoatdong.hoatdong);
                }
            }
        }

        private void testDungToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in panelPhanChinh.Controls)
            {
                if (ct.GetType() == typeof(DieuKhien.UserControlThietBi))
                {
                    var bom = (DieuKhien.UserControlThietBi)ct;
                    bom.setTrangThai(DieuKhien.trangthaihoatdong.dunghoatdong);
                }
            }
        }
        #endregion
        #region VEDOTHI
        void ghiDulieuTheoThoiGian()
        {
            if (thoigianghidulieu > hdexGlobalData.thoigianGhiDulieu)
            {
                // hàm ghi due liệu theo thời gian khi timer stick
                thoigianghidulieu = 0;
            }
            else
            {
                thoigianghidulieu += timerChayChinh.Interval;
            }

        }

        #endregion
        #region CHUC NANG MENU
        private void formClose(object sender, FormClosingEventArgs e)
        {
            // ghi thoong so ra xml 
            ghiThongSoXML();
        }
        private void btnNhanSu_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            (new quanlyNhanSu.devFormNhanSu(loginData)).ShowDialog();
        }
        private void logViewClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            (new quanlyThongBao.FormQuanlyThongBao()).ShowDialog();
        }
        private void userControlThuocTinhGhiXuongPLCTanSoDatBT1_Load(object sender, EventArgs e)
        {

        }
        private void userControlLabelHienThiNhanh2_Load(object sender, EventArgs e)
        {


        }
        #endregion
        #region XML
        void docThongSoXML(string xmlFileName= "test.xml")
        {
            Cursor = Cursors.WaitCursor;
            if (false == File.Exists(xmlFileName))
            {
                try
                {
                    ghiThongSoXML(xmlFileName);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Lỗi tạo tệp xml " + ex.Message, "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }

            XmlTextReader textReader = new XmlTextReader(xmlFileName);
            // Read until end of file  
            bool res =textReader.Read();
            if(res==false)
            {
                errorPro.errorProClass.errorMessage($"Loi doc tep XML");
                Cursor = Cursors.Default;
                return;
            }
            // load reader   
            XmlDocument doc = new XmlDocument();
            doc.Load(textReader);
            repositoryItemProgressBarBottom.Minimum = 0;
            repositoryItemProgressBarBottom.Step= 0;
            repositoryItemProgressBarBottom.Maximum = panelPhanChinh.Controls.Count;


            foreach (Control control in panelPhanChinh.Controls)
            {
                var t = control.GetType().IsSubclassOf(typeof(basePLCControl));
                if (t == true)
                {
                    DieuKhien.basePLCControl o = (DieuKhien.basePLCControl)control;
                    o.readDataFromFile(doc);
                }
                repositoryItemProgressBarBottom.Step++;

            }
            foreach (Control control in panelBaoLoi.Controls)
            {
                var t = control.GetType().IsSubclassOf(typeof(basePLCControl));
                if (t == true)
                {
                    DieuKhien.basePLCControl o = (DieuKhien.basePLCControl)control;
                    o.readDataFromFile(doc);
                }
            }
            repositoryItemProgressBarBottom.Step = 0;
            textReader.Close();
            Cursor = Cursors.Default;
        }
        void ghiThongSoXML(string xmlFileName = "test.xml")
        {
            // doc thong so xml
            Cursor = Cursors.WaitCursor;
            XmlTextWriter xmlWriter = new XmlTextWriter(xmlFileName, null);
            xmlWriter.Formatting = Formatting.Indented;

            xmlWriter.WriteStartDocument();
            xmlWriter.WriteStartElement("thong-so-thiet-bi");
            foreach (Control control in panelPhanChinh.Controls)
            {
                var t = control.GetType().IsSubclassOf(typeof(basePLCControl));
                if (t == true)
                {
                    DieuKhien.basePLCControl o = (DieuKhien.basePLCControl)control;
                    o.writeDataFromFile(xmlWriter);
                }
            }
            foreach (Control control in panelBaoLoi.Controls)
            {
                var t = control.GetType().IsSubclassOf(typeof(basePLCControl));
                if (t == true)
                {
                    DieuKhien.basePLCControl o = (DieuKhien.basePLCControl)control;
                    o.writeDataFromFile(xmlWriter);
                }
            }

            xmlWriter.WriteEndDocument();
            xmlWriter.Close();
            Cursor = Cursors.Default;

        }
        #endregion
        private void ProgressbarEditItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

        }
        private void loiXuatHien(object sender, EventArgs e)
        {

        }
        private void userControlDaoBitXoaLoi_Load(object sender, EventArgs e)
        {

        }

        private void userControlLabelHienThiNhanh1_Load(object sender, EventArgs e)
        {

        }

        private void panelPhanChinh_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
