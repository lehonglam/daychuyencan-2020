﻿namespace DayChuyenHPEX
{
    partial class formBanquyen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.macodeMaytinh = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxkeySanpham = new System.Windows.Forms.TextBox();
            this.buttonNapKeyBanQuyen = new System.Windows.Forms.Button();
            this.buttonXoaBanQuyen = new System.Windows.Forms.Button();
            this.labelInfo = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "mã đăng ký";
            // 
            // macodeMaytinh
            // 
            this.macodeMaytinh.Location = new System.Drawing.Point(90, 6);
            this.macodeMaytinh.Name = "macodeMaytinh";
            this.macodeMaytinh.Size = new System.Drawing.Size(420, 20);
            this.macodeMaytinh.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Key sản phẩm";
            // 
            // textBoxkeySanpham
            // 
            this.textBoxkeySanpham.Location = new System.Drawing.Point(90, 32);
            this.textBoxkeySanpham.Name = "textBoxkeySanpham";
            this.textBoxkeySanpham.Size = new System.Drawing.Size(420, 20);
            this.textBoxkeySanpham.TabIndex = 1;
            this.textBoxkeySanpham.KeyDown += new System.Windows.Forms.KeyEventHandler(this.keysanhamKeyDown);
            // 
            // buttonNapKeyBanQuyen
            // 
            this.buttonNapKeyBanQuyen.Location = new System.Drawing.Point(233, 58);
            this.buttonNapKeyBanQuyen.Name = "buttonNapKeyBanQuyen";
            this.buttonNapKeyBanQuyen.Size = new System.Drawing.Size(113, 23);
            this.buttonNapKeyBanQuyen.TabIndex = 2;
            this.buttonNapKeyBanQuyen.Text = "Ghi Key vào máy";
            this.buttonNapKeyBanQuyen.UseVisualStyleBackColor = true;
            this.buttonNapKeyBanQuyen.Click += new System.EventHandler(this.ghiKeyBanQuyen);
            // 
            // buttonXoaBanQuyen
            // 
            this.buttonXoaBanQuyen.Location = new System.Drawing.Point(352, 58);
            this.buttonXoaBanQuyen.Name = "buttonXoaBanQuyen";
            this.buttonXoaBanQuyen.Size = new System.Drawing.Size(75, 23);
            this.buttonXoaBanQuyen.TabIndex = 2;
            this.buttonXoaBanQuyen.Text = "Xóa mã";
            this.buttonXoaBanQuyen.UseVisualStyleBackColor = true;
            this.buttonXoaBanQuyen.Click += new System.EventHandler(this.xoacodeBanQuyen);
            // 
            // labelInfo
            // 
            this.labelInfo.AutoSize = true;
            this.labelInfo.ForeColor = System.Drawing.SystemColors.Highlight;
            this.labelInfo.Location = new System.Drawing.Point(13, 63);
            this.labelInfo.Name = "labelInfo";
            this.labelInfo.Size = new System.Drawing.Size(73, 13);
            this.labelInfo.TabIndex = 0;
            this.labelInfo.Text = "chưa đăng ký";
            // 
            // button1
            // 
            this.button1.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button1.Location = new System.Drawing.Point(435, 58);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "OK";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // formBanquyen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(520, 98);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.buttonXoaBanQuyen);
            this.Controls.Add(this.buttonNapKeyBanQuyen);
            this.Controls.Add(this.textBoxkeySanpham);
            this.Controls.Add(this.labelInfo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.macodeMaytinh);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "formBanquyen";
            this.Text = "Bản quyền";
            this.Load += new System.EventHandler(this.formBanquyen_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox macodeMaytinh;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxkeySanpham;
        private System.Windows.Forms.Button buttonNapKeyBanQuyen;
        private System.Windows.Forms.Button buttonXoaBanQuyen;
        private System.Windows.Forms.Label labelInfo;
        private System.Windows.Forms.Button button1;
    }
}