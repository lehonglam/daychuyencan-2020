﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DayChuyenHPEX
{
    static public class constan
    {
        static public string luuluong="lưu lượng";
        static public string apluc = "áp lực";
        static public string tocdo = "tốc độ";
        static public string nhietdo = "nhiệt độ";
        static public string thoigian = "thời gian";
        static public string trangthai = "trạng thái";
        static public string imgbinhthuong = "imgbinhthuong";
        static public string imgStart= "imgStart";
        static public string imgStop= "imgStop";
    }
    public enum trangthaihoatdong {hoatdong=1, dunghoatdong=0 }
    public class doituongcoban
    {
        public trangthaihoatdong trangthai = trangthaihoatdong.hoatdong;
        public string hinhanh= "";
        Dictionary<string, object> bangthongso = new Dictionary<string, object>();
        public doituongcoban()
        {
        }
        public doituongcoban(string imgPath)
        {
            hinhanh = imgPath;
        }
        public int setvalue(string key, object value)
        {
            bangthongso[key] = value;
            return 1;
        }
        public object getvalue(string key)
        {
            return bangthongso[key];
        }
    }
    public class dongco: doituongcoban
    {
        public dongco() : base()
        {
        }
        public dongco(string imgPath):base(imgPath)
        {
            hinhanh = imgPath;
        }
    }
}
