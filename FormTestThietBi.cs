﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using S7.Net;
namespace DayChuyenHPEX
{
    public partial class FormTestThietBi : Form
    {
        public FormTestThietBi()
        {
            InitializeComponent();
        }

        private void ketNoiPLC(object sender, EventArgs e)
        {
            if(S7.Net.ErrorCode.NoError== PlcFuntion.ketnoiPLC())
            {
                MessageBox.Show("Ket noi Tot");
            }
            else MessageBox.Show("Ket noi loi");

        }

        private void BatChay(object sender, EventArgs e)
        {

        }

        private void DocTrangThaiChay(object sender, EventArgs e)
        {
            int vitri =int.Parse(vitriBitQtextBox.Text);
            int dichBitQ= int.Parse(dichBitQtextBox5.Text);
            byte b = PlcFuntion.docDulieuTuBufQ(vitri, dichBitQ);
            giaTriQtextBox2.Text= b.ToString();
        }

        private void docTrangThaiBienM(object sender, EventArgs e)
        {
            int vitri = int.Parse(vitriMtextBox3.Text);
            int b = PlcFuntion.docDulieuTuBufM(vitri);
            giaTriMtextBox4.Text = b.ToString();
        }

        private void MouseDown(object sender, MouseEventArgs e)
        {



        }

        private void MouseUp(object sender, MouseEventArgs e)
        {
            //int vitri = int.Parse(VitriMStartStoptextBox1.Text);
            //int vitriBitStartStop = int.Parse(vitriBitStartStoptextBox2.Text);

            //Byte[] byt1 = hdexGlobalData.plc.ReadBytes(DataType.Memory, 1, vitri, 1);
            //byt1[0] &= 2;
            //hdexGlobalData.plc.WriteBytes(DataType.Memory, 1, vitri, byt1);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int vitriByte = int.Parse(VitriMStartStoptextBox1.Text);
            int vitriBit = int.Parse(vitriBitStartStoptextBox2.Text);

            Byte[] byt = hdexGlobalData.plc.ReadBytes(DataType.Memory, 1, vitriByte, 1);
            byt[0] |= (byte)(1 << (vitriBit + 2));
            hdexGlobalData.plc.WriteBytes(DataType.Memory, 1, vitriByte, byt);
        }
    }
}
