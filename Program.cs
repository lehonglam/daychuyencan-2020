﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DayChuyenHPEX
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            var res = DayChuyenHPEX.hdexGlobalData.kiemtraTepBanQuyen();
            if (res == false)
            {
                formBanquyen f = new formBanquyen();
                if (DialogResult.OK == f.ShowDialog())
                {
                    if (f.banquyenOK == true)
                    {
                        Application.Run(new FormChinh());
                    }
                    else
                    {
                        errorPro.errorProClass.errorMessage("Lỗi bản quyền");
                        return;
                    }
                }
            }
            else
            {
                Application.Run(new FormChinh());
            }
        }
    }
}
