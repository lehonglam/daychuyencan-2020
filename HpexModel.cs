﻿using System;
using DevExpress.Xpo;

namespace DayChuyenHPEX.Model
{

    public class HpexModel : XPObject
    {
        public HpexModel() : base()
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here.
        }

        public HpexModel(Session session) : base(session)
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here.
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
        }
    }

}