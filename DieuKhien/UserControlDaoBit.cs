﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace DayChuyenHPEX.DieuKhien
{
    public partial class UserControlDaoBit : basePLCControl
    {
        int _vitriPLCThuocTinhXacNhanPLC = -1, _dichbitThuocTinhXacNhanPLC = -1;
        Color _mauNut = Color.DeepSkyBlue;
        Image _img = null;
        [Category("AppearanceTDIN")]
        public Image m_img
        {
            get { return _img; }
            set
            {
                _img = value;
                btnDaoBit.BackgroundImage = _img;
            }
            
        }
        [Category("AppearanceTDIN")]
        public Color mauNen
        {
            get { return _mauNut; }
            set
            {
                _mauNut = value;
                btnDaoBit.BackColor=value;
            }
        }
        [Category("AppearanceTDIN")]
        public int vitriPLCThuocTinhXacNhanPLC
        {
            get { return _vitriPLCThuocTinhXacNhanPLC; }
            set { _vitriPLCThuocTinhXacNhanPLC = value; }
        }
        [Category("AppearanceTDIN")]
        public int dichbitThuocTinhXacNhanPLC
        {
            get { return _dichbitThuocTinhXacNhanPLC; }
            set { _dichbitThuocTinhXacNhanPLC = value; }
        }
        #region overide funtion
        protected override void refresh()
        {
            btnDaoBit.Text = tenthuocTinh;
        }
        private void datThuocTinh(object sender, EventArgs e)
        {
            if (plc != null)
            {
                plc.daoBitTaiVitriThuocPLC(vitriPLCThuocTinhXacNhanPLC, dichbitThuocTinhXacNhanPLC);
            }
            else
            {
                MessageBox.Show("Bạn chưa kết nối PLC");
            }
        }
        public override void readDataFromFile(XmlDocument xmlE)
        {
            try
            {
                var xnList = xmlE.SelectNodes("thong-so-thiet-bi/" + Name);
                foreach (XmlNode xn in xnList)
                {
                    var atList = xn.Attributes;
                    int color=Convert.ToInt32(atList["mauNen"].InnerText);
                    mauNen = Color.FromArgb(color);
                    vitriPLCThuocTinhXacNhanPLC = Convert.ToInt32(atList["vitriPLCThuocTinhXacNhanPLC"].InnerText);
                    dichbitThuocTinhXacNhanPLC = Convert.ToInt32(atList["dichbitThuocTinhXacNhanPLC"].InnerText);
                    readDataXMLBase(atList);
                }
            }
            catch
            {
                return;
            }
        }
        public override void writeDataFromFile(XmlTextWriter xmlWriter)
        {
            xmlWriter.WriteStartElement(Name);
            xmlWriter.WriteAttributeString("mauNen", mauNen.ToArgb().ToString());
            xmlWriter.WriteAttributeString("vitriPLCThuocTinhXacNhanPLC", dichbitThuocTinhXacNhanPLC.ToString());
            xmlWriter.WriteAttributeString("dichbitThuocTinhXacNhanPLC", vitriPLCThuocTinhXacNhanPLC.ToString());
            writeDataXMLBase(xmlWriter);
            xmlWriter.WriteEndElement();
        }
        private void mounDownClick(object sender, MouseEventArgs e)
        {
            if(e.Button==MouseButtons.Left)
            {
                datThuocTinh( sender,  e);
            }
            else if (e.Button == MouseButtons.Right)
            {
                openDlgConfig();
            }
        }

        private void enableChange(object sender, EventArgs e)
        {
            if (Enabled == false)
            {
                //btnDaoBit.BackgroundImage = null;
                //btnDaoBit.ba.
            }

        }

        public UserControlDaoBit()
        {
            InitializeComponent();
        }
        #endregion
    }
}
