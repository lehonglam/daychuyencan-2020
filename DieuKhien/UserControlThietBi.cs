﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.IO;

namespace DayChuyenHPEX.DieuKhien
{
    public partial class UserControlThietBi : basePLCControl
    {
        #region DATA
        int run1 = 0;
        // vi tri du lieu giam sat trang thai, dang chay hoac dung
        int _vitriPLCGiamSat = 0, _dichbitGiamSat = 0;
        // vi tri du lieu PLC dung cho viec bat thiet bi
        int _vitriPLCStart = 0, _dichbitStart = 0;
        // vi tri du lieu PLC dung cho viec tat thiet bi
        int _vitriPLCStop = 0, _dichbitStop = 0;
        // Cho phep bat hop thoai RUN
        bool _batHopThoaiRun = false;
        bool _choPhepMoPhong = false;

        // interface
        public RotateFlipType _gocquay = RotateFlipType.RotateNoneFlipNone;
        public trangthaihoatdong trangthai = trangthaihoatdong.dunghoatdong;
        Dictionary<string, object> bangthongso = new Dictionary<string, object>();
        Image _imgNomal = null, _imgRun1 = null, _imgRun2 = null, _imgStop = null;
        string _sImgNomal, _sImgRun1,_sImgRun2,_sImgStop;
        string _maThietbi = "",  _desc = "";
        LoaiThietBi _loaiThietBi = LoaiThietBi.MAY;
        bool _hienThitenThuocTinh = false;
        #endregion
        #region DATA-Catergory

        public UserControlThietBi()
        {
            InitializeComponent();
        }

        [Category("AppearanceTDIN")]
        public bool hienThitenThuocTinh
        {
            get { return _hienThitenThuocTinh; }
            set
            {
                _hienThitenThuocTinh = value;
                refresh();
            }
        }

        [Category("AppearanceTDIN")]
        public LoaiThietBi loaiThietBi
        {
            get { return _loaiThietBi; }
            set
            {
                _loaiThietBi = value;
            }
        }
        [Category("AppearanceTDIN")]
        public RotateFlipType m_gocquay
        {
            get { return _gocquay; }
            set
            {
                _gocquay = value;
                pictureBoxThietBi.Image = rotateImageGocquay(_imgNomal);
            }
        }
        [Category("AppearanceTDIN")]
        public string m_maThietBi
        {
            get { return _maThietbi; }
            set
            {
                _maThietbi = value;
            }
        }
        [Category("AppearanceTDIN")]
        public bool m_batHopThoaiRun { get { return _batHopThoaiRun; } set { _batHopThoaiRun = value; } }
        [Category("AppearanceTDIN")]
        public bool m_choPhepMoPhong { get { return _choPhepMoPhong; } set { _choPhepMoPhong = value; } }
        [Category("AppearanceTDIN")]
        public int m_vitriPLCGiamSat { get { return _vitriPLCGiamSat; } set { _vitriPLCGiamSat = value; } }
        [Category("AppearanceTDIN")]
        public int m_dichbitGiamSat { get { return _dichbitGiamSat; } set { _dichbitGiamSat = value; } }
        [Category("AppearanceTDIN")]
        public int m_vitriPLCStart { get { return _vitriPLCStart; } set { _vitriPLCStart = value; } }
        [Category("AppearanceTDIN")]
        public int m_dichbitStart { get { return _dichbitStart; } set { _dichbitStart = value; } }
        [Category("AppearanceTDIN")]
        public int m_vitriPLCStop { get { return _vitriPLCStop; } set { _vitriPLCStop = value; } }
        [Category("AppearanceTDIN")]
        public int m_dichbitStop { get { return _dichbitStop; } set { _dichbitStop = value; } }
        [Category("AppearanceTDIN")]
        public string m_Description { get { return _desc; } set { _desc = value; } }
        [Category("AppearanceTDIN")]
        public Image m_imgNomal
        {
            get { return _imgNomal; }
            set
            {
                _imgNomal = value;
                pictureBoxThietBi.Image = _imgNomal;
            }
        }
        [Category("AppearanceTDIN")]
        public Image m_imgRun1
        {
            get { return _imgRun1; }
            set { _imgRun1 = value; }
        }
        [Category("AppearanceTDIN")]
        public Image m_imgRun2
        {
            get { return _imgRun2; }
            set { _imgRun2 = value; }
        }
        [Category("AppearanceTDIN")]
        public Image m_imgStop
        {
            get {return _imgStop;}
            set {_imgStop = value;}
        }
        [Category("AppearanceTDIN")]
        public string m_sImgNomal { get { return _sImgNomal; } 
            set { 
                _sImgNomal = value;
                if (File.Exists(_sImgNomal))
                {
                    m_imgNomal = Image.FromFile(_sImgNomal);
                }
            } 
        }
        [Category("AppearanceTDIN")]
        public string m_sImgRun1
        {
            get { return _sImgRun1; }
            set
            {
                _sImgRun1 = value;
                if (File.Exists(_sImgRun1))
                {
                    m_imgRun1= Image.FromFile(_sImgRun1);
                }
            }
        }
        [Category("AppearanceTDIN")]
        public string m_sImgRun2
        {
            get { return _sImgRun2; }
            set
            {
                _sImgRun2 = value;
                if (File.Exists(_sImgRun2))
                {
                    m_imgRun2= Image.FromFile(_sImgRun2);
                }
            }
        }
        [Category("AppearanceTDIN")]
        public string m_sImgStop
        {
            get { return _sImgStop; }
            set
            {
                _sImgStop = value;
                if (File.Exists(_sImgStop))
                {
                    m_imgStop= Image.FromFile(_sImgStop);
                }
            }
        }

        #endregion
        #region FUNTION
        private void loadUser(object sender, EventArgs e)
        {
            pictureBoxThietBi.Image = rotateImageGocquay(_imgNomal);
        }
        public void setTrangThai(trangthaihoatdong tt)
        {
            trangthai = tt;
            if (tt == trangthaihoatdong.hoatdong)
            {
                timer1.Start();
            }
            else
            {
                timer1.Stop();
            }
            refresh();
        }

        private void MousePictureClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if (m_batHopThoaiRun)
                {
                    var f = new FormHopThoaiRun(_tenThuocTinh, plc,_loaiThietBi);
                    f.setThongtinTrangthaiHoatDong(trangthai, m_vitriPLCGiamSat, m_dichbitGiamSat, m_vitriPLCStart, m_dichbitStart, m_vitriPLCStop, m_dichbitStop);
                    f.Location = this.PointToScreen(e.Location);
                    f.StartPosition = FormStartPosition.Manual;
                    f.ShowDialog();
                }
            }
            else if (e.Button == MouseButtons.Right)
            {
                openDlgConfig();
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
        }
        private void paint(object sender, PaintEventArgs e)
        {
            if (this.Focused)
            {
                pictureBoxThietBi.BorderStyle = BorderStyle.FixedSingle;
            }
            else
            {
                pictureBoxThietBi.BorderStyle = BorderStyle.None;
            }
        }
        private void mouseMove(object sender, MouseEventArgs e)
        {
        }

        private void mouHover(object sender, EventArgs e)
        {
            BorderStyle = BorderStyle.FixedSingle;
        }


        private void mouseMove(object sender, EventArgs e)
        {
            BorderStyle = BorderStyle.FixedSingle;
            //BackColor = Color.Turquoise;
        }
        private void mouseLeave(object sender, EventArgs e)
        {
            BorderStyle = BorderStyle.None;
            //BackColor = Color.Transparent;
        }
        Image rotateImageGocquay(Image imgSrc)
        {
            if (imgSrc != null)
            {
                Image img = (Image)imgSrc.Clone();
                img.RotateFlip(m_gocquay);
                return img;
            }
            return null;
        }
        #endregion
        #region MOPHONG
        private void run(object sender, EventArgs e)
        {
            if (_choPhepMoPhong == true)
            {
                if (run1 == 1)
                {
                    if (_imgRun1 != null)
                    {
                        Image img = (Image)_imgRun1.Clone();
                        img.RotateFlip(m_gocquay);
                        pictureBoxThietBi.Image = img;
                        run1 = 0;
                    }
                }
                else
                {
                    if (_imgRun2 != null)
                    {
                        Image img = (Image)_imgRun2.Clone();
                        img.RotateFlip(m_gocquay);
                        pictureBoxThietBi.Image = img;
                        run1 = 1;
                    }
                }
            }
        }

        #endregion
        #region OVERRIDE BASE
        override protected void refresh()
        {
            if (m_choPhepMoPhong == true)
            {
                if (trangthai == trangthaihoatdong.hoatdong)
                {

                    if (_imgRun1 != null)
                    {
                        pictureBoxThietBi.Image = rotateImageGocquay(_imgRun1);
                    }

                }
                else if (trangthai == trangthaihoatdong.dunghoatdong)
                {
                    pictureBoxThietBi.Image = rotateImageGocquay(_imgStop);

                }
                else
                {
                    pictureBoxThietBi.Image = rotateImageGocquay(_imgNomal);
                }
            }

        }
        public override void capNhatGiatri()
        {
            if (plc != null)
            {
                bool trangthai = plc.docDulieuTuBufQ(m_vitriPLCGiamSat, m_dichbitGiamSat) == 1;
                if (trangthai)
                    setTrangThai(DieuKhien.trangthaihoatdong.hoatdong);
                else
                    setTrangThai(DieuKhien.trangthaihoatdong.dunghoatdong);
            }
        }
        #endregion
        #region XML-DATA
        public override void readDataFromFile(XmlDocument xmlE)
        {
            try
            {
                var xnList = xmlE.SelectNodes("thong-so-thiet-bi/" + Name);
                foreach (XmlNode xn in xnList)
                {
                    var atList = xn.Attributes;
                    m_maThietBi= atList["m_maThietBi"].InnerText;
                    m_vitriPLCGiamSat = Convert.ToInt32(atList["m_vitriPLCGiamSat"].InnerText);
                    m_dichbitGiamSat = Convert.ToInt32(atList["m_dichbitGiamSat"].InnerText);
                    m_vitriPLCStart = Convert.ToInt32(atList["m_vitriPLCStart"].InnerText);
                    m_dichbitStart = Convert.ToInt32(atList["m_dichbitStart"].InnerText);
                    m_vitriPLCStop = Convert.ToInt32(atList["m_vitriPLCStop"].InnerText);
                    m_dichbitStop = Convert.ToInt32(atList["m_dichbitStop"].InnerText);
                    m_choPhepMoPhong = Convert.ToBoolean(atList["m_choPhepMoPhong"].InnerText);
                    m_batHopThoaiRun = Convert.ToBoolean(atList["m_batHopThoaiRun"].InnerText);
                    hienThitenThuocTinh = Convert.ToBoolean(atList["hienThitenThuocTinh"].InnerText);

                    if (atList["m_sImgNomal"] != null)
                    {
                        m_sImgNomal =atList["m_sImgNomal"].InnerText;
                    }
                    if (atList["m_sImgRun1"] != null)
                    {
                        m_sImgRun1 = atList["m_sImgRun1"].InnerText;
                    }
                    if (atList["m_sImgRun2"] != null)
                    {
                        m_sImgRun2 = atList["m_sImgRun2"].InnerText;
                    }
                    if (atList["m_sImgStop"] != null)
                    {
                        m_sImgStop = atList["m_sImgStop"].InnerText;
                    }
                    readDataXMLBase(atList);
                }
            }
            catch
            {
                return;
            }
        }
        public override void writeDataFromFile(XmlTextWriter xmlWriter)
        {
            xmlWriter.WriteStartElement(Name);
            xmlWriter.WriteAttributeString("m_maThietBi", m_maThietBi);
            xmlWriter.WriteAttributeString("m_vitriPLCGiamSat", m_vitriPLCGiamSat.ToString());
            xmlWriter.WriteAttributeString("m_dichbitGiamSat", m_dichbitGiamSat.ToString());
            xmlWriter.WriteAttributeString("m_vitriPLCStart", m_vitriPLCStart.ToString());
            xmlWriter.WriteAttributeString("m_dichbitStart", m_dichbitStart.ToString());
            xmlWriter.WriteAttributeString("m_vitriPLCStop", m_vitriPLCStop.ToString());
            xmlWriter.WriteAttributeString("m_dichbitStop", m_dichbitStop.ToString());
            xmlWriter.WriteAttributeString("m_choPhepMoPhong", m_choPhepMoPhong.ToString());
            xmlWriter.WriteAttributeString("m_batHopThoaiRun", m_batHopThoaiRun.ToString());
            xmlWriter.WriteAttributeString("hienThitenThuocTinh", hienThitenThuocTinh.ToString());
            if(m_sImgNomal!="")
                xmlWriter.WriteAttributeString("m_sImgNomal", m_sImgNomal);
            if (m_sImgRun1 != "")
                xmlWriter.WriteAttributeString("m_sImgRun1", m_sImgRun1);
            if (m_sImgRun2 != "")
                xmlWriter.WriteAttributeString("m_sImgRun2", m_sImgRun2);
            if (m_sImgStop != "")
                xmlWriter.WriteAttributeString("m_sImgStop", m_sImgStop);
            writeDataXMLBase(xmlWriter);


            xmlWriter.WriteEndElement();
        }
        #endregion
    }
}
