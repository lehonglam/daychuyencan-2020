﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Media;

namespace DayChuyenHPEX.DieuKhien
{
    public class PLCMaLoi
    {
        public  int m_vitriPLC = 0;
        public int m_dichBitPLC = 0;
        public string m_sLoi = "loi";
    }
    public partial class UserControlBaoLoi : basePLCControl
    {
        #region DATA
        int _vitriPLC = -1, _dichbitPLC = -1;
        bool _loi = false, _beep=false;
        string _sTextLoi = "";
        List<PLCMaLoi> listMaLoi = new List<PLCMaLoi>();
        string _strPLCVaMLoi = "";// (120,3,Loi he thong);Vi tri PLC=120: 3: Dich bit PLC, Loi he thong: Ten Loi
        [Category("AppearanceTDIN")]
        public string strPLCVaMLoi
        {
            get { return _strPLCVaMLoi; }
            set { _strPLCVaMLoi = value;
                tachChuoiPLCVaTenLoi(_strPLCVaMLoi);
            }
        }
        [Category("AppearanceTDIN")]
        public bool beep
        {
            get { return _beep; }
            set { _beep = value; refresh(); }
        }
        [Category("AppearanceTDIN")]
        public int vitriPLC
        {
            get { return _vitriPLC; }
            set { _vitriPLC = value; }
        }
        [Category("AppearanceTDIN")]
        public bool loi
        {
            get { return _loi; }
            set { _loi = value; refresh(); }
        }
        [Category("AppearanceTDIN")]
        public int dichbitPLC
        {
            get { return _dichbitPLC; }
            set { _dichbitPLC = value; }
        }
        [Category("AppearanceTDIN")]
        public string m_sTextLoi
        {
            get { return _sTextLoi; }
            set 
            { 
                _sTextLoi = value;
                btnErrorLabel.Text = _sTextLoi;
            }
        }
        #endregion
        #region Override function
        [Browsable(true)]
        [Category("AppearanceTDIN-action")]
        [Description("Xuat hien loi")]
        public event EventHandler XuatHienLoi;
        protected void XuatHienLoi_Appear(object sender, EventArgs e)
        {
            if(XuatHienLoi!=null)
            {
                XuatHienLoi(this,e);
            }
        }
        protected override void refresh()
        {
            if (loi == true)
            {
                btnErrorLabel.Text = $"error-{_sTextLoi}";
            }
            else
            {
                btnErrorLabel.Text = $"OK-{_sTextLoi}";
            }
        }
        public override void readDataFromFile(XmlDocument xmlE)
        {
            try
            {
                var xnList = xmlE.SelectNodes("thong-so-thiet-bi/" + Name);
                foreach (XmlNode xn in xnList)
                {
                    var atList = xn.Attributes;
                    vitriPLC = Convert.ToInt32(atList["vitriPLC"].InnerText);
                    dichbitPLC = Convert.ToInt32(atList["dichbitPLC"].InnerText);
                    m_sTextLoi = atList["m_sTextLoi"].InnerText;
                    beep = Convert.ToBoolean(atList["beep"].InnerText);
                    strPLCVaMLoi = atList["strPLCVaMLoi"].InnerText;
                    readDataXMLBase(atList);

                }
            }
            catch
            {
                return;
            }
        }
        public override void writeDataFromFile(XmlTextWriter xmlWriter)
        {
            xmlWriter.WriteStartElement(Name);
            xmlWriter.WriteAttributeString("vitriPLC", vitriPLC.ToString());
            xmlWriter.WriteAttributeString("dichbitPLC", dichbitPLC.ToString());
            xmlWriter.WriteAttributeString("m_sTextLoi", m_sTextLoi);
            xmlWriter.WriteAttributeString("beep", beep.ToString());
            xmlWriter.WriteAttributeString("strPLCVaMLoi", strPLCVaMLoi.ToString());
            writeDataXMLBase(xmlWriter);
            xmlWriter.WriteEndElement();
        }
        private void clickLabel(object sender, EventArgs e)
        {
            
        }
        bool docLoiTaiViTriPLC(PLCMaLoi p)
        {
            double g = plc.docDulieuTuBufQ(p.m_vitriPLC,p.m_dichBitPLC);
            // hien thi nguoc lai
            loi = (g > 0.2);
            if (loi)
            {
                Visible = true;
                // goi event xuathienloi
                if (XuatHienLoi != null)
                {
                    XuatHienLoi(this, null);
                    Visible = true;
                    _sTextLoi = p.m_sLoi;
                }
                if (beep)
                {
                    timerBeep.Start();
                }
                return true;// co loi
            }
            else
            {
            }

            return false;
        }
        public override void capNhatGiatri()
        {
            if (quyenQuantri == true)
            {
                Visible = true;
                return;
            }
            else
            {
                //
                if (plc.erPLC == S7.Net.ErrorCode.NoError)
                {
                    foreach (var p in listMaLoi)
                        docLoiTaiViTriPLC(p);
                    //double g = plc.docDulieuTuBufQ(vitriPLC, dichbitPLC);
                    //// hien thi nguoc lai
                    //loi = (g > 0.2);
                    //if(loi)
                    //{
                    //    Visible = true;
                    //    // goi event xuathienloi
                    //    if (XuatHienLoi != null)
                    //    {
                    //        XuatHienLoi(this, null);
                    //        Visible = true;
                    //    }
                    //    if(beep)
                    //    {
                    //        timerBeep.Start();
                    //    }
                    //}
                    //else
                    //{
                    //}

                }
                else
                {
                    loi = false;
                }
            }
            refresh();
        }

        private void timerBeepStick(object sender, EventArgs e)
        {
            if(loi==true) 
                SystemSounds.Beep.Play();
        }

        private void mouseDownBtnError(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                openDlgConfig();
            }
        }

        #endregion
        #region contructor - load
        public UserControlBaoLoi()
        {
            InitializeComponent();
        }
        private void UserControlThuocTinhGhiXuongPLC_Load(object sender, EventArgs e)
        {
            //Visible = quyenQuantri||loi;
        }
        #endregion
        void tachChuoiPLCVaTenLoi(string sText)
        {
            listMaLoi.Clear();
            var ar1 = sText.Split(';');
            foreach(var s1 in ar1)
            {
                var ar2=s1.Split(',');
                if(ar2.Length==3)
                {
                    var p = new PLCMaLoi();
                    p.m_vitriPLC = Convert.ToInt32(ar2[0]);
                    p.m_dichBitPLC= Convert.ToInt32(ar2[1]);
                    p.m_sLoi= ar2[2];
                    listMaLoi.Add(p);
                }
            }
        }

    }
}
