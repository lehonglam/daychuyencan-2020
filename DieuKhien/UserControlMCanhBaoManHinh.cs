﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DayChuyenHPEX.DieuKhien
{
    public partial class UserControlMCanhBaoManHinh : basePLCControl
    {
        int mau = 0;
        string _noidungCanhBao = "";
        int _vitriPLC = 1;
        double _scale = 1;
        double _giatri = 0;
        double _gioihanCanhBao = 10;
        public UserControlMCanhBaoManHinh()
        {
            InitializeComponent();
        }
        [Category("AppearanceTDIN")]
        public string noidungCanhBao
        {
            get { return _noidungCanhBao; }
            set
            {
                _noidungCanhBao = value;
            }
        }
        [Category("AppearanceTDIN")]
        public int vitriPLC
        {
            get { return _vitriPLC; }
            set { _vitriPLC = value; }
        }
        [Category("AppearanceTDIN")]
        public double scale
        {
            get { return _scale; }
            set { _scale = value; }
        }
        [Category("AppearanceTDIN")]
        public double gioihanCanhBao
        {
            get { return _gioihanCanhBao; }
            set { _gioihanCanhBao = value; }
        }
        public void canhBao()
        {
            labelCanhBao.Visible = true;
            labelCanhBao.Text = _noidungCanhBao;
            timer1.Start();
        }
        public void tatCanhBao()
        {
            labelCanhBao.Visible = false;
            labelCanhBao.Text = "";
            timer1.Stop();
        }
        private void timerTick(object sender, EventArgs e)
        {
            if (mau == 0)
            {
                labelCanhBao.ForeColor = Color.Blue;
                mau = 1;
            }
            else
            {
                labelCanhBao.ForeColor = Color.Red;
                mau = 0;
            }

        }
        public override void capNhatGiatri()
        {
            if (plc.erPLC == S7.Net.ErrorCode.NoError)
            {
                int g = plc.docDulieuTuBufM(vitriPLC);
                // hien thi nguoc lai

                double x = g;
                double s = scale;
                double v = s * x;
                _giatri = v;
                if (_giatri >= gioihanCanhBao)
                {
                    canhBao();
                }
                else tatCanhBao();
            }
        }

        private void labelCanhBao_Click(object sender, EventArgs e)
        {

        }

        private void UserControlMCanhBaoManHinh_Load(object sender, EventArgs e)
        {
            labelCanhBao.Visible = false;
        }
    }
}
