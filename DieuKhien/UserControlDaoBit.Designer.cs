﻿namespace DayChuyenHPEX.DieuKhien
{
    partial class UserControlDaoBit
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDaoBit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnDaoBit
            // 
            this.btnDaoBit.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDaoBit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDaoBit.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDaoBit.Location = new System.Drawing.Point(0, 0);
            this.btnDaoBit.Name = "btnDaoBit";
            this.btnDaoBit.Size = new System.Drawing.Size(161, 44);
            this.btnDaoBit.TabIndex = 0;
            this.btnDaoBit.Text = "Set";
            this.btnDaoBit.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.btnDaoBit.UseVisualStyleBackColor = true;
            this.btnDaoBit.Click += new System.EventHandler(this.datThuocTinh);
            this.btnDaoBit.MouseDown += new System.Windows.Forms.MouseEventHandler(this.mounDownClick);
            // 
            // UserControlDaoBit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnDaoBit);
            this.Name = "UserControlDaoBit";
            this.Size = new System.Drawing.Size(161, 47);
            this.EnabledChanged += new System.EventHandler(this.enableChange);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnDaoBit;
    }
}
