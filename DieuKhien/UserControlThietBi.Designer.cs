﻿namespace DayChuyenHPEX.DieuKhien
{
    partial class UserControlThietBi
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pictureBoxThietBi = new System.Windows.Forms.PictureBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxThietBi)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBoxThietBi
            // 
            this.pictureBoxThietBi.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxThietBi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxThietBi.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxThietBi.Name = "pictureBoxThietBi";
            this.pictureBoxThietBi.Size = new System.Drawing.Size(215, 206);
            this.pictureBoxThietBi.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxThietBi.TabIndex = 1;
            this.pictureBoxThietBi.TabStop = false;
            this.pictureBoxThietBi.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MousePictureClick);
            this.pictureBoxThietBi.MouseLeave += new System.EventHandler(this.mouseLeave);
            this.pictureBoxThietBi.MouseHover += new System.EventHandler(this.mouHover);
            this.pictureBoxThietBi.MouseMove += new System.Windows.Forms.MouseEventHandler(this.mouseMove);
            // 
            // timer1
            // 
            this.timer1.Interval = 200;
            this.timer1.Tick += new System.EventHandler(this.run);
            // 
            // UserControlThietBi
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.Transparent;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Controls.Add(this.pictureBoxThietBi);
            this.Name = "UserControlThietBi";
            this.Size = new System.Drawing.Size(215, 206);
            this.Load += new System.EventHandler(this.loadUser);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.paint);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxThietBi)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxThietBi;
        private System.Windows.Forms.Timer timer1;
    }
}
