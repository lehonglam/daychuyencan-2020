﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace DayChuyenHPEX.DieuKhien
{
    public partial class UserControlThuocTinhGhiXuongPLC : basePLCControl
    {
        string _donviDo = "độ C";
        int _vitriPLC=-1;
        int _vitriPLCThuocTinhXacNhanPLC = -1, _dichbitThuocTinhXacNhanPLC=-1;
        double _scale = 10;
        double _gioiHanTren = 10000;
        double _gioiHanDuoi = 0;
        bool dangGhi = false;
        string _formatText = "  0.00";
        [Category("AppearanceTDIN")]
        public string formatText
        {
            get { return _formatText; }
            set
            {
                _formatText = value;
                refresh();
            }
        }

        [Category("AppearanceTDIN")]
        public double  gioiHanTren
        {
            get { return _gioiHanTren; }
            set { _gioiHanTren = value; }
        }
        [Category("AppearanceTDIN")]
        public double gioiHanDuoi
        {
            get { return _gioiHanDuoi; }
            set { _gioiHanDuoi = value; }
        }
        [Category("AppearanceTDIN")]
        public string donviDo
        {
            get { return _donviDo; }
            set { 
                _donviDo = value;
                refresh();
            }
        }
        [Category("AppearanceTDIN")]
        public double scale
        {
            get { return _scale; }
            set { _scale = value; }
        }
        [Category("AppearanceTDIN")]
        public int vitriPLCThuocTinhXacNhanPLC
        {
            get { return _vitriPLCThuocTinhXacNhanPLC; }
            set { _vitriPLCThuocTinhXacNhanPLC = value; }
        }
        [Category("AppearanceTDIN")]
        public int dichbitThuocTinhXacNhanPLC
        {
            get { return _dichbitThuocTinhXacNhanPLC; }
            set { _dichbitThuocTinhXacNhanPLC = value; }
        }
        [Category("AppearanceTDIN")]
        public int vitriPLC
        {
            get { return _vitriPLC; }
            set { _vitriPLC = value; }
        }
        protected override void refresh()
        {
            toolStripLabel1.Text = _tenThuocTinh +"("+ _donviDo+")";
        }
        public override void readDataFromFile(XmlDocument xmlE)
        {
            try
            {
                var xnList = xmlE.SelectNodes("thong-so-thiet-bi/" + Name);
                foreach (XmlNode xn in xnList)
                {
                    var atList = xn.Attributes;
                    scale = Convert.ToDouble(atList["scale"].InnerText);
                    vitriPLC = Convert.ToInt32(atList["vitriPLC"].InnerText);
                    vitriPLCThuocTinhXacNhanPLC = Convert.ToInt32(atList["vitriPLCThuocTinhXacNhanPLC"].InnerText);
                    dichbitThuocTinhXacNhanPLC = Convert.ToInt32(atList["dichbitThuocTinhXacNhanPLC"].InnerText);
                    gioiHanTren = Convert.ToInt32(atList["gioiHanTren"].InnerText);
                    gioiHanDuoi = Convert.ToInt32(atList["gioiHanDuoi"].InnerText);
                    formatText = atList["formatText"].InnerText;
                    donviDo = atList["donviDo"].InnerText;
                    readDataXMLBase(atList);
                }
            }
            catch
            {
                return;
            }
        }
        public override void writeDataFromFile(XmlTextWriter xmlWriter)
        {
            xmlWriter.WriteStartElement(Name);
            xmlWriter.WriteAttributeString("scale", scale.ToString());
            xmlWriter.WriteAttributeString("vitriPLC", vitriPLC.ToString());
            xmlWriter.WriteAttributeString("vitriPLCThuocTinhXacNhanPLC", dichbitThuocTinhXacNhanPLC.ToString());
            xmlWriter.WriteAttributeString("dichbitThuocTinhXacNhanPLC", vitriPLCThuocTinhXacNhanPLC.ToString());
            xmlWriter.WriteAttributeString("gioiHanTren", gioiHanTren.ToString());
            xmlWriter.WriteAttributeString("gioiHanDuoi", gioiHanDuoi.ToString());
            xmlWriter.WriteAttributeString("formatText", formatText.ToString());
            xmlWriter.WriteAttributeString("donviDo", donviDo.ToString());
            writeDataXMLBase(xmlWriter);
            xmlWriter.WriteEndElement();
        }
        public UserControlThuocTinhGhiXuongPLC()
        {
            InitializeComponent();
        }

        private void UserControlThuocTinhGhiXuongPLC_Load(object sender, EventArgs e)
        {
            
        }

        private void MouseDown(object sender, MouseEventArgs e)
        {
            if (plc == null)
            {
                MessageBox.Show("PLC =null", "Loi PLC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            //1. ghi du lieu xuong vung nho
            double giatri = LAM_Base.convertText.ToDouble(GiaTritextBox1.Text);

            if((giatri<gioiHanDuoi)||(giatri>gioiHanTren))
            {
                MessageBox.Show(string.Format("Bạn ghi quá giới hạn {0} - {1}",gioiHanDuoi,gioiHanTren));
                return;
            }
            giatri = giatri * scale;
            if (plc.erPLC == S7.Net.ErrorCode.NoError)
            {
                try
                {
                    plc.ghiDulieuVaoPLC((int)giatri, vitriPLC);
                }
                catch(Exception ex)
                {
                    string s = string.Format("{0},{1},{2}", "Loi ghiDulieuVaoPLC PLC " + ex.Message, vitriPLC, "MouseDown");

                    MessageBox.Show(s + ex.Message, "Loi ghi xuong PLC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                dangGhi = true;
            }
        }

        private void MouseUp(object sender, MouseEventArgs e)
        {
            if(plc==null)
            {
                MessageBox.Show("PLC =null", "Loi PLC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (plc.erPLC == S7.Net.ErrorCode.NoError)
            {
                if (dangGhi == true)
                {
                    try
                    {
                        plc.daoBitTaiVitriThuocPLC(vitriPLCThuocTinhXacNhanPLC, dichbitThuocTinhXacNhanPLC);
                    }
                    catch (Exception ex)
                    {
                        string s = string.Format("{0},vitriPLCThuocTinhXacNhanPLC=:{1}/{2},", "Loi daoBitTaiVitriThuocPLC PLC " + ex.Message, vitriPLCThuocTinhXacNhanPLC, "MouseUp");
                        MessageBox.Show(s, "Loi dao bit xuong PLC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    dangGhi = false;
                }
            }
        }

        private void dbClick(object sender, EventArgs e)
        {

        }

        private void MouseDownClickSetup(object sender, MouseEventArgs e)
        {
            openDlgConfig();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {

        }

        public override void capNhatGiatri()
        {
            int g = plc.docDulieuTuBufM(vitriPLC);
            // hien thi nguoc lai
            double x = g;
            double s = scale;
            double v = x/s;
            toolStripLabelGiaTri.Text= v.ToString(formatText);
        }
    }
}
