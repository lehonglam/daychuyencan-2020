﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace DayChuyenHPEX.DieuKhien
{
    public partial class UserControlLabelHienThiNhanh : basePLCControl
    {
        string _donviDo= "độ C";
        double _giatri = 0;
        int _vitriPLC = -1;
        double _scale = 0.1;
        string _dinhdangthapphan = "0.0";
        bool _hienThitenThuocTinh = false;

        [Category("AppearanceTDIN")]
        public bool hienThitenThuocTinh
        {
            get { return _hienThitenThuocTinh; }
            set
            {
                _hienThitenThuocTinh = value;
                refresh();
            }
        }
        [Category("AppearanceTDIN")]
        public double giatri
        {
            get { return _giatri; }
            set
            {
                _giatri = value;
                refresh();
            }
        }
        [Category("AppearanceTDIN")]
        public string dinhdangthapphan
        {
            get { return _dinhdangthapphan; }
            set
            {
                _dinhdangthapphan = value;
                refresh();
            }
        }
        [Category("AppearanceTDIN")]
        public double scale
        {
            get { return _scale; }
            set { _scale = value; }
        }
        [Category("AppearanceTDIN")]
        public string donviDo
        {
            get { return _donviDo; }
            set { _donviDo = value; }
        }
        [Category("AppearanceTDIN")]
        public int vitriPLC
        {
            get { return _vitriPLC; }
            set { _vitriPLC = value; }
        }
        public override void readDataFromFile(XmlDocument xmlE)
        {
            try
            {
                var xnList = xmlE.SelectNodes("thong-so-thiet-bi/" + Name);
                foreach (XmlNode xn in xnList)
                {
                    var atList = xn.Attributes;
                    scale = Convert.ToDouble(atList["scale"].InnerText);
                    vitriPLC = Convert.ToInt32(atList["vitriPLC"].InnerText);
                    dinhdangthapphan = atList["dinhdangthapphan"].InnerText;
                    donviDo = atList["donviDo"].InnerText;
                    hienThitenThuocTinh = Convert.ToBoolean(atList["hienThitenThuocTinh"].InnerText);

                    readDataXMLBase(atList);
                }
            }
            catch
            {
                return;
            }
        }
        public override void writeDataFromFile(XmlTextWriter xmlWriter)
        {
            xmlWriter.WriteStartElement(Name);
            xmlWriter.WriteAttributeString("scale", scale.ToString());
            xmlWriter.WriteAttributeString("vitriPLC", vitriPLC.ToString());
            xmlWriter.WriteAttributeString("dinhdangthapphan", dinhdangthapphan);
            xmlWriter.WriteAttributeString("donviDo", donviDo);
            xmlWriter.WriteAttributeString("hienThitenThuocTinh", hienThitenThuocTinh.ToString());

            writeDataXMLBase(xmlWriter);

            xmlWriter.WriteEndElement();
        }

        public UserControlLabelHienThiNhanh()
        {
            InitializeComponent();
        }
        protected override void refresh()
        {
            if(hienThitenThuocTinh==true)
                richTextBox1.Text =$"{_tenThuocTinh} {giatri.ToString(dinhdangthapphan)} {donviDo}";
            else
                richTextBox1.Text = $"{giatri.ToString(dinhdangthapphan)} {donviDo}";
            //richTextBox1.Text = _tenThuocTinh + _giatri.ToString("0.00") + " " + _donviDo;
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void UserControlLabel_Load(object sender, EventArgs e)
        {
           
            refresh();
        }

        private void mouseHover(object sender, EventArgs e)
        {
           richTextBox1.ForeColor = Color.Turquoise;
        }

        private void mouseLeaver(object sender, EventArgs e)
        {
            richTextBox1.ForeColor = Color.Blue;
        }

        private void clickLabel(object sender, MouseEventArgs e)
        {
            openDlgConfig();
        }
        public override void capNhatGiatri()
        {
            if (plc.erPLC == S7.Net.ErrorCode.NoError)
            {
                int g = plc.docDulieuTuBufM(vitriPLC);
                // hien thi nguoc lai

                double x = g;
                double s = scale;
                double v = s * x;
                _giatri = v;

                refresh();
            }
        } 

    }
}
