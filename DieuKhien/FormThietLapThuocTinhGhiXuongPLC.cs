﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DayChuyenHPEX.DieuKhien
{
    public partial class FormThietLapThuocTinhGhiXuongPLC : Form
    {
        basePLCControl ur;
        public FormThietLapThuocTinhGhiXuongPLC(basePLCControl urIn)
        {
            ur = urIn;
            InitializeComponent();
        }

        private void FormThietLapThuocTinhGhiXuongPLC_Load(object sender, EventArgs e)
        {
            propertyGrid1.SelectedObject = ur;
            Text = "config:("+ur.Name+")-"+ ur.tenthuocTinh;
        }

        private void onOKClick(object sender, EventArgs e)
        {
        }
    }
}
