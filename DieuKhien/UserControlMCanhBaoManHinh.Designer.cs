﻿namespace DayChuyenHPEX.DieuKhien
{
    partial class UserControlMCanhBaoManHinh
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.labelCanhBao = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // labelCanhBao
            // 
            this.labelCanhBao.AutoSize = true;
            this.labelCanhBao.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCanhBao.ForeColor = System.Drawing.Color.OrangeRed;
            this.labelCanhBao.Location = new System.Drawing.Point(3, 2);
            this.labelCanhBao.Name = "labelCanhBao";
            this.labelCanhBao.Size = new System.Drawing.Size(298, 20);
            this.labelCanhBao.TabIndex = 0;
            this.labelCanhBao.Text = "Cảnh báo hệ thống quá áp hê thống";
            this.labelCanhBao.Click += new System.EventHandler(this.labelCanhBao_Click);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timerTick);
            // 
            // UserControlMCanhBaoManHinh
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.labelCanhBao);
            this.Name = "UserControlMCanhBaoManHinh";
            this.Size = new System.Drawing.Size(310, 26);
            this.Load += new System.EventHandler(this.UserControlMCanhBaoManHinh_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelCanhBao;
        private System.Windows.Forms.Timer timer1;
    }
}
