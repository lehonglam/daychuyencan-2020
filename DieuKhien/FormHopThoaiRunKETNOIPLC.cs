﻿using DayChuyenHPEX.DongThoi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DayChuyenHPEX.DieuKhien
{
    public partial class FormHopThoaiRunKETNOIPLC : Form
    {
        PLCS71200 plc = null;
        public LoaiThietBi loaiThietBi = LoaiThietBi.MAY;
        public FormHopThoaiRunKETNOIPLC(string thongtin, PLCS71200 p)
        {
            InitializeComponent();
            this.Text = thongtin;
            plc = p;
        }
        private void FormHopThoaiRun_Load(object sender, EventArgs e)
        {
            if (plc != null)
            {
                buttonStart.Enabled = true;
                buttonStop.Enabled = false;
            }
            else
            {
                buttonStart.Enabled = buttonStop.Enabled = false;
            }
        }
        void capNhatTrangThaiHoatDong()
        {
            if (plc == null)
            {
                buttonStart.Enabled = buttonStop.Enabled = false;
                return;
            }
            // hien thi nguoc            lai
            bool trangthai = (plc.erPLC == S7.Net.ErrorCode.NoError);
            if (trangthai == false)
            {
                buttonStart.Enabled = true;
                buttonStop.Enabled = false;
            }
            else if (trangthai == true)// dang hoat dong
            {
                buttonStop.Enabled = true;
                buttonStart.Enabled = false;
            }
        }
        private void Start(object sender, EventArgs e)
        {
            if (plc == null)
            {
                buttonStart.Enabled = buttonStop.Enabled = false;
                return;
            }
            Cursor = Cursors.WaitCursor;
            plc.erPLC=plc.ketnoiPLC();
            Cursor = Cursors.Default;
            if (plc.erPLC == S7.Net.ErrorCode.NoError)
            {
                capNhatTrangThaiHoatDong();
            }
            else
            {
                errorPro.errorProClass.errorMessage($"Lỗi kết nối PLC {plc.erPLC.ToString()}");
            }
        }
        private void Stop(object sender, EventArgs e)
        {
            if (plc == null)
            {
                buttonStart.Enabled = buttonStop.Enabled = false;
                return;
            }
            plc.Close();
            buttonStart.Enabled = true;
            buttonStop.Enabled = false;
        }
    }
}
