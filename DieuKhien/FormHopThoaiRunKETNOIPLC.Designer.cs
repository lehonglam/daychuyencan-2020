﻿namespace DayChuyenHPEX.DieuKhien
{
    partial class FormHopThoaiRunKETNOIPLC
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormHopThoaiRunKETNOIPLC));
            this.buttonStart = new DevExpress.XtraEditors.SimpleButton();
            this.buttonStop = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.SuspendLayout();
            // 
            // buttonStart
            // 
            resources.ApplyResources(this.buttonStart, "buttonStart");
            this.buttonStart.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("buttonStart.ImageOptions.Image")));
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Click += new System.EventHandler(this.Start);
            // 
            // buttonStop
            // 
            resources.ApplyResources(this.buttonStop, "buttonStop");
            this.buttonStop.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("buttonStop.ImageOptions.Image")));
            this.buttonStop.Name = "buttonStop";
            this.buttonStop.Click += new System.EventHandler(this.Stop);
            // 
            // simpleButton3
            // 
            this.simpleButton3.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.simpleButton3.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton3.ImageOptions.Image")));
            resources.ApplyResources(this.simpleButton3, "simpleButton3");
            this.simpleButton3.Name = "simpleButton3";
            // 
            // FormHopThoaiRunKETNOIPLC
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.simpleButton3);
            this.Controls.Add(this.buttonStop);
            this.Controls.Add(this.buttonStart);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormHopThoaiRunKETNOIPLC";
            this.Load += new System.EventHandler(this.FormHopThoaiRun_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton buttonStart;
        private DevExpress.XtraEditors.SimpleButton buttonStop;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
    }
}