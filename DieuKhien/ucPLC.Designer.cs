﻿namespace DayChuyenHPEX.DieuKhien
{
    partial class ucPLC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucPLC));
            this.imageListBtnPLC = new System.Windows.Forms.ImageList(this.components);
            this.btnPLC = new DevExpress.XtraEditors.SimpleButton();
            this.SuspendLayout();
            // 
            // imageListBtnPLC
            // 
            this.imageListBtnPLC.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListBtnPLC.ImageStream")));
            this.imageListBtnPLC.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListBtnPLC.Images.SetKeyName(0, "apply_32x32.png");
            this.imageListBtnPLC.Images.SetKeyName(1, "UpgradeReport_Warning.png");
            // 
            // btnPLC
            // 
            this.btnPLC.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnPLC.Appearance.Options.UseFont = true;
            this.btnPLC.ImageOptions.ImageIndex = 1;
            this.btnPLC.ImageOptions.ImageList = this.imageListBtnPLC;
            this.btnPLC.Location = new System.Drawing.Point(3, 3);
            this.btnPLC.Name = "btnPLC";
            this.btnPLC.Size = new System.Drawing.Size(104, 38);
            this.btnPLC.TabIndex = 1;
            this.btnPLC.Text = "192.168.1.5";
            this.btnPLC.ToolTip = "PLC";
            this.btnPLC.Click += new System.EventHandler(this.ClickBtnPLC);
            this.btnPLC.MouseClick += new System.Windows.Forms.MouseEventHandler(this.mouseClickPLC);
            this.btnPLC.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MouseDownBtnClick);
            // 
            // ucPLC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.btnPLC);
            this.Name = "ucPLC";
            this.Size = new System.Drawing.Size(135, 47);
            this.Load += new System.EventHandler(this.ucPLC_Load);
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraEditors.SimpleButton btnPLC;
        private System.Windows.Forms.ImageList imageListBtnPLC;
    }
}
