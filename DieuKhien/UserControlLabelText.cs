﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace DayChuyenHPEX.DieuKhien
{
    public partial class UserControlLabelText : basePLCControl
    {
        Color _mauText = Color.Black;
        [Category("AppearanceTDIN")]
        public Color mauText
        {
            get { return _mauText; }
            set
            {
                _mauText = value;
                richTextBox1.ForeColor= _mauText;
            }
        }

        public override void readDataFromFile(XmlDocument xmlE)
        {
            try
            {
                var xnList = xmlE.SelectNodes("thong-so-thiet-bi/" + Name);
                foreach (XmlNode xn in xnList)
                {
                    var atList = xn.Attributes;
                    int color = Convert.ToInt32(atList["mauText"].InnerText);
                    mauText = Color.FromArgb(color);
                    readDataXMLBase(atList);

                }
            }
            catch
            {
                return;
            }
        }
        public override void writeDataFromFile(XmlTextWriter xmlWriter)
        {
            xmlWriter.WriteStartElement(Name);
            xmlWriter.WriteAttributeString("mauText", mauText.ToArgb().ToString());
            writeDataXMLBase(xmlWriter);

            xmlWriter.WriteEndElement();
        }

        public UserControlLabelText()
        {
            InitializeComponent();
        }
        protected override void refresh()
        {
            richTextBox1.Text =$"{_tenThuocTinh}";
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void UserControlLabel_Load(object sender, EventArgs e)
        {
           
            refresh();
        }

        private void mouseHover(object sender, EventArgs e)
        {
        }

        private void mouseLeaver(object sender, EventArgs e)
        {
        }

        private void clickLabel(object sender, MouseEventArgs e)
        {
            openDlgConfig();
        }
        public override void capNhatGiatri()
        {
        } 

    }
}
