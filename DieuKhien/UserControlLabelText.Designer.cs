﻿namespace DayChuyenHPEX.DieuKhien
{
    partial class UserControlLabelText
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.richTextBox1 = new DayChuyenHPEX.DieuKhien.CustomLabel();
            this.SuspendLayout();
            // 
            // richTextBox1
            // 
            this.richTextBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.richTextBox1.Cursor = System.Windows.Forms.Cursors.Default;
            this.richTextBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox1.ForeColor = System.Drawing.Color.Blue;
            this.richTextBox1.Location = new System.Drawing.Point(0, 0);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(126, 20);
            this.richTextBox1.TabIndex = 0;
            this.richTextBox1.Text = "thuộc tính";
            this.richTextBox1.TextChanged += new System.EventHandler(this.richTextBox1_TextChanged);
            this.richTextBox1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.clickLabel);
            this.richTextBox1.MouseLeave += new System.EventHandler(this.mouseLeaver);
            this.richTextBox1.MouseHover += new System.EventHandler(this.mouseHover);
            // 
            // UserControlLabelHienThiNhanh
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Controls.Add(this.richTextBox1);
            this.Name = "UserControlLabelHienThiNhanh";
            this.Size = new System.Drawing.Size(126, 20);
            this.Load += new System.EventHandler(this.UserControlLabel_Load);
            this.MouseLeave += new System.EventHandler(this.mouseLeaver);
            this.MouseHover += new System.EventHandler(this.mouseHover);
            this.ResumeLayout(false);

        }

        #endregion

        private CustomLabel richTextBox1;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}
