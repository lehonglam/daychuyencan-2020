﻿using DayChuyenHPEX.DongThoi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DayChuyenHPEX.DieuKhien
{
    public partial class FormHopThoaiRun : Form
    {
        PLCS71200 plc = null;
        DieuKhien.trangthaihoatdong trangthai = DieuKhien.trangthaihoatdong.binhthuong;
        // vi tri du lieu giam sat trang thai, dang chay hoac dung
        int vitriPLCGiamSat = 0, dichbitGiamSat = 0;
        // vi tri du lieu PLC dung cho viec bat thiet bi
        int vitriPLCStart = 0, dichbitStart = 0;
        // vi tri du lieu PLC dung cho viec tat thiet bi
        int vitriPLCStop = 0, dichbitStop = 0;
        public LoaiThietBi loaiThietBi = LoaiThietBi.MAY;
        public FormHopThoaiRun(string thongtin, PLCS71200 p, LoaiThietBi loaiTb)
        {
            this.Text = thongtin;
            plc = p;
            loaiThietBi = loaiTb;
            InitializeComponent();
        }
        private void FormHopThoaiRun_Load(object sender, EventArgs e)
        {
            if (loaiThietBi == LoaiThietBi.MAY)
            {
                buttonStart.Text = "Chạy";
                buttonStop.Text = "Dừng";
            }
            else if (loaiThietBi == LoaiThietBi.VANDONGMO)
            {
                buttonStart.Text = "Mở";
                buttonStop.Text = "Đóng";
            }
            else if (loaiThietBi == LoaiThietBi.MAY)
            {
                buttonStart.Text = "Kết Nối";
                buttonStop.Text = "Ngắt";
            }
            timer1.Start();
        }
        public void setThongtinTrangthaiHoatDong(DieuKhien.trangthaihoatdong trangthaiIn,
                    int vitriPLCGiamSatIn, int dichbitGiamSatIn, int vitriPLCStartIn, int dichbitStartPlcIn, int vitriPLCStopIn, int dichbitStopPlcIn)
        {

            trangthai = trangthaiIn;
            vitriPLCGiamSat = vitriPLCGiamSatIn;
            dichbitGiamSat = dichbitGiamSatIn;
            vitriPLCStart = vitriPLCStartIn;
            dichbitStart = dichbitStartPlcIn;
            vitriPLCStop = vitriPLCStopIn;
            dichbitStop = dichbitStopPlcIn;

            capNhatTrangThaiHoatDong();
        }

        private void timerTick(object sender, EventArgs e)
        {
            capNhatTrangThaiHoatDong();
        }

        void capNhatTrangThaiHoatDong()
        {
            if (plc == null)
            {
                buttonStart.Enabled = buttonStop.Enabled = false;
                return;
            }
            // hien thi nguoc            lai
            byte trangthai = plc.docDulieuTuBufQ(vitriPLCGiamSat, dichbitGiamSat);
            if (trangthai == 0)
            {
                buttonStart.Enabled = true;
                buttonStop.Enabled = false;
            }
            else if (trangthai == 1)// dang hoat dong
            {
                buttonStop.Enabled = true;
                buttonStart.Enabled = false;
            }
            else
            {
                buttonStop.Enabled = false;
                buttonStart.Enabled = false;
            }
        }
        private void Start(object sender, EventArgs e)
        {
            if (plc == null)
            {
                buttonStart.Enabled = buttonStop.Enabled = false;
                return;
            }
            plc.chayThietBi(vitriPLCStart, dichbitStart);
            capNhatTrangThaiHoatDong();
        }
        private void Stop(object sender, EventArgs e)
        {
            if (plc == null)
            {
                buttonStart.Enabled = buttonStop.Enabled = false;
                return;
            }

            plc.dungThietBi(vitriPLCStop, dichbitStop);
             capNhatTrangThaiHoatDong();
        }
    }
}
