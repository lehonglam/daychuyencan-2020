﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using DayChuyenHPEX.DongThoi;

namespace DayChuyenHPEX.DieuKhien
{
    public partial class ucPLC : basePLCControl
    {
        #region DATA
        string _plcIp = "192.168.10.10";
        bool _batHopThoaiRun = false;
        bool _trangThaiKetNoi = false;
        int _nSizeM = 1000;//500;
        int _nSizeQ = 100;//50;
        [Category("AppearanceTDIN")]
        public int nSizeM
        {
            get { return _nSizeM; }
            set
            {
                _nSizeM = value;
                if(plc!=null)
                {
                    plc.nSizeM = _nSizeM;
                    plc.bufM= new byte[_nSizeM];
                }
            }
        }
        [Category("AppearanceTDIN")]
        public int nSizeQ
        {
            get { return _nSizeQ; }
            set
            {
                _nSizeQ = value;
                if (plc != null)
                {
                    plc.nSizeQ= _nSizeQ;
                    plc.bufQ = new byte[_nSizeQ];
                }
            }
        }
        [Category("AppearanceTDIN")]
        public bool trangThaiKetNoi { get { return _trangThaiKetNoi; } 
            set {
                _trangThaiKetNoi = value; 
                if(_trangThaiKetNoi==false)
                {
                    btnPLC.ImageOptions.ImageIndex = 1;
                }
                else
                {
                    btnPLC.ImageOptions.ImageIndex = 0;
                }
            }
        }
        [Category("AppearanceTDIN")]
        public string plcIp
        {
            get { return _plcIp; }
            set { 
                _plcIp = value;
                if (_plc != null)
                {
                    _plc.setIpPLC(_plcIp);
                }
                _trangThaiKetNoi = false;
                btnPLC.Text= _plcIp;
            }
        }
        [Category("AppearanceTDIN")]
        public bool m_batHopThoaiRun { get { return _batHopThoaiRun; } set { _batHopThoaiRun = value; } }
        #endregion
        #region FUNCTION
        public ucPLC():base()
        {
            InitializeComponent();
        }
        public bool docToanBoDuLieuTuPLCbufM()
        {
            if (plc.erPLC != S7.Net.ErrorCode.NoError) return false;
            plc.docToanBoDuLieuTuPLCbufM();
            return true;
        }
        public bool docToanBoDuLieuTuPLCbufQ()
        {
            if (plc.erPLC != S7.Net.ErrorCode.NoError) return false;
            plc.docToanBoDuLieuTuPLCbufQ();
            return true;
        }
        public override void readDataFromFile(XmlDocument xmlE)
        {
            try
            {
                var xnList = xmlE.SelectNodes("thong-so-thiet-bi/" + Name);
                foreach (XmlNode xn in xnList)
                {
                    var atList = xn.Attributes;
                    plcIp = atList["plcIp"].InnerText;
                    m_batHopThoaiRun = Convert.ToBoolean(atList["m_batHopThoaiRun"].InnerText);
                    nSizeM = Convert.ToInt32(atList["nSizeM"].InnerText);
                    nSizeQ = Convert.ToInt32(atList["nSizeQ"].InnerText);
                    readDataXMLBase(atList);

                }
            }
            catch
            {
                return;
            }
        }
        public override void writeDataFromFile(XmlTextWriter xmlWriter)
        {
            xmlWriter.WriteStartElement(Name);
            xmlWriter.WriteAttributeString("plcIp", plcIp);
            xmlWriter.WriteAttributeString("m_batHopThoaiRun", m_batHopThoaiRun.ToString());
            xmlWriter.WriteAttributeString("nSizeM", nSizeM.ToString());
            xmlWriter.WriteAttributeString("nSizeQ", nSizeQ.ToString());
            writeDataXMLBase(xmlWriter);
            xmlWriter.WriteEndElement();
        }
        private void mouseClickPLC(object sender, MouseEventArgs e)
        {

        }
        private void ClickBtnPLC(object sender, EventArgs e)
        {
        }
        private void MouseDownBtnClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if (m_batHopThoaiRun)
                {
                    var f = new FormHopThoaiRunKETNOIPLC(_tenThuocTinh, plc);
                    f.Location = this.PointToScreen(e.Location);
                    f.StartPosition = FormStartPosition.Manual;
                    f.ShowDialog();
                }
            }
            else if (e.Button == MouseButtons.Right)
            {
                openDlgConfig();
            }
        }
        #endregion
        private void ucPLC_Load(object sender, EventArgs e)
        {
            _plc = new PLCS71200();
            _plc.setIpPLC(plcIp);
        }
    }
}
