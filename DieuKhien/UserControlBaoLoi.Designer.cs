﻿namespace DayChuyenHPEX.DieuKhien
{
    partial class UserControlBaoLoi
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnErrorLabel = new DevExpress.XtraEditors.SimpleButton();
            this.timerBeep = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // btnErrorLabel
            // 
            this.btnErrorLabel.Appearance.BorderColor = System.Drawing.Color.Transparent;
            this.btnErrorLabel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnErrorLabel.Appearance.ForeColor = System.Drawing.Color.Red;
            this.btnErrorLabel.Appearance.Options.UseBorderColor = true;
            this.btnErrorLabel.Appearance.Options.UseFont = true;
            this.btnErrorLabel.Appearance.Options.UseForeColor = true;
            this.btnErrorLabel.Appearance.Options.UseTextOptions = true;
            this.btnErrorLabel.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnErrorLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnErrorLabel.ImageOptions.SvgImage = global::DayChuyenHPEX.Properties.Resources.warning;
            this.btnErrorLabel.Location = new System.Drawing.Point(0, 0);
            this.btnErrorLabel.Name = "btnErrorLabel";
            this.btnErrorLabel.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btnErrorLabel.Size = new System.Drawing.Size(272, 38);
            this.btnErrorLabel.TabIndex = 1;
            this.btnErrorLabel.Text = "error lỗi sẽ xuất hiện theo thông số trong PLC. Khi biến bật thì sẽ hiển thị lỗi " +
    "ra";
            this.btnErrorLabel.Click += new System.EventHandler(this.clickLabel);
            this.btnErrorLabel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.mouseDownBtnError);
            // 
            // timerBeep
            // 
            this.timerBeep.Interval = 500;
            this.timerBeep.Tick += new System.EventHandler(this.timerBeepStick);
            // 
            // UserControlBaoLoi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.btnErrorLabel);
            this.Name = "UserControlBaoLoi";
            this.Size = new System.Drawing.Size(272, 38);
            this.Load += new System.EventHandler(this.UserControlThuocTinhGhiXuongPLC_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnErrorLabel;
        private System.Windows.Forms.Timer timerBeep;
    }
}
