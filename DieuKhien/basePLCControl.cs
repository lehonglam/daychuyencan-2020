﻿using DayChuyenHPEX.DongThoi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace DayChuyenHPEX.DieuKhien
{
    public class basePLCControl :UserControl
    {
        bool _quyenQuantri = false,_enable=true, _visible=true;
        Point _Location;
        Size _size;
        protected PLCS71200 _plc = null;
        protected string _tenThuocTinh = "Nhiệt độ";
        [Category("AppearanceTDIN")]
        public string tenthuocTinh
        {
            get { return _tenThuocTinh; }
            set
            {
                _tenThuocTinh = value;
                refresh();
            }
        }
        [Category("AppearanceTDIN")]
        public bool quyenQuantri
        {
            get { return _quyenQuantri; }
            set
            {
                _quyenQuantri = value;
            }
        }
        [Category("AppearanceTDIN")]
        public bool enable
        {
            get { return _enable; }
            set
            {
                _enable = value;
                this.Enabled = _enable;
                if (quyenQuantri==true)
                    this.Enabled = true;
            }
        }
        [Category("AppearanceTDIN")]
        public bool m_Visible
        {
            get { return _visible; }
            set
            {
                _visible = value;
                if (quyenQuantri == true)
                    this.Visible = true;
                else
                    this.Visible = _visible;
            }
        }
        [Category("AppearanceTDIN")]
        public Point m_Location
        {
            get { return Location; }
            set
            {
                _Location= value;
                Location = _Location;
            }
        }
        [Category("AppearanceTDIN")]
        public Size m_size
        {
            get { return Size; }
            set
            {
                _size = value;
                this.Size= _size;
            }
        }

        protected virtual void refresh()
        {
        }
        public virtual void capNhatGiatri()
        {        }

        public virtual void readDataFromFile(XmlDocument xmlE)
        {        

        }
        public virtual void writeDataFromFile(XmlTextWriter xtw)
        {
        }
        protected void readDataXMLBase(XmlAttributeCollection atList)
        {
            tenthuocTinh = atList["tenthuocTinh"].InnerText;
            enable = Convert.ToBoolean(atList["enable"].InnerText);
            if (atList["X"] != null)
            {
                int X = Convert.ToInt32(atList["X"].InnerText);
                int Y = Convert.ToInt32(atList["Y"].InnerText);
                Location = new Point(X, Y);
            }
            m_Visible = Convert.ToBoolean(atList["m_Visible"].InnerText);
            if (atList["Width"] != null)
            {
                int Width = Convert.ToInt32(atList["Width"].InnerText);
                int Height = Convert.ToInt32(atList["Height"].InnerText);
                m_size = new Size(Width, Height);
            }
        }
        protected void writeDataXMLBase(XmlTextWriter xmlWriter)
        {
            xmlWriter.WriteAttributeString("tenthuocTinh", tenthuocTinh);
            xmlWriter.WriteAttributeString("enable", enable.ToString());
            xmlWriter.WriteAttributeString("X", Location.X.ToString());
            xmlWriter.WriteAttributeString("Y", Location.Y.ToString());
            xmlWriter.WriteAttributeString("m_Visible", m_Visible.ToString());
            xmlWriter.WriteAttributeString("Width", m_size.Width.ToString());
            xmlWriter.WriteAttributeString("Height", m_size.Height.ToString());

        }
        // bat hop thoai thiet lap thong so
        protected void openDlgConfig()
        {
            if (quyenQuantri == true)
            {
                var f = new FormThietLapThuocTinhGhiXuongPLC(this);
                f.ShowDialog();
            }
        }

        public PLCS71200 plc { get { return _plc; } set { _plc = value; } }
    }
}
