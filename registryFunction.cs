﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DayChuyenHPEX
{
    public class registryFunction
    {
        const string userRoot = "HKEY_CURRENT_USER";
        static string keyName = userRoot + "\\" + Properties.Settings.Default.registryName;
        static public string docReg(string regName)
        {
            return (string )Microsoft.Win32.Registry.GetValue(keyName, regName,"khong doc duoc");
        }
        static public void ghiReg(string regName, string value)
        {
            Microsoft.Win32.Registry.SetValue(keyName, regName, value);
        }
    }
}
